\chapter{Experiments} \label{t:experimentschapter}
In this chapter, we study the quality of the image formation algorithm. In the first section, we describe the potential map used in the experiments. In the next section, we study the behavior of the scattering functions for various parameters. In later sections, we show the simulated projections and compare them to the projections of the transmittance and the multislice models. In the last section, we evaluate the correctness of the simulation via normalized cross-correlation with the measured dataset.

\section{Data}
For experiments, we use the \bgal\ molecule, determined by \gls{cryoem} at an average resolution of \num{~2.2} \angstrom \  \citep{bgalpdb}. Both the reconstructed map and the atomic model files of the molecule are publicly available. We use the atomic model from the \gls{pdb} file format and reconstruct the potential map in our software, to make sure that we have a map of the Coulomb potential of the molecule. \autoref{fig:bgalatom} displays the atomic model of the molecule and \autoref{fig:bgalpoten} shows the Coulomb potential of the molecule generated from the atomic model.

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{../img/bgal-atom.png}
\caption{ Visualization of the publicly available atomic map of the \bgal. Rendered in the UCSF Chimera tool.}
\label{fig:bgalatom}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{../img/bgal-recon.png}
\caption{ Visualization of the generated potential map of the \bgal\ used in experiments. Rendered in the UCSF Chimera tool.}
\label{fig:bgalpoten}
\end{figure}

\section{Scattering function}
In this section, we illustrate the behavior of the implemented scattering functions: the \gls{dcs} scattering function, and the Rutherford scattering function. For both functions, we plot their \gls{pdf} for various positions and settings. For these experiments, we used a generated potential map with a voxel size of \num{0.6375} \angstrom\  and the electron energy of \num{300} \kilo\electronvolt, which is the same energy used for the acquisition of the \bgal\ and commonly used in \gls{cryoem}.

\subsection{DCS scattering function}
With the \gls{dcs} scattering function, we are mainly interested in the influence of the size of the considered potential location - the radius term $r$ in the localized potential Fourier transform. In \autoref{fig:dcsr}, we show the differential cross-section values for a cone of angles around the forward direction. While there are some changes in the shape, the preferred direction is always the forward direction and the directions of interest are within \num{4} degrees from the forward direction, which fits the theory that elastic scattering angles are very small \citep{vulovic}. For this reason, we use only as many samples as required to correctly reconstruct scattering angles up to \num{4} degrees, see \autoref{t:localizedfourierpotential}. For higher scattering angles, we approximate the \gls{dcs} with zero. The bias introduced by this cut-off of angles is expected to be small.

\begin{figure}[htb]
    \centering 
    \subfloat[r=2\bohr] 
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/dcs2r.png}
    } 
    \subfloat[r=4\bohr] 
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/dcs4r.png}
    } 
    \subfloat[r=8\bohr] 
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/dcs8r.png}
    }
    \subfloat[r=16\bohr]
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/dcs16r.png}
    }
    \subfloat[r=32\bohr]
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/dcs32r.png}
    }
    \caption{The figure displays the \gls{pdf} of the \gls{dcs} scattering function around the forward direction represented by the pixel in the middle, for different radii $r$ of the molecule potential. The spread angle in the image is \num{6} degrees, meaning that a pixel in the center of each side of the image represents a direction with a scattering angle of \num{6} degrees. Very low scattering angles are to be anticipated and match the theory. }
    \label{fig:dcsr}
\end{figure}

\subsection{Rutherford scattering function}
The Rutherford scattering function is defined for a single atom of a known atomic number and does not depend on the potential values. Nevertheless, it makes a good analytical alternative to the \gls{dcs} scattering function. We found two applications of this function. We use it as a fast analytical approximation of the \gls{dcs} scattering function and compare those functions in the image formation simulation. We also use the function to importance sample the \gls{dcs} function thanks to their similarity. 
\par
In \autoref{fig:rutherex} we display the Rutherford \gls{pdf} for different elements which are present in the \bgal\ and common in other biological molecules. The image shows that the scattering function for Hydrogen (atomic number $1$) is visually most similar with the \gls{dcs} scattering function examples shown above. The suitability of this atomic number is later confirmed by the cross correlation experiments with real data (\autoref{fig:ruthergraph}).

\begin{figure}[htb]
    \centering 
    \subfloat[H, Z=1] 
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/ruther1.png}
    } 
    \subfloat[C, Z=6] 
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/ruther6.png}
    } 
    \subfloat[O, Z=8] 
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/ruther8.png}
    }
    \subfloat[Mg, Z=12]
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/ruther12.png}
    }
    \subfloat[S, Z=16]
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/ruther16.png}
    }
    \caption{The figure displays the \gls{pdf} of the Rutherford scattering function around the forward direction, represented by the pixel in the middle for various elements and their atomic numbers Z. The spread angle in the image is \num{6} degrees, meaning that a pixel in the center of each side represents a direction with the scattering angle of \num{6} degrees. Hydrogen visually fits the \gls{dcs} the most, compare to \ref{fig:dcsr}. The suitability of the lowest atomic number is later confirmed by the experiments (\autoref{fig:ruthergraph}). }
    \label{fig:rutherex}
\end{figure}


\section{Projections}
In this section, we show projections of the molecule with different simulation setups and later compare them to the projections from the transmittance and multislice model. Note that all images are normalized to increase the perceived contrast. This may cause the diversions in the color of the background. The setup is similar as before, we used a generated potential map with a voxel size of \num{0.6375}~\angstrom\  and the electron energy of \num{300} \kilo\electronvolt.

\subsection{Electron dose} 
In \autoref{fig:eldose} we show projections with the \gls{dcs} scattering function at an increasing electron doses. As expected, with the increasing dose the noise within the image recedes. For illustration, the dose used to acquire the \bgal\ data is \num{45} $\slfrac{e^-}{\angstrom^2}$ which corresponds to approx. \num{12} $\slfrac{e^-}{\bohr^2}$. However, the estimated mean free path for the majorant element, which is Sulfur for the  \bgal, is approx. \num{3800} \bohr, while the length of the volume is about \num{700} \bohr. This means that only \num{2} electrons per Bohr squared undergo a scattering event and many of these events will result in null scattering, essentially contributing no information to the image. The real projections contain noise from other sources (scattering from the air and water molecules, detector noise, variances in the electron beam density, etc.) and it is not possible to directly compare them, as shown in \autoref{t:comparison}. Thus, we cannot verify the validity of the noise of our model caused by undersampling.

\begin{figure}[htb]
    \centering 
    \subfloat[2 $\slfrac{e^-}{\bohr^2}$] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_d2.png}
    } 
    \subfloat[12 $\slfrac{e^-}{\bohr^2}$] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_d12.png}
    } 
    \subfloat[50 $\slfrac{e^-}{\bohr^2}$] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_d50.png}
    }
    \subfloat[120 $\slfrac{e^-}{\bohr^2}$]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_d120.png}
    }
    \\
    \subfloat[250 $\slfrac{e^-}{\bohr^2}$]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_d250.png}
    }
    \subfloat[1000 $\slfrac{e^-}{\bohr^2}$] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_d1000.png}
    } 
    \subfloat[5000 $\slfrac{e^-}{\bohr^2}$] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_d5000.png}
    }
    \subfloat[10000 $\slfrac{e^-}{\bohr^2}$] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_d10000.png}
    }
    \\
    \subfloat[50000 $\slfrac{e^-}{\bohr^2}$]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_d50000.png}
    }
    \subfloat[100000 $\slfrac{e^-}{\bohr^2}$]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_d100000.png}
    }
    \caption{The figure shows the influence of the electron dose on the acquired image. The dose of 12 $\slfrac{e^-}{\bohr^2}$ corresponds to a realistic dose of 45 $\slfrac{e^-}{\angstrom^2}$, which has been used to acquire the real \bgal\ data. The images are normalized which can cause diversions in the backgroud color. As anticiapted, the percieved amount of noise is reduced with increased electron dose. }
    \label{fig:eldose}
\end{figure}

\subsection{Fourier radius influence}
In \autoref{fig:dcsrad} we illustrate how the projections differ for various radii of the Fourier transform of the potential in the \gls{dcs} scattering function. The projections differ more than was anticipated. Note that the radius does not influence where the electrons scatter, only in which directions. With an increasing radius, low-frequency effects of the scattered electrons seem to be exchanged for higher-level clusters of scattered electrons. It may be explained by the fact that for larger scattering radii, a larger portion of the molecule influences the new direction, leading to the scattered electron carrying lower frequency information, but it is only a speculation. We also evaluate the different radii in comparison with the measured data in \autoref{fig:dcsgraph}. However, these measurements do not give a clear answer to which of the radii approximates the real behavior of the electron most correctly, it remains an open question.

\begin{figure}[htb]
    \centering 
    \subfloat[r=1\bohr] 
    {
    \includegraphics[width=0.31\textwidth]{../img/exp/proj_r1.png}
    } 
    \subfloat[r=2\bohr] 
    {
    \includegraphics[width=0.31\textwidth]{../img/exp/proj_r2.png}
    } 
    \subfloat[r=4\bohr] 
    {
    \includegraphics[width=0.31\textwidth]{../img/exp/proj_r4.png}
    }
    \\
    \subfloat[r=8\bohr]
    {
    \includegraphics[width=0.31\textwidth]{../img/exp/proj_r8.png}
    }
    \subfloat[r=16\bohr]
    {
    \includegraphics[width=0.31\textwidth]{../img/exp/proj_r16.png}
    }
    \caption{ Projections from the \gls{dcs} scattering model with different radius of the scattering. The radius of \num{1} \bohr\ causes the scattered electrons to be mostly weighted down to zero, corresponding to tiny scattering angles. With an increasing radius, low frequency effects of the scattered electrons seem to create more centered clusters of scattered electrons. The effect was not anticipated and more experiments are needed for explanation. }
    \label{fig:dcsrad}
\end{figure}


\subsection{Rutherford scattering influence}
\autoref{fig:rproj_s} displays the simulated projections with the Rutherford scattering function for varying atomic numbers. We later show that this approximation works quite well with the lowest atomic number. We have used a dose of \num{100000} $\slfrac{e^-}{\bohr^2}$. The image shows that for increasing atomic number the halo effect around the molecule becomes larger. This is due to the scattered electrons having tendency to scatter into larger angles for heavier atoms, as shown in \autoref{fig:rutherex}.

\begin{figure}[htb]
    \centering 
    \subfloat[Z=1] 
    {
    \includegraphics[width=0.31\textwidth]{../img/exp/rproj_s1.png}
    } 
    \subfloat[Z=3] 
    {
    \includegraphics[width=0.31\textwidth]{../img/exp/rproj_s3.png}
    } 
    \subfloat[Z=8] 
    {
    \includegraphics[width=0.31\textwidth]{../img/exp/rproj_s8.png}
    }
    \\
    \subfloat[Z=12]
    {
    \includegraphics[width=0.31\textwidth]{../img/exp/rproj_s12.png}
    }
    \subfloat[Z=16]
    {
    \includegraphics[width=0.31\textwidth]{../img/exp/rproj_s16.png}
    }
    \caption{ Images show projections from the Rutherford scattering model with different atomic number used in the scattering formula. For higher atomic numbers, the scattering angles became larger (\autoref{fig:rutherex}), leading to an increased halo distance around the molecule. The Hydrogen with Z=1 is later shown to be the best approximation when compared with the measured data. }
    \label{fig:rproj_s}
\end{figure}

\subsection{Defocus influence}
In this subsection, we show the influence of various defocus distances for the Rutherford scattering model, however, the effect is nearly identical for the \gls{dcs} model. As described in \autoref{t:simpledetector}, we model the defocus only as a distance of the detector from the sample, because our model does not contain any lenses. As shown in \autoref{fig:rutherdefocus}, the important effect of this parameter is the area that the scattered electrons cover on the detector. Again a dose of \num{100000} $\slfrac{e^-}{\bohr^2}$ has been used to produce the images. In the \bgal\ dataset, the estimated defocus values range between \num{3000} \angstrom\ and \num{40000} \angstrom. In the figures, we show a similar range of defocus values. We have also included the projection at a very low defocus of \num{192} \angstrom\ to illustrate the vast difference. Our model does not allow smaller defocus than half of the size of the input volume. Also note that for our model a value of zero defocus would mean loss of all information for the potential in focus. A similar problem is also present in the real microscopes and is the reason why the measurements are done in defocus.

\begin{figure}[htb]
    \centering 
    \subfloat[192 \angstrom] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/rproj_s1.png}
    }
    \subfloat[300 \angstrom] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/rproj_s1_def300.png}
    }
    \subfloat[600 \angstrom] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/rproj_s1_def600.png}
    } 
    \subfloat[1200 \angstrom] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/rproj_s1_def1200.png}
    }
    \\
    \subfloat[2400 \angstrom] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/rproj_s1_def2400.png}
    }
    \subfloat[5000 \angstrom] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/rproj_s1_def5k.png}
    }  
    \subfloat[13750 \angstrom] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/rproj_s1_def13750.png}
    }
    \subfloat[22500 \angstrom]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/rproj_s1_def22500.png}
    }
    \\
    \subfloat[31250 \angstrom]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/rproj_s1_def31250.png}
    }
    \subfloat[31250 \angstrom]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/rproj_s1_def31250.png}
    }
    \caption{ The figure illustrates how the simulated images differ for various defocus values. In these figures, we used the Rutherford scattering function with the Hydrogen scattering parameter, which provided the finest fit with the measured data, as we show in \autoref{t:comparison}. }
    \label{fig:rutherdefocus}
\end{figure}


\subsection{Electron energy influence}
\autoref{fig:eleenergy} illustrates how the projections differ for various initial electron energies. We have used the \gls{dcs} model for this experiment, as the energy of the electron influences not only the electron mean free path, but also the magnitude of the electron momentum which is used in the \gls{dcs} evaluation. However, as can be observed in the image, the major effect is in the amount of noise stemming from longer mean free paths for higher energies, as shown in \autoref{fig:estimfp}. If we return once more to the sphere graph from \autoref{fig:dcssphere}, the electron energy influences the radius of the sphere, but the low scattering angles are still located around the frequencies close to zero. This means that the \gls{dcs} distribution for low scattering angles probably remains similar for various energies of the electrons.
\begin{table}[ht]
\centering
\begin{tabular}{c|c}
Energy [\kilo\electronvolt] & $\lambda$ [\bohr] \\
\hline
50 &  1278.46 \\
100 & 2049.53 \\
150 & 2652.42 \\
200 & 3132.67 \\
250 & 3521.41 \\
300 & 3840.50  \\
350 & 4105.63 \\
400 & 4328.31 \\
450 & 4517.15 \\
500 & 4678.67 \\
\end{tabular}
\caption{Table shows estimated mean free paths (in Bohr units) for different electron energies. Note that the distance from the emitter to detector at the lowest defocus setup, which is used in this test, is around \num{700} \bohr. }
\label{fig:estimfp}
\end{table}

\begin{figure}[htb]
    \centering 
    \subfloat[50 \kilo\electronvolt] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/energy50.png}
    }
    \subfloat[100 \kilo\electronvolt] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/energy100.png}
    }
    \subfloat[150 \kilo\electronvolt] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/energy150.png}
    } 
    \subfloat[200 \kilo\electronvolt] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/energy200.png}
    }
    \\
    \subfloat[250 \kilo\electronvolt] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/energy250.png}
    }
    \subfloat[300 \kilo\electronvolt] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/energy300.png}
    }  
    \subfloat[350 \kilo\electronvolt] 
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/energy350.png}
    }
    \subfloat[400 \kilo\electronvolt]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/energy400.png}
    }
    \\
    \subfloat[450 \kilo\electronvolt]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/energy450.png}
    }
    \subfloat[500 \kilo\electronvolt]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/energy500.png}
    }
    \caption{ The figure illustrates how the simulated images differ for various electron energies. The main difference is the amount of noise in the images, which stems from different estimated mean free paths (shown in \autoref{fig:estimfp}). }
    \label{fig:eleenergy}
\end{figure}

\section{Comparison} \label{t:comparison}
In this section, we compare simulated images from our model with both the transmittance and multislice models. The measured projections from the microscope are not viable for direct comparison due to a very low signal to noise ratio, see \autoref{fig:realproj}. Instead, in \autoref{t:ca} we compare the quality of the simulation with averages of the measured projections.

\begin{figure}[htb]
    \centering 
    \subfloat 
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/proj42_1.png}
    } 
    \subfloat 
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/proj42_16.png}
    } 
    \subfloat 
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/proj42_56.png}
    }
    \subfloat
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/proj42_63.png}
    }
    \subfloat
    {
    \includegraphics[width=0.18\textwidth]{../img/exp/proj42_73.png}
    }
    \caption{ Figure shows examples of measured projections from the \bgal\ dataset. The particle is difficult to see due to the low signal to noise ratio. }
    \label{fig:realproj}
\end{figure}


\subsection{Projections}
\autoref{fig:models} displays the projections of the \bgal\ molecule from the same angle using different models. The multislice model shows the amplitude of the exit plane wave, without the effects of the lenses and defocus. The transmittance model does not simulate defocus at all. Our \gls{dcs} and Rutherford models have the lowest possible defocus of half the size of the potential volume, which is about 160 \angstrom. There is a relatively good visual correspondence of the \gls{dcs} and Rutherford models with the multislice model. Note that while the multislice model simulates a plane wave function propagating through the potential map, our models simulate electrons as particles and we simply detect their numbers on the detector, so the similarity is remarkable. The transmittance model is not a good approximation of the image formation, at least not at low defocus. However, as shown in \autoref{fig:rutherdefocus}, the defocus causes scattered electrons to cover a larger area and the transmittance model actually becomes a very good approximation. This is beacuse the contrast effect of the scattered electrons becomes weaker, and the transmittance model treats the volume as a purely absorbtion volume, in which electrons disapear during interactions.

\begin{figure}[htb]
    \centering
    \subfloat[\gls{dcs}]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_r4.png}
    } 
    \subfloat[Rutherford]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/rproj_s1.png}
    } 
    \subfloat[Multislice]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_multislice.png}
    }
    \subfloat[Transmittance]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_int.png}
    }
    \caption{ Figure shows the simulated projection of the molecule via different models. For the \gls{dcs} model we have used the radius of \num{4} \bohr. For the Rutherford model, the sampling atomic number is set to \num{1}. The multislice projection has been generated by the Matlab simulator by Vulović \cite{vulovic}. The slice thickness is set to a resolution of one voxel, which is 0.7 \angstrom. It uses a blurred potential because the Fourier transform does not handle high-frequency information well and produces sinc-like artifacts. For the transmittance model, we have used a random supersampling with six rays per pixel. }
    \label{fig:models}
\end{figure}

\subsection{Class averages} \label{t:ca}
In this subsection, we compare the simulation with the measured data from the \bgal\  dataset. As shown in \autoref{fig:realproj}, the measured projections themselves are too noisy for direct comparison. Instead, we use class averages from the preprocessing step of the reconstruction - after the projections of the molecule are found and cut from the micrograph, they are classified by similarity into various classes representing projections from a similar angle. The classification was done by the RELION tool \citep{relion}. \autoref{fig:classaverages} shows four averages that we used for the model quality estimation.

\begin{figure}[htb]
    \centering
    \subfloat[class10]
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/class10.png}
    } 
    \subfloat[class38]
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/class38.png}
    } 
    \subfloat[class42]
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/class42.png}
    }
    \subfloat[class50]
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/class50.png}
    }
    \caption{ The figure shows class averages that we used for comparison with the simulated data. There is a noticeable circle in each average, which is caused by the classification algorithm. Due to the different rotation of each projection, the area outside of the intersection of these projections is nulled. The averages are generated using the RELION software \citep{relion}. }
    \label{fig:classaverages}
\end{figure}



\subsubsection{Setup}
During the reconstruction process, the direction of each projection is estimated. From the RELION tool, we have four sets of projections with similar viewing angle and the corresponding average image. We simulate a set of projections with the same angles and parameters, and then compare the average of the simulated set with the average of the measured set. As a similarity metric of the averages, we use a normalized cross-correlation defined as:
\begin{equation}
ncc(A,B)=\dfrac{\sum_{x=0}^{n-1}\sum_{y=0}^{m-1} A[x,y]*B[x,y]}{\sqrt{\sum_{x=0}^{n-1}\sum_{y=0}^{m-1} A[x,y]^2 * \sum_{x=0}^{n-1}\sum_{y=0}^{m-1} B[x,y]^2}}
\end{equation}
where $A$ and $B$ are the compared images with sizes $n$ and $m$.
\par
For comparison, we use both the \gls{dcs} model, the Rutherford model, and the transmittance model. In the \gls{dcs} model we test the influence of the radius parameter and in the Rutherford model, we modify the scattering atomic number parameter. The transmittance model is tested with one ray per pixel and with random supersampling with six rays per pixel.
\par 
We do not generate and compare the projections of the multislice model because it is difficult to set up fair conditions. The multislice approach models defocus through a \gls{ctf}. However, the \gls{ctf} function also models other lens properties which are not desirable in our setup, because we have \gls{ctf} corrected real data averages. That means that the effect of the \gls{ctf} has been suppressed and comparing \gls{ctf} affected images with corrected images yields poor correlation.
\par
Also, as described previously, the class averages generated by the RELION tool have inverted contrast. This results in a negative sign of the computed cross-correlation. In the following sections, we will compare only the absolute values of the normalized cross-correlation and ignore the sign. In real unmodified measurements, the particle projections are represented by lower values compared to the background, which is in correspondence with both the \gls{dcs} and Rutherford model.

\subsubsection{Results}

\begin{description}
\item[\gls{dcs} model] \autoref{fig:dcsgraph} shows normalized cross-correlation of the \gls{dcs} scattering model with each of the class averages. The figure also displays how does the localized Fourier transform radius influence the normalized cross-correlation. Three of the four class averages show similar behavior concerning the radius influence, but the behavior is not linear. Together with the fact that the fourth class average displays nearly inverse behavior, it is difficult to establish which radius is the best or how the correlation might progress for higher radii.
\item[Rutherford model] \autoref{fig:ruthergraph} shows normalized cross-correlation of the Rutherford scattering model with each of the class averages and the effect of the scattering atomic number. There is a clear relationship between the atomic number and the normalized cross-correlation which is consistent across all classes. The figure also shows that the lowest atomic number \num{1} (Hydrogen) has the best fit across all classes.
\item[Transmittance model] With the transmittance model we tested one ray per pixel and random supersampling with six rays per pixel. The difference in the achieved cross-correlation was minimal between those two setups, with the supersampled version being always very slightly better. Thus we have used only the supersampled results and in \autoref{fig:bestof} we show a comparison of the transmittance model and the other two models.
\end{description}


\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{../img/cmp/dcs_graph.png}
\caption{ The figure shows a normalized cross-correlation of the \gls{dcs} model and the class average. On the X-axis are radii used for the localized Fourier transform and each line represents correlations with one class average. The figure shows similar behavior for all class averages except for the average of class 50. It might be due to the angle of the average or higher noise levels. Also, note that the behavior is not linear - it is not clear whether any of the radii are better than others. More measurements are needed. }
\label{fig:dcsgraph}
\end{figure}


\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{../img/cmp/ruther_graph.png}
\caption{ The figure shows a normalized cross-correlation of the Rutherford model and the class average. Each line represents correlations with one class average. The X-axis represents the atomic number used for the scattering of the electrons. There is a clear trend of decreasing normalized cross-correlation with increasing atomic number. }
\label{fig:ruthergraph}
\end{figure}

\par
\autoref{fig:bestof} shows the best cross-correlation for each model and each class. Both the \gls{dcs} and Rutherford model outperform the transmittance model in the correlation with the class averages. The \gls{dcs} scattering model is better than Rutherford in all but the class average \num{50}, where the behavior of all models is distinct. We expected the \gls{dcs} model to outperform the Rutherford, as it is in theory better approximation of the scattering directions. Ideally, comparison with more class averages could help to rule out the strange properties of class \num{50}.
\par
For illustration purposes, \autoref{fig:total} shows class averages and the generated averages for each class and each of the models. The positions of the images corresponds with the organization of \autoref{fig:bestof}.

\begin{table}[ht]
\centering
\begin{tabular}{l|c|c|c}
\diagbox{class}{model} & \gls{dcs} & Rutherford & Transmittance \\
\hline
class10 & -0.972441 & -0.962295 & 0.922992 \\
class38 & -0.975895 & -0.963070 & 0.925164 \\
class42 & -0.975895 & -0.967548 & 0.932671 \\
class50 & -0.976039 & -0.977107 & 0.945431 \\
\end{tabular}
\caption{ Each row of the table represents one class average and each column represents one of the models. Each entry is the best achieved normalized cross-correlation of the given model and the given class average. Clearly, DCS yields the highest correlation with the real data class averages and both DCS and Rutherford models outperform the Transmittance model. }
\label{fig:bestof}
\end{table}

\begin{figure}[htb]
    \centering 
    \subfloat[class10] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c10m.png}
    }
    \subfloat[\gls{dcs}] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c10d.png}
    }
    \subfloat[Rutherford] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c10r.png}
    }
    \subfloat[Transmittance] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c10i.png}
    }
    \\
    \subfloat[class38] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c38m.png}
    }
    \subfloat[\gls{dcs}] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c38d.png}
    }
    \subfloat[Rutherford] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c38r.png}
    }
    \subfloat[Transmittance] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c38i.png}
    }
    \\
    \subfloat[class42] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c42m.png}
    }
    \subfloat[\gls{dcs}] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c42d.png}
    }
    \subfloat[Rutherford] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c42r.png}
    }
    \subfloat[Transmittance] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c42i.png}
    }
    \\
    \subfloat[class50] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c50m.png}
    }
    \subfloat[\gls{dcs}] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c50d.png}
    }
    \subfloat[Rutherford] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c50r.png}
    }
    \subfloat[Transmittance] 
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c50i.png}
    }
    \caption{  Each row of the table represents one class average and each column represents one of the models. The first column contains class averages with inverted values, to optically correspond with the real measurements. The second column is the average generated by the \gls{dcs} model with the best cross-correlation with the given class average. Similarly, the third and fourth columns show the best average for the Rutherford model and transmittance model, respectively. }
    \label{fig:total}
\end{figure}







 
