\chapter{Electron elastic scattering image formation} \label{t:chapterone}
In this chapter, we describe the algorithm for image formation via electron tracing. As mentioned in \nameref{c:objectives}, our algorithm aims to encompass the same extent of functionality as \nameref{c:xray} and \nameref{c:multislice}, namely without the additional effects of optics, detector, radiation damage, and inelastically scattered electrons.
\par
In the following section, we characterize electron interactions with the sample. Then, we define our simplified setup of the simulation. Finally, we describe the image formation as \gls{rte} for which we derive a Monte Carlo estimator with two viable scattering models.

\section{Electron-sample interactions} 
Electrons interact with the Coulomb potential of the target molecules and the surrounding vitreous ice molecules. The image obtained from the electron microscope is a form of projection of this potential onto the detector. The commonly used energies for electron acceleration are from a few \kilo\electronvolt\ to a few \mega\electronvolt. It corresponds to one Volt unit multiplied by the electron's elementary charge. For lower acceleration energies, the emitted electrons become indistinguishable from the sample electrons, while for energies higher than a few~\mega\electronvolt, relativistic effects and knock-on damage become significant \citep{reimer2013transmission}. The interaction event can be classified either as elastic or inelastic scattering. \autoref{fig:elinteraction} shows both types of interaction and other secondary effects. Simply said, the elastic scattering carries information while the inelastic scattering damages the sample. In the following subsections, we provide closer look into the events, for detailed info see \cite{eenergy}.

\subsection{Elastic scattering} \label{elasticscattering}
Elastic interactions are characterized by identical initial and final quantum states of the molecule atoms. In other words, very little energy has been transferred from an electron to atoms of the sample during the interaction. Due to the large mass of the molecule compared to the mass of the electron, the average energy lost by the electron during elastic scattering is a tiny fraction of its energy and can be safely neglected. This is equivalent to assuming that the target has infinite mass \citep{penelope}. The elastically scattered electrons transfer the structural information about the molecule onto the detector. The elastic scattering occurs more often in the \gls{cryoem} setup than inelastic, and its effects are less damaging. For these reasons elastic scattering events are the main contributors to the signal in the final image \citep{vulovic}.

\subsection{Inelastic scattering} 
During inelastic scattering, the energy of the electron is deposited into the sample. This causes excitations and ionisations of the atoms, loss of energy of the electron, and larger deflection angles compared to the elastic scattering~\citep{penelope}. Inelastic scattering contributes heavily to the radiation damage and noise in the final image~\citep{cryoemoverview}. Inelastically scattered electrons also contribute in the form of amplitude contrast. However, their contribution is low and blurry because the electrons lose their coherency~\citep{vulovic}. For these reasons they are filtered out and do not reach the detector. The transferred energy and caused damage may also result in the secondary effects of interaction displayed in \autoref{fig:elinteraction}.

\begin{figure}[ht]
\centering
\includegraphics[width=0.7\linewidth]{../img/elinteraction.png}
\caption{
The image shows various radiation types that can appear after the scattering event. Many of these types are secondary effects of the inelastic scattering and the subsequent radiation damage. Image taken from \cite{electronmatter}. 
}
\label{fig:elinteraction}
\end{figure}

\section{Simplified image formation process}
In this section, we describe the implemented formation process and simulated electron microscope components. \autoref{fig:simpleschema} illustrates the simplified setup of our simulation. We do not simulate the effects of lenses and apertures, and we do not explicitly model the noise caused by the vitreous ice. We mainly focus our attention on the elastic scattering of electrons, neglecting all other interactions.

\begin{figure}[ht]
\centering
\includegraphics[width=0.7\linewidth]{../img/simpleschema.png}
\caption{ Schema of the simplified image formation process. Electrons are emitted perpendicular to the emitter plane. They elastically interact with the potential map of a single molecule. Finally, all emitted electrons are counted on the detector plane. }
\label{fig:simpleschema}
\end{figure}

\subsection{Emitter}
We simplify the emitter and the lenses that focus electrons on the sample by a virtual plane emitting beam of parallel electrons in a perfectly orthogonal direction. This corresponds to an ideal situation, in which the electrons in the beam are perfectly parallel before they hit the sample. 

\subsection{Input} \label{c:interactions} \label{c:input}
The input to our simulator is a 3D representation of the Coulomb potential map of the molecule, because electrons interact elastically with the Coulomb potential of the sample. The Coulomb potential map can be either outcome of some cryo-EM reconstruction, artificially generated potential from \gls{pdb} files or potential map acquired by some other method. Similarly to other simulation models, we cannot rely on the physical plausibility of the values in the potential map, otherwise the applicability of this simulation would be severely limited. The impact of this input property is explored in \autoref{t:meanfreepath}.

\subsection{Detector} \label{t:simpledetector}
In our simulation setup, it is not essential to model the lenses because our virtual detector does not need magnification. We simulate a perfect counter detector, which counts each electron impact in a pixel of the detector and the pixels can be as small as we choose. However, to compare the output images with the real measured projections, we model a part of the defocus effect through this component. In a real microscope, defocus is the distance of the sample from the focal point of the objective lens, as shown in \autoref{fig:defocus}. The distance has a great impact on the acquired projections. As in a classic light microscope, defocus causes blurring of the image. But it also strongly influences the spread radius of the electrons on the detector plane, which in turn strongly changes the acquired image, see \autoref{fig:rutherdefocus} for examples. To approximate the electron spread effect, we move the detector away from the sample by the defocus value. When we refer to the defocus in the following text of the thesis, we mean by it the distance of the detector.

\begin{figure}[ht]
\centering
\includegraphics[width=0.7\linewidth]{../img/defocus.png}
\caption{ The figure shows an overfocused and underfocused sample in a real electron microscope. The distance of the sample from the focal plane determines the spread radius of the scattered electrons on the detector plane.}
\label{fig:defocus}
\end{figure}

\section{Electron elastic cross section} \label{electronscattering}
For this thesis, we limit the electron interactions to elastic scattering only, as explained in \autoref{c:interactions}. The elastic scattering occurs when the electron is somewhat deflected from its current path by the Coulomb potential of the sample. Similarly to other particle-matter interactions, we can describe the phenomena with a \acrfull{dcs} equation \cite{vaclav}. In this section, we provide two means of estimating the differential and total cross-section for electron elastic scattering, but before we introduce the electron \gls{dcs}, we describe the cross-section value.

\subsection{Cross-section}
A cross-section is a quantification of scattering effects of atoms on an electron. \Acrlong{dcs} is a hypothetical area that captures the amount of flux of electron for the given incoming and outgoing directions. If we multiply the \gls{dcs} by the flux of incoming electrons, we get the number of electrons per second that are crossing the \gls{dcs} area. It ends up being the same as the number of electrons scattered into the given direction. In a way, the \acrlong{dcs} can be interpreted as a size of an area that will scatter the particles into the given direction, and total cross-section \gls{tcs} as an area that the electron must hit for the scattering to occur.

\subsection{Electron elastic differential cross section}
The elastic scattering \gls{dcs} is directly tied to the Coulomb potential of the atoms. The relation of the electron elastic \gls{dcs} to the Coulomb potential \gls{cp} is given by:
\begin{equation} \label{eedcs}
	\frac{\mathrm{d} \sigma}{\mathrm{d} \Omega} (p' \leftarrow p) = ( \frac{m}{2 \pi} )^2 \lvert \hat{V}(q) \rvert^2,
\end{equation}
where $\Omega$ is a solid angle, \gls{cpf} is a Fourier transform of the potential, $m$ is the electron mass, $p$ is the original momentum of the electron, $p'$ is the new momentum after scattering and $q = p' - p$ \citep{vaclav}.
\par

\nameref{elasticscattering} is characterized by a negligible loss of energy of the electron from which follows that $\lvert p' \rvert = \lvert p \rvert $. We can make the observation that the relevant values of the Fourier transform of the potential are located on a sphere with radius $|p|$ and center in $-p$, see \autoref{fig:dcssphere}. From now on, we might use the symbols $p$ and $p'$ both as directions and as momentums of the electron. It should be clear from the context whether the magnitude property of the term is important or not. Keep in mind that the electron does not lose any of its energy, thus the magnitude of the momentum remains constant during simulation.

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{../img/tcs.png}
\caption{ Visualization of the sphere appearing in \autoref{eedcs}. Vector $p$ is the original momentum of the electron and vector $p'$ represents one of possible new momentums of the electron. The sphere then displays all possible values for expression $q = p' - p$. Image created via geogebra tool \citep{geogebra}. }
\label{fig:dcssphere}
\end{figure}

\begin{figure}[htb]
    \centering
    \subfloat[]
    {
        \includegraphics[width=0.23\textwidth]{../img/dcs1.png}
        \label{fig:first_sub}
    }
    \subfloat[]
    {
        \includegraphics[width=0.23\textwidth]{../img/dcs2.png}
        \label{fig:second_sub}
    }
    \subfloat[]
    {
        \includegraphics[width=0.23\textwidth]{../img/dcs3.png}
        \label{fig:third_sub}
    }
      \subfloat[]
    {
        \includegraphics[width=0.23\textwidth]{../img/dcs4.png}
    }
    \caption{Examples of the \gls{dcs} scattering computed from the \autoref{eedcs} model. The images show \gls{dcs} scattering directions on different positions within the potential. The original direction points into the middle of the image and the pixel in the middle of each side represent a scattering angle of 6 degrees. It can be viewed as a projection of the values on the cap of the sphere (see \autoref{fig:dcssphere}), centered around the $p$ vector.}
    \label{fig:dcsexamples}
\end{figure}

\subsubsection{Total cross section and mean free path} \label{t:totalcrosssection}
By integrating \autoref{eedcs} over all scattering directions
\begin{equation} \label{totalcrosssectionequation}
\int_{\Omega} \frac{\mathrm{d} \sigma}{\mathrm{d} \Omega} (p' \leftarrow p) \mathrm{d}\Omega,
\end{equation}
we compute the total cross section \gls{tcs} of the elastic scattering. The total cross section value \gls{tcs} is also tied with the mean free path~$\lambda$ via the following formula:
\begin{equation} \label{meanfreepath}
\lambda = \frac{1}{\rho_n \sigma},
\end{equation}
where $\rho_n$ is the number density of scattering centers, in our case the number of atoms \citep{meanfreepathbook}. This formula becomes useful later, as it fits nicely into the implemented delta-tracking algorithm.

\subsubsection{Frame size of the Coulomb potential} \label{c:framesize}
In \autoref{eedcs}, we have introduced the dependency of the cross-section on the Coulomb potential of the sample.  However, it is undesirable to use the potential map of the whole sample. The first Born approximation assumes electron incoming from infinity, interacting with an isolated potential, and observing the scattering at infinite distance \citep{vaclav}. In other words, it assumes an isolated interaction. We reflect this idea by limiting the area that can affect the cross-section computation.
\par
We tested different sizes of the surrounding area used to compute the \gls{dcs}. In theory, the Born approximation should work the best for very low radii of the area, around \num{2} to \num{4} Bohrs \citep{vaclav}. From now on, we will denote the localized Coulomb potential with center $c$ and radius $r$ as \gls{cpcr} and the corresponding Fourier transform as \gls{cpfcr}.

\subsection{Screened Rutherford formulae} \label{t:rutherfordformulae}
Screened Rutherford formuale are a mathematical model that describes \gls{dcs} of an elastic electron scattering for a single atom of any chemical element \citep{heavyparticles}. The model provides analytical forms for both the electron \acrlong{dcs} and total cross section:
\begin{equation} \label{rutherdcs}
\frac{\mathrm{d} \sigma}{\mathrm{d} \Omega} = Z^2 r_e^2 \left( \frac{1-\beta^2}{\beta^4}   \right) \frac{1}{(1 - cos\theta + 2\eta)^2 }
\end{equation}
\begin{equation} \label{ruthertcs}
\sigma = Z^2 r_e^2 \left( \frac{1-\beta^2}{\beta^4}   \right) \frac{1}{2\eta(\eta+1)}
\end{equation}
where $\theta$ is the scattering angle between the incident and the outgoing electron, $Z$ is the atomic number of the atom, $r_e$ is the classical electron radius, $\beta$ is the ratio between relativistic speed of the electron and the speed of light and $\eta$ is the screening parameter, which reduces \gls{dcs} at small scattering angles. The Rutherford formulae are dervied from the first Born approximation and thus are valid only for low atomic numbers and high energies \citep{betterscreeningparamsoverview}. Fortunately, this fits the situation within the considered \gls{cryoem}. In our implementation, we use Rutherford formulae as a fast but less precise alernative to the \autoref{eedcs}.

\subsubsection{Screening parameter}
A number of screening parameters has been derived for the Rutherford formulae, their overview can be found in \cite{screeningparamsoverview}. In our software, we have used the Moliere screening parameter as described in \cite{betterscreeningparamsoverview}. The implemented Moliere screening parameter formula looks as follows:
\begin{equation}
\eta_m = \frac{Z^{\frac{2}{3}}}{4} \left( \frac{\alpha}{0.885} \right)^2 \frac{1-\beta^2}{\beta^2} \left[  1.13 + 3.76 \left( \frac{\alpha Z}{\beta} \right)^2  \right],
\end{equation}
where $\alpha$ is the fine structure constant. We have compared the quality of the Rutherford approximation with the chosen Moliere sceening parameter by comparing it with a measured data from NIST database \citep{NIST} for Sulfur. \autoref{fig:ruthevsnist} shows comparison of the differential cross sections values from NIST database and from Rutherford formula for low scattering angles. There is a slight difference in magnitude which influences the determination of the mean free path described in \autoref{t:meanfreepath}. However, the magnitude difference does not influence the scattering direction probabilites. The Rutherford approximation seems to fit the measured data very nicely.

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{../img/ruthervsnist.png}
\caption{ The figure shows a comparison of the Rutherford model computed differential cross sections and measured differential cross sections for Sulfur from NIST database \citep{NIST}. The values fall off quickly to zero as we approach larger scattering angles. This corresponds with the \gls{dcs} visualization images in \autoref{fig:dcsexamples} generated via the model described by \autoref{eedcs}.}
\label{fig:ruthevsnist}
\end{figure}

\section{Electron tracing}
In \autoref{electronscattering}, we have introduced equations to compute the scattering directions of the electron. The proposed problem to find the image formed by elastically scattered electrons can be formulated as particle transport in volume and thus can be solved via Monte Carlo (MC) electron tracing. Numerous MC algorithms are designed to solve the general problem of particle transport in volume \citep{mcphysical}.

\subsection{Problem formulation}
Within each detector pixel, we are interested in the number of electrons incoming from all directions. The problem can be formulated as an integral \acrfull{rte} described in \citep{mcphysical}. Let $E(x,\omega)$ be a number of electrons incoming to a point $x$ from a solid angle $\omega$. We can modify the standard \gls{rte} with properties of our model. We intentionally leave out the absorption term, as our model does not support absorption. Moreover, we replace the scattering function with one of the electron scattering functions described later in \autoref{phasefunc}:
\begin{equation} \label{erte}
E(x,\omega) = \int_{0}^{\infty} T(x,y) \left[ \mu(y)\int_{\Omega}f_E(\omega \leftarrow \bar{\omega})E(y, \bar{\omega}) \mathrm{d}\bar{\omega}  \right] \mathrm{d}y,
\end{equation}
where \gls{tms} is a transmission between $x$ and $y$, $f_E$ is an electron scattering function and $\mu(y)$ is the scattering coefficient at point $y$. The \gls{tms} term corresponds to the probability that the particle will not encounter any collision on the path between points $x$ and $y$. We describe the used scattering functions in \autoref{phasefunc}.

\subsubsection{Scattering coefficient} \label{t:scatteringcoefficient}
The scattering coefficient $\mu$ is defined by the relation $\mu = \sigma \rho_n$, where $\rho_n$  is the number density in m$^{-3}$ describing how many matter particles there are in one volume unit. We also know the relation between the Coulomb potential and the total scattering section $\sigma$ from \autoref{totalcrosssectionequation}. Combined together with the localized version described in \autoref{c:framesize}, it yields:
\begin{equation}
\mu(c) = \rho_n(c,r) \int_{\Omega}  ( \frac{m}{2 \pi} )^2 \lvert \hat{V}_{c,r}(q) \rvert^2.
\end{equation}
Assuming an infinitely small radius of the Coulomb potential \gls{cpcr}, the term \gls{cpfcr} becomes a Fourier transform of a constant, which is a Dirac delta function:
$$
\mu(c) = \rho_n(c) \int_{\Omega}  ( \frac{m}{2 \pi} )^2 \lvert V(c) \delta_0(q) \rvert^2,
$$
where $V(c)$ is a Coulomb potenial at center $c$.
Now, we can bring out the constant terms from the integral and use the fact that integration over Dirac delta function is \num{1} to get a relation of the scattering factor and the Coulomb potential \gls{cp}:
\begin{equation}
\mu(c) = \rho_n(c) ( \frac{m}{2 \pi} )^2  V(c)^2.
\end{equation}
From our input, we cannot deduce the number density $\rho_n$ because we do not know which elements generated the Coulomb potential at the given point $c$. Therefore, we neglect the effects of this value and set $\rho_n=1$. This likely increases a scattering ratio of the less dense portions of the molecule and similarly reduces the scattering ratio of denser portions.

\subsection{Null-collision algorithms}
In \autoref{erte}, the \acrfull{rte} has been established. It must be taken into account that the Coulomb potential map of the molecule is heterogeneous, which complicates the computation of the transmission. There are several approaches for the computation of the radiation transport in heterogeneous media \citep{mcphysical}. One family of algorithms that solve the transmission estimation part of the \gls{rte} are null-collision algorithms. 
\par
The core step of the null-collision algorithms is homogenization of the heterogeneous volume by adding an artificial null volume, such that the scattering terms add up to the same constant scattering coefficient \gls{majorant} for each corresponding point of the homogenized volume, see illustration in \autoref{fig:homogenization}. The null volume does not cause any scattering, it has Dirac delta scattering function $f_N(\omega \leftarrow \bar{\omega}) = \delta(\omega-\bar{\omega})$. 
\par
By introducing the null volume, we have artificially increased the scattering events of each particle because of an increased majorant \gls{majorant} value. To compensate for that, we also proportionally decide whether the scattering happened or whether it was an artificial scattering, in which case we do not modify the particle properties. The effects cancel out and keep the physical plausibility of the \gls{rte}:
$$
-\mu_{n}(x)E(x,\omega) + \mu_{n}(x) \int_{S^2} \delta(\omega-\bar{\omega})E(x,\bar{\omega})\mathrm{d}\bar{\omega} = 0,
$$
where $\mu_{n}$ is the null portion of the scattering coefficient, brought in by the null volume \citep{mcphysical}. The equation can be easily proved by solving the integral for the Dirac delta scattering function. Inserting this formula into the \gls{rte} \autoref{erte} yields:
\begin{equation} \label{nullerte}
E(x,\omega) = \int_{0}^{\infty} T_{\bar{\mu}}(x,y) \left[ \mu(y)\int_{\Omega}f_E(\omega \leftarrow \bar{\omega})E(y, \bar{\omega}) \mathrm{d}\bar{\omega} + \mu_{n}(y)E(y,\omega) \right] \mathrm{d} y,
\end{equation}
where $\mu_n(y) = \bar{\mu} - \mu(y)$ represents the ratio of null collisions at the point $y$. 
\par

\begin{figure}[ht]
\centering
\includegraphics[width=0.7\linewidth]{../img/homogenization.png}
\caption{The image illustrates homogenization of the heterogeneous volume (blue). At each position within the considered volume bounds, the input volume and the null volume add up to the same majorant \gls{majorant}. Image taken from \cite{mcphysical}. }
\label{fig:homogenization}
\end{figure}

\subsection{Transmission estimator} \label{c:transmissionest}
The transmission Monte Carlo estimator for the null-collision \gls{rte} given in \autoref{nullerte} has the following form:
\begin{equation} \label{estimator0}
\langle E(x,\omega) \rangle 
= \frac{T_{\bar{\mu}}(x,y)}{pdf(x,y)}
\left[ \mu(y)\int_{\Omega}f_E(\omega \leftarrow \bar{\omega})E(y, \bar{\omega}) \mathrm{d}\bar{\omega} + \mu_{n}(y)E(y,\omega) \right],
\end{equation} 
where $pdf(x,y)$ is the probability of picking the point $y$, or alternatively, picking a distance from $x$ to $y$. We further simplify the estimator evaluation via delta-tracking in the following subsection.

\subsubsection{Delta-tracking} \label{c:deltatracking}
Delta-tracking algorithm makes samples per the homogenized transmission term $T_{\bar{\mu}}$. For elastically scattered electron, the transmission term $T(t)$ corresponds to the probability that the electron will not scatter within distance $t$, i.e.,
$$ 
T(t) = P(X > t) = e^{ -\int_0^t \mu(s) ds }.
$$ 
From that, we can derive the probability function that the electron scatters within distance $t$: 
$$
F(t) = P(X \leq t) = 1 - T(t) = 1 - e^{ -\int_0^t \mu(s) ds}. 
$$
The function $F(t)$ corresponds to the cumulative distribution function for distance sampling. Before we can make it into a generator for path samples, we take advantage of the homogenized volume which has constant \gls{majorant} value everywhere. This simplifies the terms:
\begin{equation} \label{homotrans}
T_{\bar{\mu}}(t) = e^{ -\int_0^t \bar{\mu} ds } = e^{-t\bar{\mu}}
\end{equation}
and so:
$$
F_{\bar{\mu}}(t) = 1 - e^{ -\int_0^t \bar{\mu} ds } = 1 - e^{ -t\bar{\mu}}.
$$ 
By inverting this formula, we get the recipe for generating samples with the correct distribution within the homogenized volume:
$$
t = F_{\bar{\mu}}^{-1}(\xi) = - \frac{1}{\bar{\mu}} ln(\xi),
$$
where $\xi$ is a uniformly distributed random number between $0$ and $1$. We also need the \gls{pdf} of the generated samples, which is:
\begin{equation} \label{transpdf}
pdf(t) = \frac{\mathrm{d}}{\mathrm{d} t} 1 - e^{ -t\bar{\mu}} = \bar{\mu} e^{ -t\bar{\mu}}.
\end{equation}

Let $t$ be the distance between points $x$ and $y$. By plugging the derived formulae from \autoref{homotrans} and \autoref{transpdf} into the estimator described by \autoref{estimator0}, we can further simplify it:
\begin{equation}
\begin{aligned}
\langle E(x,\omega) \rangle 
= \frac {e^{-t\bar{\mu}}} {\bar{\mu} e^{ -t\bar{\mu}}}
\left[ \mu(y)\int_{\Omega}f_E(\omega \leftarrow \bar{\omega})E(y, \bar{\omega}) \mathrm{d}\bar{\omega} + \mu_{n}(y)E(y,\omega) \right] = \\
\frac{\mu(y)}{\bar{\mu}} \int_{\Omega}f_E(\omega \leftarrow \bar{\omega})E(y, \bar{\omega}) \mathrm{d}\bar{\omega} + \frac{\mu_{n}(y)}{\bar{\mu}} E(y,\omega),
\end{aligned}
\end{equation}
where $ \mu(y) + \mu_{n}(y) =  \bar{\mu}$. Since we want to trace only a single electron and not both the scattering and null-collision interactions at the same time, we estimate also the inner terms by randomly choosing which branch to evaluate:
\begin{equation} \label{dtestim}
\langle E(x,\omega) \rangle = \begin{cases}
\int_{\Omega}f_E(\omega \leftarrow \bar{\omega})E(y, \bar{\omega})\mathrm{d}\bar{\omega}       &\mbox{if } \varepsilon_1 \leq \frac{\mu(y)}{\bar{\mu}}  \\
E(y,\omega)   				                                                      &\mbox{if } \varepsilon_1 > \frac{\mu(y)}{\bar{\mu}}.
\end{cases} 
\end{equation}
The $\varepsilon_1$ term is a  random sample in range $[0,1]$ with a uniform distribution. It corresponds to a random decision whether to follow the null-scattering or real scattering function.

\subsection{Scattering estimator} \label{phasefunc}
In the previous section we have derived a delta-tracking transmission estimator (see \autoref{dtestim}) for our \gls{rte} \autoref{erte}. In this section, we provide two viable electron scattering functions and develop MC estimator for the scattering term:
$$
 \int_{\Omega}f_E(\omega \leftarrow \bar{\omega})E(y, \bar{\omega}) \mathrm{d}\bar{\omega}.
$$
The scattering functions are the Rutherford scattering function and the \gls{dcs} scattering function.

\subsubsection{Rutherford scattering function} \label{t:rutherfordsampling}
We have described an analytical formulation of the electron \gls{dcs} for single atom via Rutherford formulae in \autoref{t:rutherfordformulae}. We can turn this differential equation into a \acrlong{pdf} of $\cos\theta$ via normalization:
\begin{equation}\label{rutherpdf}
pdf(\cos\theta) = \frac {Z^2 r_e^2 \left( \frac{1-\beta^2}{\beta^4}   \right) \frac{1}{(1 - \cos\theta + 2\eta)^2} } {\sigma} = \frac {2\eta(\eta+1)} {(1 - \cos\theta + 2\eta)^2}.
\end{equation}
We integrate \autoref{rutherpdf} over the interval $[-1, \, \cos\theta]$ in order to derive cumulative distribution function:
\begin{equation}\label{ruthercumu}
\int_{-1}^{\cos\theta}  \frac {2\eta(\eta+1)} {(1 - \cos\theta + 2\eta)^2} \mathrm{d}\cos\theta = \frac {\eta \cos\theta + \eta} {2\eta - \cos\theta +1}.
\end{equation}
Finally, by inverting the cumulative distribution function in \autoref{ruthercumu}, we get a formula for sampling $\cos\theta$ proportionally to the \gls{dcs}:
\begin{equation}
\cos\theta = \frac{2\eta\varepsilon - \eta + \varepsilon}{\eta+\varepsilon},
\end{equation}
where $\varepsilon$ is a uniformly distributed random sample between $0$ and $1$. Because we sample $\bar{\omega}$ proportionally to $f_E$, then $\dfrac{f_E(\omega \leftarrow \bar{\omega})}{pdf(\bar{\omega})}=1$, and we can omit the term in the MC estimator. The estimation formula from \autoref{dtestim} simplifes to:
\begin{equation}
\langle E(x,\omega) \rangle = \begin{cases}
E(y, \bar{\omega})       &\mbox{if } \varepsilon_1 \leq \frac{\mu(y)}{\bar{\mu}}  \\
E(y,\omega)              &\mbox{if } \varepsilon_1 > \frac{\mu(y)}{\bar{\mu}}.
\end{cases} 
\end{equation}

\subsubsection{DCS scattering function} \label{t:phasefuncdcs}
In this section, we derive the second viable scattering function from the \gls{dcs} \autoref{eedcs}. By normalizing \autoref{eedcs} with total cross section, we can derive the scattering function:
\begin{equation} \label{dirpdf}
 pdf(p') = f_E(p' \leftarrow p) =  \frac {  \frac{\mathrm{d} \sigma}{\mathrm{d} \Omega} (p' \leftarrow p)  } {\sigma} = \frac {( \frac{m}{2 \pi} )^2 \lvert \hat{V}(q) \rvert^2}{\sigma} .
\end{equation}
Note that we use the localized version of the Coulomb potential, to emphasize that the function depends on the position within the volume. If we wanted to sample this function directly, we would have to integrate its \gls{pdf} to get the cumulative distribution function, and then invert it, as we did for the distance sampling. However, this approach is not viable for this scattering function.
\par
One solution would be to somehow precompute the values and then sample the acquired discrete distribution. But there is a problem with aliasing, which the discrete distribution will very likely cause unless we do a very fine discretization, which would require a substantial amount of memory. Another problem is the function dependency on the position within the Coulomb potential map. We would have to have the precomputed samples for every position of that volume. However, we can also use the importance sampling technique, which is a common approach in computer graphics.

\subsubsection{Importance sampling of DCS}
The idea of importance sampling is to draw samples from a distribution that is proportional to our scattering function $f_E$. This allows us to use scattering function that we cannot explicitly sample. The better the correspondence between the sampling function and the scattering function, the less variance we get in our estimations. With importance sampling, the estimator is:
\begin{equation}
\langle E(x,\omega) \rangle = \begin{cases}
\frac{f_E(p' \leftarrow p)}{pdf_S(\bar{\omega})} E(y, \bar{\omega})     										 &\mbox{if } \varepsilon_1 \leq \frac{\mu(y)}{\bar{\mu}}  \\
E(y,\omega)                                    																	    &\mbox{if } \varepsilon_1 > \frac{\mu(y)}{\bar{\mu}}.
\end{cases} 
\end{equation}
where $f_E$ is the \gls{dcs} scattering function and $f_S$ is the sampling function with the probability density function $pdf_S$. In our implementation, we used the Rutherford fromula as $f_S$ to importance sample the \gls{dcs} scattering function.



