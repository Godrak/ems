\chapter{Optimizations}
In the previous chapter, we described the simplified microscope model and the corresponding Monte Carlo estimator for the elastic electron scattering simulation. However, we intentionally did not discuss some of the obstacles that arise during the theory to program transformation. In this chapter, we explore more in-depth implementation difficulties of the proposed models. We focus on the Fourier transform of the Coulomb potential. Next, we analyze the choice of the majorant and undesired properties of the input data. After that, we explore the mean free path and its impact on the rendering, and lastly, we inspect the implementation complications of the Rutherford scattering model.

\section{Units}
We use atomic units throughout our implementation, mainly the Bohr units [\bohr] for length.  While Angstrom [\angstrom] is usually used within the \gls{cryoem} reconstructions and research, atomic units simplify the physics and equations that describe the behavior of an electron within the potential field and thus are more convenient for us, as explained in \cite{vaclav}. The relation between Bohr and Angstrom is:
$$ 
\num{1}\ \bohr\ \approx\ \num{0.529177210903}\ \angstrom.
$$

\section{Fourier transform of the Coulomb potential} \label{t:localizedfourierpotential}
The elastic \gls{dcs} depends on the Fourier transform of the localized Coulomb potential \gls{cpcr} as described in \autoref{c:framesize}. To simplify the equations and implementation, we consider the location to be cube-shaped. The formal expansion of the Fourier transform of the potential in center $c$ and with radius $r$ is:
\begin{multline}
	\frac{\mathrm{d} \sigma}{\mathrm{d} \Omega} (p' \leftarrow p) = ( \frac{1}{2 \pi} )^2 \lvert \hat{V}_{c,r}(q) \rvert^2 = \\
	 ( \frac{1}{2 \pi} )^2 \int_{c_x-r}^{c_x+r} \int_{c_z-r}^{c_z+r} \int_{c_z-r}^{c_z+r} V(x,y,z) e^{-2\pi i q \left( (x-c_x)+(y-c_y)+(z-c_z) \right)} \mathrm{d}x \mathrm{d}y \mathrm{d}z.
\end{multline}
Note that we have replaced the electron mass with \num{1} thanks to the application of atomic units. After discretization of the integrals, we have the following formula for a discrere Fourier transform:
\begin{multline} \label{dft}
	\frac{\mathrm{d} \sigma}{\mathrm{d} \Omega} (p' \leftarrow p) = \\
	 ( \frac{1}{2 \pi} )^2 
	 \sum_{x = c_x-r \atop x+=s}^{c_x+r}  \sum_{y = c_y-r \atop y+=s}^{c_y+r} \sum_{z = c_z-r \atop z+=s}^{c_z+r} V(x,y,z) e^{-2\pi i q \left(  \frac{x-c_x}{2r}+\frac{y-c_y}{2r}+\frac{z-c_z}{2r} \right)},
\end{multline}
where $s$ is the sampling step size, which can be computed as $\frac{2r}{\#samples}$.
\par
There exist a number of implementations of the \gls{fft} algorithm, even for the 3D case presented here. However, we are only interested in frequencies that lie on a sphere, as depcited in \autoref{fig:dcssphere}. That is because for the given momentums $p$ and $p'$ we want the value for the frequency $q = p' - p$, where the magnitude of $p'$ and $p$ is the same. We would throw away most of the frequencies acquired from full 3D \gls{fft}.
\par
Also, to avoid Moiré patterns, the number of samples must be sufficiently high. For example, for electron with an energy of \num{300} \kilo\electronvolt, the magnitude of its momentum is \num{148.49} \si{\per\bohr}. The forward direction always corresponds with the zero frequency value. However, for larger scattering angles the corresponding frequencies increase rapidly. For the largest scattering angle (essentially reversing the direction of the electron) we need to compute the value of frequency of up to \num{2} $\times$ \num{148.49} \si{\per\bohr} $=$ \num{296.98} \si{\per\bohr} for this electron. Note that the frequency still has units.
\par
To correctly compute a value of a signal with such frequency, the sampling step must be half the wavelength of the signal, which is $\frac{1}{2\cdot{}296.98} \approx 0.0017$\ \bohr\ in this case. Because the radius values that we intend to use range from \num{1}\ \bohr\ to \num{16} \bohr, this leads to an enormous number of samples. At the same time, failing to respect this limit leads to undesired Moiré patterns in the undersampled frequencies and affects the weights of the importance sampling.
\par
Fortunately, from \autoref{fig:ruthevsnist} and from the experiments in \autoref{fig:dcsexamples}, we can observe that vast majority of electrons do not scatter to angles wider than approximately \num{4} degrees. Based on this observation, we have done the following steps:
\begin{itemize}
  \item First, we have implemented the Fourier transform as described by the \autoref{dft} above. Compared to the standard discrete \gls{fft} algorithm, this allows us to correctly represent any frequency up to the precision of the numeric type. We also do not have to compute the values for frequencies outside those that are of interest. The algorithm is simpler than \gls{fft} and can be executed within a single CUDA thread. Lastly, with importance sampling, it is sufficient to compute only a single frequency, which is faster.
  \item Second, we parse the number of samples per axis and the locality radius of the Fourier transform from the input parameters of the simulator. We then compute the maximum scattering angle that we can correctly represent with this setup and restrict the importance sampling accordingly.
\end{itemize}

\section{Majorant choice}
Before we discuss how to choose the majorant value, we first need to acquire a volume that is used for distance sampling, as described in \autoref{c:transmissionest}. As we have declared in \autoref{c:input}, the input to our simulator is a map representing a Coulomb potential. In \autoref{t:scatteringcoefficient}, we have derived the relation between scattering coefficient and the Coulomb potential. To get a volume for rendering, we take the input potential map and transform it by the derived relation, acquiring a map of scattering coefficients.
\par
Setting the maximum value of the generated scattering coefficients volume as majorant is not ideal. We can expect outliers in both the reconstructed data, and also artificially generated data because the equations for the atom Coulomb potential diverge to infinity at zero distance from the atom. Thus, there would be a very high chance that the maximum value of the volume is an outlier, resulting in low scattering tendency everywhere else in the volume and low contrast in the final image.

\subsection{Element estimation} \label{t:elemestim}
To avoid this problem, we take advantage of a chemical composition of the molecule known from e.g. spectroscopy. We assume that the largest scattering coefficient belongs to the atoms with the highest atomic number - an assumption that seems to be correct from the behavior of the Rutherford formula. We compute the percentual representation of this element in the studied molecule and choose the lowest scattering coefficient from the volume, which is still within the percentile of the largest atomic number. The percentile is set manually through the parameters and recommended values are above \num{0.99}. For example, the $\beta$-galactosidase molecule used in the experiments has a chemical distribution shown in \autoref{fig:chembeta}. The highest atomic number is Sulfur, so we use as the majorant a value with $1-\frac{0.474834}{100} \approx 0.995$ percentile. This is a heuristic approach and it would not work for molecules where the highest atomic number is extensively present, so we advocate to use a reasonably high percentile.


\begin{table}[ht]
\centering
\begin{tabular}{l|c|c|c}
Element & Atomic number & Count & Percentage \\
\hline
Carbon- C    & 6  & 20816& 61.7759  \\
Oxygen- O    & 8  & 6896 & 20.4653  \\
Nitrogen- N  &  7 & 5808 & 17.2365  \\
Sulfur- S    & 16 & 160  & 0.474834  \\
Sodium- Na   &  11& 8    & 0.0237417 \\
Magnesium- Mg&  12& 8    & 0.0237417 \\
\end{tabular}
\caption{Element distribution of the $\beta$-galactosidase molecule. }
\label{fig:chembeta}
\end{table}

\subsection{Mean free path} \label{t:meanfreepath}
Another problem tied with the majorant scattering coefficient is choosing the sampling mean free path. In classic delta-tracking, the majorant scattering coefficient also determines the sampling mean free path via the following formula $\lambda = \frac{1}{\bar{\mu}}$. However, as declared in \autoref{c:input}, we cannot rely on the scale of the input values. Even though we have chosen a majorant scattering coefficient, plugging it into the path estimation described in \autoref{c:transmissionest} would return incorrect values.
\par
To solve this problem, we re-use the element estimation. From the approach in \autoref{t:elemestim}, we have established which element represents the majorant scattering coefficient. From the Rutherford formula, we can compute the realistic total cross-section for the estimated majorant element. We can then use this realistic total cross-section to compute a realistic mean free path from the following formula from \autoref{t:totalcrosssection}:
\begin{equation}
\lambda = \frac{1}{\rho_n \sigma}.
\end{equation}
From the knowledge of the estimated majorant element, we can compute the density of the scattering centers $\rho_n$:
$$
\rho_n = \frac{N_A\rho}{A},
$$
where $N_A$ is the Avogadro constant, $\rho$ is the weight density of the element and $A$ is the atomic weight \citep{meanfreepathbook}. 
\par
By associating the mean free path with a majorant scattering coefficient, instead of computing it, we essentially normalized the volume. It does not matter what the scale of the values in the input molecule is, because the sampling mean free path will always be determined by the estimated majorant element. However, it also overloads the meaning of the majorant value - now it does not influence only the noise of the final image, but also the contrast of the image, because of its association with a set mean free path.

\subsubsection{Unscattered dose propagation} \label{t:dose}
The typical elastic mean free paths of high energy electrons within \gls{cryoem} are several times longer than the size of the molecule. This means that the vast majority of electrons do not interact with the specimen and contribute only to the background of the image. To save computational time, we do not simulate these electrons explicitly. Instead, we compute the unscattered dose from the cumulative distribution function for distances described in \autoref{c:deltatracking}. Let $t$ be the distance from the emitter to the detector, then the probability that the electron will not scatter is:
$$
P(X > t) = e^{\frac{t}{\lambda}}.
$$
The detector is initialized with the unscattered dose before the simulation. To keep the estimator unbiased, we have to compensate for this by sampling the first scattering distance only within the distance $t$. For distance sampling, we use the following formula:
$$
s = -\lambda\ln(1-\varepsilon),
$$
where $s$ is the sampled distance and $\varepsilon$ is a random number with uniform distribution. To restrict the sampling to distances between $0$ and $t$, we restrict the $\varepsilon$ to the range $[0, 1-e^{\frac{-t}{\lambda}}]$.

\section{Rutherford scattering model}
In \autoref{phasefunc}, we have described the Rutherford scattering function and how to utilize it in the estimator. However, the Rutherford formulae from \autoref{t:rutherfordformulae} are derived for a single atom of a known type, not for a potential map or scattering coefficients map. We tried to use the approach from \autoref{t:elemestim} again and during scattering within the volume, we estimated the atomic number of the local atom based on the local scattering coefficient and the majorant scattering coefficient, for which we know the element it represents. Unfortunately, this did not work very well because the estimated scattering angles were too wide. In the experiments in \autoref{t:experimentschapter}, we show that a good approximation model is to simply use one Rutherford formula for all scattering events.


