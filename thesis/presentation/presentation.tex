\documentclass[12pt,xcolor=dvipsnames]{beamer}

\usetheme{Madrid}
%\usetheme{CambridgeUS}
\usepackage[dvipsnames]{xcolor}
\usecolortheme[named=BrickRed]{structure}
\setbeamertemplate{navigation symbols}{}
\setbeamercovered{invisible}
\setbeamersize{text margin left=1.9em}
\setbeamersize{text margin right=1.9em}
\usepackage{pgfpages}
\pgfpagesuselayout{resize to}[a4paper,,border shrink=5mm,landscape]
\usepackage[utf8]{inputenc}
\usepackage{multirow}
\pdfmapfile{+sansmathaccent.map}

\usepackage{subfig}

\newcommand{\abs}[1]{\left|{#1}\right|}
\newcommand{\norm}[1]{\lVert{#1}\rVert}
\setcounter{MaxMatrixCols}{15}

\title[Image formation via electron tracing]{Simulating image formation in an electron microscope by electron tracing}

\author[P. Mikuš et al.]{Bc. Pavel Mikuš \inst{1} \and Mgr. Tomáš Iser \inst{1} \and Mgr. Václav Alt\inst{2,4} \and Mgr. Eva Havelková \inst{3,4} \and Mgr. Lukáš Maršálek \inst{4}}
\institute[]{\inst{1} Computer Graphics Group,\newline Faculty of Mathematics and Physics, Charles University in Prague \and \inst{2}  Institute of Theoretical Physics,\newline 
Faculty of Mathematics and Physics, Charles University in Prague \and \inst{3}  Department of Numerical Mathematics,\newline Faculty of Mathematics and Physics, Charles University in Prague \and \inst{4} Eyen SE}
\date{2021, Prague}

\begin{document}

\frame{\maketitle}

\definecolor{MGray}{RGB}{100, 118, 135}


\begin{frame}{Cryo-Electron Microscopy}
\begin{itemize}
\begin{footnotesize}
\item Very thin sample with target molecules.
\item Sample is rapidly frozen and studied under cryogenic temperatures.
\item Parallel beam of electrons generates images.
\item Projections used to reconstruct Coulomb potential.
\end{footnotesize}
\end{itemize}

\begin{figure}
\centering
\includegraphics[width=4cm]{../img/projection.png}
\end{figure}
\end{frame}



\begin{frame}{Electron microscope}
\begin{columns}

\begin{column}{0.5\textwidth}
    
High energy (300 keV) electrons for atomic resolution. \newline

High radiation damage $\rightarrow$ \newline 
low electron dose $\rightarrow$ \newline noise in the images.

\end{column}

\begin{column}{0.57\textwidth}

\begin{figure}
\centering
\includegraphics[height=6cm]{../img/emschema.png}
\caption{ \begin{footnotesize}
Electron microscope schema
\end{footnotesize} }
\end{figure}
\end{column}

\end{columns}
\end{frame}




\begin{frame}{Simplified setup}

\begin{figure}[b]
\centering
\includegraphics[width=6cm]{../img/simpleschema.png}
\end{figure}
\vskip-2em
\begin{itemize}
\begin{footnotesize}
\item Perfect virtual detector.
\item Perfect parallel electron beam.
\item Volumetric data of single molecule.
\end{footnotesize}
\end{itemize}

\end{frame}





\begin{frame}{Alternative models: Transmittance model}
\begin{footnotesize}
\begin{itemize}
\item Absorption based model.
\item Not grounded in real behavior of electrons.
\item Fast, invertible.
\end{itemize}
\end{footnotesize}
\begin{figure}[b]
\centering
\includegraphics[width=5cm]{../img/transmittance.png}
\caption{ \begin{footnotesize}
Each ray computes an integral of potential values on its path. \end{footnotesize}}
\end{figure}
\end{frame}




\begin{frame}{Alternative models: Multislice model}
\begin{footnotesize}
\begin{itemize}
\item Derived as a solution to the Schrödinger equation.
\item Wave-function based model - hard to interpret with electrons as particles.
\end{itemize}
\end{footnotesize}
\begin{figure}[b]
\centering
\includegraphics[width=4cm]{../img/multislice.png}
\caption{ \begin{footnotesize}
Processed by slices, convolution at each step.
\end{footnotesize} }
\end{figure}

\end{frame}


\begin{frame}{Electron interactions}

Events:
\begin{figure}[b]
\centering
\includegraphics[width=6cm]{../img/elinteraction.png}
\end{figure}

\begin{footnotesize}
\begin{itemize}
\item Elastic scattering $\rightarrow$ small angles, information.
\item Inelastic scattering $\rightarrow$ radiation damage, secondary effects.
\end{itemize}
\end{footnotesize}
$\rightarrow$ Focus only on the elastic scattering.

\end{frame}




\begin{frame}{Problem formulation}
Radiative transfer equation with scattering only:
\begin{equation} \label{erte}
E(x,\omega) = \int_{0}^{\infty} T(x,y) \left[ \mu(y)\int_{\Omega}f_E(\omega \leftarrow \bar{\omega})E(y, \bar{\omega}) \mathrm{d}\Omega  \right] \mathrm{d}y
\end{equation}
\begin{itemize}
\begin{footnotesize}
\item \textcolor{Gray}{$E(x,\omega)$ is a number of electrons incoming to point $x$ from a solid angle $\omega$, $\Omega$ is a solid angle, 
$T(x,y)$ is a transmission between $x$ and $y$, $f_E$ is an electron phase function, $\mu(y)$ is a scattering coefficient at point $y$.}
\end{footnotesize}
\end{itemize}

\begin{itemize}
\item Sampling outer integral $\leftrightarrow$ sampling paths
\item Sampling inner integral $\leftrightarrow$ sampling scattering directions
\end{itemize}

\end{frame}




\begin{frame}{Delta tracking}

MC estimator for the previous formula:
\begin{equation} \label{dtestim}
\langle E(x,\omega) \rangle = \begin{cases}
\int_{\Omega}f_E(\mu \leftarrow \bar{\mu})E(y, \bar{\mu})\mathrm{d}\Omega       &\mbox{if } \varepsilon_1 =< \frac{\mu(y)}{\bar{\mu}}  \\
E(y,\omega)      			                                                    &\mbox{if } \varepsilon_1 > \frac{\mu(y)}{\bar{\mu}}.
\end{cases} 
\end{equation}
\begin{footnotesize}
\begin{itemize}
\item $\bar{\mu}$ is the majorant scattering coefficient.
\end{itemize}
\end{footnotesize}


\begin{figure}
\centering
\includegraphics[width=6cm]{../img/homogenization.png}
\caption{ \begin{footnotesize}
Homogenized volume = real volume + null volume.
\end{footnotesize} }
\end{figure}

\end{frame}




\begin{frame}{Elastic scattering models}
\vskip-1em
Elastic scattering differential cross section:
\begin{equation}
	\frac{\mathrm{d} \sigma}{\mathrm{d} \Omega} (p' \leftarrow p) = ( \frac{m}{2 \pi} )^2 \lvert \color{Red} {\hat{V}_{c,r}(q) } \rvert^2
\end{equation}
\vskip-1em
\begin{footnotesize}
\begin{itemize}
\item Physically correct elastic scattering in Coulomb potential.
\item \color{Red}{Local Fourier Transform} $\rightarrow$ expensive evaluation.
\end{itemize}
\end{footnotesize}
\vskip1em
Rutherford formula:
\begin{equation}
\frac{\mathrm{d} \sigma}{\mathrm{d} \Omega} = Z^2 r_e^2 \left( \frac{1-\beta^2}{\beta^4}   \right) \frac{1}{(1 - cos\theta + 2\eta)^2 }
\end{equation}
\vskip-1em
\begin{footnotesize}
\begin{itemize}
\item Formula for scattering of single atom.
\item Analytical sampling formula can be derived!
\end{itemize}
\end{footnotesize}

$\rightarrow$ \color{MGray}{ Importance sampling and approximation model.}

\end{frame}




\begin{frame}{PDF of scattering models}
\vskip-1em
\begin{figure}[htb]
    \centering
    \subfloat[]
    {
        \includegraphics[width=0.2\textwidth]{../img/dcs1.png}
        \label{fig:first_sub}
    }
    \subfloat[]
    {
        \includegraphics[width=0.2\textwidth]{../img/dcs2.png}
        \label{fig:second_sub}
    }
    \subfloat[]
    {
        \includegraphics[width=0.2\textwidth]{../img/dcs3.png}
        \label{fig:third_sub}
    }
      \subfloat[]
    {
        \includegraphics[width=0.2\textwidth]{../img/dcs4.png}
    }
    \caption{Forward direction PDF of DCS, 6 degrees from center to side. }
\end{figure}

\vskip-1em

\begin{figure}[htb]
    \centering
    \subfloat[Z1]
    {
        \includegraphics[width=0.2\textwidth]{../img/exp/ruther1.png}
        \label{fig:first_sub}
    }
    \subfloat[Z6]
    {
        \includegraphics[width=0.2\textwidth]{../img/exp/ruther6.png}
        \label{fig:second_sub}
    }
    \subfloat[Z8]
    {
        \includegraphics[width=0.2\textwidth]{../img/exp/ruther8.png}
        \label{fig:third_sub}
    }
      \subfloat[Z16]
    {
        \includegraphics[width=0.2\textwidth]{../img/exp/ruther16.png}
    }
    \caption{Forward direction PDF of Rutherford, similar setup. }
\end{figure}

\end{frame}


\begin{frame}{Implementation}
\begin{footnotesize}
\begin{itemize}
\item C++/CUDA
\item Both CPU only and GPU accelerated mode (BoltView).
\item Flexible design - different models via single framework.
\end{itemize}
\end{footnotesize}

\begin{figure}
\centering
\includegraphics[width=9cm]{ems.png}
\caption{ \begin{footnotesize}
Simulation pipeline \newline 
\color{MGray} {Gray parts fixed but configurable.} \newline
\color{Red} {Red components replaceable - ``duck typing'' interface.}
\end{footnotesize} }
\end{figure}



\end{frame}



\begin{frame}{Simulated images comparison}

\begin{figure}[htb]
    \centering
    \subfloat[DCS]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_r4.png}
    } 
    \subfloat[Rutherford]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/rproj_s1.png}
    } 
    \subfloat[Multislice]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_multislice.png}
    }
    \subfloat[Transmittance]
    {
    \includegraphics[width=0.23\textwidth]{../img/exp/proj_int.png}
    }
    \caption{ Simulated images of artificially reconstructed Coulomb potential of molecule. \newline (Transmitance model has inverted colors - the information has negative sign.)} 
\end{figure}

\end{frame}



\begin{frame}{Real data}
\vskip-2em
\begin{figure}[]
    \subfloat
    {
        \includegraphics[width=0.23\textwidth]{../img/exp/proj42_16.png}
    }
    \subfloat
    {
        \includegraphics[width=0.23\textwidth]{../img/exp/proj42_56.png}
    }
    \subfloat
    {
        \includegraphics[width=0.23\textwidth]{../img/exp/proj42_63.png}
    }
      \subfloat
    {
        \includegraphics[width=0.23\textwidth]{../img/exp/proj42_73.png}
    }
     \caption{Examples of real measured projections.}
     
    \subfloat[class10]
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/class10.png}
    } 
    \subfloat[class38]
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/class38.png}
    } 
    \subfloat[class42]
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/class42.png}
    }
    \subfloat[class50]
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/class50.png}
    }
     \caption{Class average: average of many projections from similar angle.}
\end{figure}

\end{frame}




\begin{frame}{Class42 simulation}
Comparison of DCS, Rutherford and transmitance model with measured class average. {\color{Gray}{(Multislice model not used.) }}

\begin{figure}[htb]
    \centering
    \subfloat[class42]
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c42m.png}
    }
    \subfloat[DCS]
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c42d.png}
    } 
    \subfloat[Rutherford]
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c42r.png}
    } 
    \subfloat[Transmittance]
    {
    \includegraphics[width=0.23\textwidth]{../img/cmp/c42i.png}
    }
    \caption{ Simulated averages from various models side by side. } 
\end{figure}

\end{frame}




\begin{frame}{Normalized cross correlation}

\begin{figure}[b]
\centering
\includegraphics[width=8cm]{datagraph.png}
\caption{ \begin{footnotesize}
Normalized cross correlation of the simulated averages with the real data of each of the four class averages.
\end{footnotesize} }
\end{figure}

\end{frame}






\begin{frame}{QA time}

\begin{center}
\begin{large}
\textbf{Thank you for your attention.}
\end{large}
\end{center}

\end{frame}





\begin{frame}{Performance1}

\begin{figure}[b]
\centering
\includegraphics[width=10cm]{time240.png}
  \caption{ Logarithmic scale on both axes. Size of the volume 240 voxels per side, time in milliseconds, dose in electrons per Bohr squared. GeForce GTX 1050 Ti Mobile.} 
\end{figure}

\end{frame}


\begin{frame}{Performance2}

\begin{figure}[b]
\centering
\includegraphics[width=10cm]{time360.png}
\caption{ Logarithmic scale on both axes. Size of the volume 360 voxels per side, time in milliseconds, dose in electrons per Bohr squared. GeForce GTX 1050 Ti Mobile. } 
\end{figure}

\end{frame}


\begin{frame}{Examples}
\begin{figure}[htb]
    \centering 
    \subfloat[12] 
    {
    \includegraphics[width=0.48\textwidth]{../img/exp/proj_d12.png}
    }
    \subfloat[120] 
    {
    \includegraphics[width=0.48\textwidth]{../img/exp/proj_d120.png}
    }
    \caption{ \begin{footnotesize}
Projections from the Rutherford model at various electron doses (in electrons per Bohr squared).
\end{footnotesize} }
\end{figure}

\end{frame}



\begin{frame}{Expected amount of scattering events}
\begin{footnotesize}
\begin{itemize}
  \item Energy: $300$ keV $\rightarrow$ elastic mean free path in Sulfur: $3840.50$ Bohr.
  \item Size of the molecule from tests: $\approx 700$ Bohr.
  \item Fraction of unscattered particles: $0.770442$.
  \item Dose: $12$ electrons per bohr $\rightarrow$ unscattered dose: $9.2453$, scattered dose: $2.7547$.
  \item Simulation of the scattered dose only (biased distance sampling).
  \item The unscattered dose directly written to the detector.
\end{itemize}
\end{footnotesize}


\end{frame}




\end{document}




















