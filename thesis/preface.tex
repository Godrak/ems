\chapter*{Preface}
\addcontentsline{toc}{chapter}{Preface}

\section*{Introduction to cryo-EM}
\Gls{cryoem} is an evolving field that allows visualization of three-dimensional structures of living organisms at nearly atomic resolutions. Structural information of various molecules and proteins at all scales is necessary to understand how different components of the living organisms interact and what their purpose is. The use of electron microscopes and the ability to reconstruct large macromolecules at atomic levels revolutionized molecular biology \citep{cyoemadvances}.

\subsection*{Electron microscope} \label{c:microscope}
The first electron microscope was constructed in the 1930s by Ernst Ruska. Compared to the standard light microscope, an electron microscope has superior resolution due to the very short wavelength of high energy electrons (about \num{2}-\num{5}~\pico\metre) compared to the wavelength of the visible light (\num{400}-\num{700}~\nano\metre). The light microscope can achieve resolution of only \num{0.2}~\micro\metre, while the state-of-the-art electron microscopes have resolution of \num{50}~\pico\metre~$=$~\num{0.00005}~\micro\metre \  \citep{vulovic}. In theory, electron microscopes can achieve even smaller resolutions. However, there are limitations in the lens aberrations and small numerical aperture. \autoref{fig:emschema} shows a generic schema of an electron microscope. In the following paragraph, we provide a high level description of the microscope components adopted from \citep{microscopy}:

\begin{figure}[ht]
\centering
\includegraphics[width=0.5\linewidth]{../img/emschema.png}
\caption{The schema of an electron microscope. Individual components are briefly described in the \nameref{c:microscope} section. Image was taken from \cite{electronschema}.}
\label{fig:emschema}
\end{figure}

\begin{description}
  \item[Electron source] Electron source is usually an electron gun - a sharp piece of metal with high current going through it. 
  \item[Condenser lens] In an electron microscope, lenses are electromagnetic coils. The stronger the magnetic field, the shorter the focal length. Condenser lens focuses the emitted beam of electrons onto the sample.
  \item[Condenser aperture] Limits the cone angle of the electrons that can interact with the sample. It works in conjunction with the condenser lens to eliminate high angle electrons.
  \item[Sample] Sample consists of a tiny lattice covered by a water-based solution with the target molecule. The sample is frozen to cryogenic temperatures before insertion into the microscope.
  \item[Objective lens] Objective lens is the strongest and most important lens of the whole system. It generates a highly magnified first intermediate image, which determines the resolution and quality of the final image.
  \item[Objective aperture] Similarly to the condenser aperture, the objective aperture limits which electrons contribute to the final image.
  \item[Selected area aperture] Selected area aperture is a second aperture after the sample. It selects which part of the sample is projected.
  \item[Intermediate lens and projective lens] Final set of lenses, which further magnifies the first intermediate image and finally projects it on the screen of the detector.
\end{description}

\subsection*{Data acquisition}
Electron microscopes are utilized in a variety of scientific fields. In this thesis, we focus on \gls{spa} for the description of the electron microscope utilization. However, our simulation algorithm is based on general electron interactions and thus has a potential to be usable in other fields as well. In the following paragraphs, we describe the general idea of \gls{spa} and the role of the electron microscope.
\par
During image acquisition, electrons are emitted from the electron gun in various directions. The first pair of lenses bends the electrons into a coherent beam and focuses them on the sample. Those electrons that were not succesfully focused are stopped by the condenser aperture. After interacting with the sample, the electron beam is transmitted through several lenses to achieve a high magnification factor, and finally measured on the detector. Electrons that scattered too much away are stopped by the objective aperture.
\par
High energy electrons have very short wavelengths and thus can provide a very fine resolution. This requires that the microscope operates in a high vacuum, otherwise the electrons would interact with air molecules, adding a substantial amount of noise to the projected image. However, biological samples are not stable in a vacuum environment. Other difficulties arise in a form of a constant thermodynamic movement of the sample molecules and radiation damage from the electrons. For these reasons, after applying the purified molecules to a metal grid, the sample is rapidly frozen in liquid ethane \citep{cryoemoverview}. This protects the sample from the vacuum and prevents the movement. 
\par
During illumination by the electron beam, 2D projections of the sample are acquired. The acquired images are called micrographs and each micrograph contains hundreds of projections of the target molecule from various directions, as shown in \autoref{fig:projection}. However, the micrographs (example is shown in \autoref{fig:micrograph}) suffer from a low signal to noise ratio due to low suitable radiation dose. Further software processing is required to extract the desired information from the images. 

\begin{figure}[ht]
\centering
\includegraphics[width=0.7\linewidth]{../img/projection.png}
\caption{Image displays a sample with many instances of the target molecule. The sample is then projected on the screen of the microscope detector, forming a micrograph. The next step is then software processing of the micrographs and 3D reconstruction of the target molecule. Image taken from \cite{projectionschema}.}
\label{fig:projection}
\end{figure}

\begin{figure}[ht]
\centering
\includegraphics[width=\linewidth]{../img/micrograph.png}
\caption{An example of a micrograph with picked projections of the target molecule. The noise level is very high because of the low electron dose. Higher doses would destroy the molecules, yielding invalid images (Dataset 10013 from EMPIAR \citep{empiar}) }
\label{fig:micrograph}
\end{figure}

\subsection*{Reconstruction} \label{c:reconstruction}
After a sufficient amount of data has been acquired, it needs to be computationally processed. First, the projections are picked from the micrographs using templates and other classification algorithms. Next, the new dataset of molecule projections is used in an iterative process with two steps taking turns. One step is projecting the reconstructed potential, computing the residuum, and updating the potential map. The second step is direction and shift estimation of the projections, as we do not know these properties from micrographs. For further details about the process see \citep{bayesian}.

\subsection*{Modeling the projections}
In order to simulate the projections of the sample in the electron microscope, two different models are commonly used: the transmittance model and the multislice model.

\subsubsection*{Transmittance model} \label{c:xray}
The transmittance model is the most common model used during the reconstruction process \citep{evaregularization}. It takes the potential map as an input and generates its 2D projection. It does so by tracing a parallel beam of rays from a given direction, integrating the potential map along each ray. The process is illustrated in \autoref{fig:transmittance} and its result approximates the transmittances through the sample along the direction. This model is also known as the X-ray transform.


\begin{figure}[ht]
\centering
\includegraphics[width=0.5\linewidth]{../img/transmittance.png}
\caption{Illustration of the transmittance model projection in 2D. TThe projection P is generated by integrating volume f over a set of parallel rays. Image taken from \citep{transmittanceschema}. }
\label{fig:transmittance}
\end{figure}


\subsubsection*{Multislice model} \label{c:multislice}
The multislice model is the state-of-the-art model for electron microscope simulation. It utilizes an electron wave formalism to simulate the behavior of an electron within a potential map. In the first step, the potential map is divided along the projection axis into several slices that are then sequentially processed. The thinner the slices, the more precise the method, as it is derived on a basis of infinitely thin slices. The next step is the electron wave propagation through the slices, where at each step the wave is attenuated by the processed slice. For details see \citep{vulovic}. The iterative process is depicted in \autoref{fig:multisliceschema}. To our knowledge, this model is not currently used for the structural reconstructions.

\begin{figure}[ht]
\centering
\includegraphics[width=0.5\linewidth]{../img/multislice.png}
\caption{Illustration of the iterative process of the multislice model. The input volume is divided into a number of slices marked by the bold lines. A wave function is propagated slice by slice through the volume, performing forward and inverse Fourier transforms in each step. Image taken from \cite{multisliceschema}. }
\label{fig:multisliceschema}
\end{figure}

\section*{Thesis objectives} \label{c:objectives}
In this thesis, we aim to derive and implement a new model for simulating electron microscope projections. Our model utilizes an electron tracing simulation based on the particle-like behavior of the electrons. We focus on the simulation of electron-specimen interactions and a correct image formation, neglecting some of the processes occuring during image acquisition, such as radiation damage. The precise specification of the simulation setup is described in the first chapter. 
\par 
To our best knowledge, the particle-based approach to the simulation has not yet been implemented. The purpose of this thesis is two-fold. First, it provides an intuitive insight into the processes within the electron microscope from a different perspective than the traditional wave function approach. Second, our implementation can optionally replace the transmittance model popular with \gls{cryoem} reconstruction methods and provide a testing tool for generating artificial projections.

\section*{Thesis outline}
In the first chapter, we define our simplified simulation setup and characterize the electron interactions within the sample. We formulate the image formation problem as a \gls{rte}, for which we propose scattering models. We provide a formulation of the Monte Carlo estimators that solve the presented \gls{rte}.

The second chapter is dedicated to several problems that arise during the implementation of the proposed models. We describe the used solutions and their impact on the simulation.

The third chapter describes the implementation of the accompanying library. The description follows the same order as the code execution in the provided test program. We explain how we reused the same ray-tracing algorithm core for different model implementations.

In the fourth chapter, we provide examples of the rendered projections. We visually compare both our models with the multislice model. We also evaluate the quality of our simulated projections with the measured class averages and compare the results to the transmittance model.


\mbox{}
\vfill

\begin{flushright}
\rightskip=1.8cm\textit{``What you aim at determines what you see.''} \\
\vspace{.2em}
\rightskip=.8cm---Jordan B. Peterson
\end{flushright}
\vspace{1em}








