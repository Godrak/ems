% ****** Start of file apssamp.tex ******
%
%   This file is part of the APS files in the REVTeX 4.2 distribution.
%   Version 4.2a of REVTeX, December 2014
%
%   Copyright (c) 2014 The American Physical Society.
%
%   See the REVTeX 4 README file for restrictions and more information.
%
% TeX'ing this file requires that you have AMS-LaTeX 2.0 installed
% as well as the rest of the prerequisites for REVTeX 4.2
%
% See the REVTeX 4 README file
% It also requires running BibTeX. The commands are as follows:
%
%  1)  latex apssamp.tex
%  2)  bibtex apssamp
%  3)  latex apssamp.tex
%  4)  latex apssamp.tex
%
\documentclass[%
 reprint,
%superscriptaddress,
%groupedaddress,
%unsortedaddress,
%runinaddress,
%frontmatterverbose, 
%preprint,
%preprintnumbers,
%nofootinbib,
%nobibnotes,
%bibnotes,
 amsmath,amssymb,
 aps,
%pra,
%prb,
%rmp,
%prstab,
%prstper,
floatfix,
]{revtex4-2}

\usepackage{graphicx}% Include figure files
\usepackage{dcolumn}% Align table columns on decimal point
% \usepackage{bm}% bold math
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage{placeins}
\usepackage{hyperref}% add hypertext capabilities
\usepackage{braket}
\usepackage{bbm}
\usepackage{amsmath,amsfonts,amssymb}   %% AMS mathematics macros
%\usepackage[mathlines]{lineno}% Enable numbering of text and display math
%\linenumbers\relax % Commence numbering lines

%\usepackage[showframe,%Uncomment any one of the following lines to test 
%%scale=0.7, marginratio={1:1, 2:3}, ignoreall,% default settings
%%text={7in,10in},centering,
%%margin=1.5in,
%%total={6.5in,8.75in}, top=1.2in, left=0.9in, includefoot,
%%height=10in,a5paper,hmargin={3cm,0.8in},
%]{geometry}

\newcommand{\op}[1]{\mathcal{{#1}}}
\newcommand{\vv}[1]{\mathbf{{#1}}}


\begin{document}

% \preprint{APS/123-QED}

\title{Multiple potential scattering in TEM microscopy}% Force line breaks with \\

\author{Václav Alt}
 \email{alt.vaclav@gmail.com}%Lines break automatically or can be forced with \\
\affiliation{%
	Institute of Theoretical Physics, Faculty of Mathematics and Physics, Charles University, V~Holešovičkách 2, 180~00 Praha 8, Czech Republic}
\affiliation{%
 Eyen SE, Počernická 272/96, 108~00, Praha 10, Czech Republic
}%

%\date{\today}% It is always \today, today,
\date{2020}% It is always \today, today,
             %  but any date may be explicitly specified

\begin{abstract}
\end{abstract}

%\keywords{Suggested keywords}

\maketitle
\section{Introduction}

This is an accompanying document to the master thesis \emph{Simulating image formation in an electron microscope by electron tracing} \cite{mikus}. We construct a simple elastic scattering model to simulate the passage of an electron through a specimen in a transmission electron microscope by means of multiple scattering. The text is meant for non-physicists and therefore covers briefly basic scattering description needed to understand the model.

\section{Units}

For practical reason we use the \textbf{atomic units}, in which the electron mass $m$, the reduced Planck constant $\hbar$, the elemntary charge $e$ and the inverse Coulomb constant $4\pi\varepsilon_0$ are set to unity.

$$
	m = \hbar = e = 4\pi\varepsilon_0 = 1
$$


\section{Scattering problem}
This is a brief overview of basic quantum scattering description, which can be found for example in \cite{taylor2012scattering} or \cite{newton2002scattering}.

Our goal is to simulate the passage of an electron through the specimen in a transmission electron microscope. Since a detailed treatment of the interaction dynamics in an electron-molecule collision can be very complicated or even impossible for complicated molecules such as proteins, we restrict ourselves to simulation via multiple electron-atom scatterings. This approach requires several assumptions and approximation which are discussed later.

In the scattering process the projectile in the initial state $\ket{\phi_i}$ (the in-asymptote) approaches the target, interacts with it (evolves to state $\ket{\psi}$) gets scatter in the final state $\ket{\phi_f}$ (the out-asymptote).
\begin{equation}
	\ket{\phi_\mathrm{in}} \to \underset{\mathrm{interaction}}{\ket{\psi}} \to \ket{\phi_\mathrm{out}}
\end{equation}

The scattering is fully described by the Schrödinger equation

\begin{equation}
	\op{H} \ket{\psi} = E \ket{\psi}
\end{equation}
with the Hamiltonian
\begin{equation}
	\op{H} = \op{H}_0 + \op{V}
\end{equation}
The interaction is described by the operator $\op{V}$. The initial and final states are not affected by the interaction and are therefore solutions of the free Schrödinger equation
\begin{equation}
	\op{H}_0 \ket{\phi_{i,f}} = E \ket{\phi_{i,f}}
	\label{eq:sr_free}
\end{equation}
We assume that the scattering is elastic, i.e. the initial and the final states have the same energy $E$.

The initial states are related through the scattering operator $\op{S}$
\begin{equation}
	\ket{\phi_{f}} = \op{S} \ket{\phi_{i}}
\end{equation}
which is often decomposed in two parts a representing the unscattered and scattered part (the $\op{T}$ operator)
\begin{equation}
	\op{S} = \mathbbm{1} - 2\pi i\op{T}
\end{equation}

% The scattering process is fully described by the Lippmann-Schwinger equation
% \begin{equation}
% 	\ket{\psi} = \ket{\phi} + \op{G} \op{V} \ket{\psi},
% \end{equation}
% where $\op{G}$ is the Green function of the corresponding Schrödinger equation with Hamiltonian $\op{H}$
% \ldots
% 
% \begin{align}
% 	\op{G} &= \frac{1}{E-\op{H}}\\
% 	\op{H} &= -\frac{\nabla^2}{2} + \op{V}
% \end{align}



\subsection{Scattering cross section}
Scattering cross section can be understood as the proportion of the incoming particles that gets scattered. Let us have a beam of incoming particles of density $n\textrm{b}$, cross-sectional area $A_b$ and velocity $v_\textrm{b}$ hitting the target and let us denote $N\textrm{sc}$ the number of projectiles that get scattered per time unit. The scattering cross section $\sigma$ is defined as
\begin{equation}
	\sigma = \frac{N_\textrm{sc}}{n_\textrm{b}} = \frac{N_\textrm{sc} v_\textrm{b} A_\textrm{b}}{J_\textrm{b}}
\end{equation}

where $J_\textrm{b}$ is the particle flux in the incoming beam.

Similarly we can define the differential cross section as the ratio between the number of projectiles that have scattered into solid angle $\textrm{d}\Omega$ and the beam density $n_\textrm{b}$.

$$
\frac{\textrm{d}\sigma}{\textrm{d}\Omega}\textrm{d}\Omega = \frac{N_\textrm{sc}(\textrm{d}\Omega)}{n_\textrm{b}}
$$

The total scattering cross section $\sigma$ can be obtained by integrating over the full solid angle
\begin{equation}
	\sigma = \int_\Omega \frac{\mathrm{d}\sigma}{\mathrm{d}\Omega} \mathrm{d}\Omega 
	\label{eq:s_decomp}
\end{equation}

\subsection{Plane wave}
In the microscope the initial state is a plane wave and we assume that the interaction is weak enough not to distort the plane wave. For that reason restrict ourselves to plane waves only. Plane waves $\ket{\vv{p}}$ corresponding to momentum $\vv{p}$ are the simplest solutions of the free Schrödinger equation (\ref{eq:sr_free}) with energy $E_\vv{p} = \frac{p^2}{2m}$
\begin{align}
	\phi_\vv{p}(\vv{x}) = \braket{\vv{x} | \vv{p}} &= \frac{1}{(2\pi \hbar)^{3/2}} e^{-i \vv{p}\cdot\vv{x}}\\
	\braket{\vv{p}' | \vv{p}} &= \delta_3(\vv{p}' - \vv{p})
\end{align}

The matrix element of the $\op{S}$ operator between plane waves

\begin{equation}
	\braket{\vv{p}^\prime | \op{S}| \vv{p}} = \delta_3(\vv{p}^\prime - \vv{p}) - 2\pi i \braket{\vv{p}^\prime | \op{T}| \vv{p}}
\end{equation}

\begin{align}
	\braket{\vv{p}^\prime | \op{T}| \vv{p}} & = 
	\delta(E_{\vv{p}^\prime} - E_{\vv{p}})
	t(\vv{p}^\prime \leftarrow \vv{p}) \\
	&= -\delta(E_{\vv{p}^\prime} - E_{\vv{p}}) \frac{f(\vv{p}^\prime \leftarrow \vv{p})}{(2\pi)^2m},
\end{align}
where $f(\vv{p}^\prime \leftarrow \vv{p})$ is the scattering amplitude, which is closely related to the $\op{T}$-matrix element $t(\vv{p}^\prime \leftarrow \vv{p})$ and which can be used to calculate the differential cross section
\begin{equation}
	\frac{\mathrm{d}\sigma}{\mathrm{d}\Omega} (\vv{p}^\prime \leftarrow \vv{p})) = |f(\vv{p}^\prime \leftarrow \vv{p})|^2
	\label{eq:dcs_scat_amp}
\end{equation}


% \begin{equation}
% 	s(\vv{p}^\prime \leftarrow \vv{p}) =
% 	\delta_3(\vv{p}^\prime - \vv{p})
% 	- 2\pi i \delta(E_{\vv{p}^\prime} - E_{\vv{p}})
% 	t(\vv{p}^\prime \leftarrow \vv{p})
% \end{equation}
% 
% \begin{equation}
% 	s(\vv{p}^\prime \leftarrow \vv{p}) =
% 	\delta_3(\vv{p}^\prime - \vv{p})
% 	+ \frac{i}{2\pi m} \delta(E_{\vv{p}^\prime} - E_{\vv{p}})
% 	f(\vv{p}^\prime \leftarrow \vv{p})
% \end{equation}
% 
% \begin{equation}
% 	\frac{\mathrm{d}\sigma}{\mathrm{d}\Omega} = |f(\vv{p}^\prime \leftarrow \vv{p})|^2
% \end{equation}
% 
% \begin{equation}
% 	f(\vv{p}^\prime \leftarrow \vv{p}) = -(2\pi)^2 m \braket{\vv{p}^\prime | \op{T}| \vv{p}}
% \end{equation}

\subsection{Born series and Born approximation}
The Lippmann-Schwinger equation for the $\op{T}$ operator
\begin{equation}
	\op{T} = \op{V} + \op{V} \op{G}_0 \op{T},
\end{equation}
where $\op{G}_0$ is the Green function corresponding to the free Schrödinger equation:
\begin{equation}
	\op{H}_0 \ket{\phi} = E \ket{\phi}
\end{equation}
can be used to express the $\op{T}$ operator as the so-called Born series

\begin{equation}
	\label{eq:born_series}
	\op{T} = \sum_{i=0}^\infty \left( \op{V} \op{G}_0 \right )^i \op{T}
\end{equation}

In the Born approximation only the first term in (\ref{eq:born_series}) is kept, that is:

\begin{equation}
	\op{T} \approx \op{V}
\end{equation}

Let us see what happens with the scattering amplitude in the Born approximation by inserting $\mathbbm{1} = \int \ket{\vv{x}}\bra{\vv{x}} \mathrm{d}\vv{x}$

\begin{align}
	f_1(\vv{p}^\prime \leftarrow \vv{p}) &= -(2\pi)^2 m \braket{\vv{p}^\prime | \op{V}| \vv{p}} \\
	 &= -(2\pi)^2 m \int \mathrm{d}\vv{x}\bra{\vv{p}^\prime} V(\vv{x}) \ket{\vv{x}}\braket{\vv{x} | \vv{p}} \\
	 &= -(2\pi)^2 m\frac{1}{(2\pi \hbar)^3} \int \mathrm{d}\vv{x}\ V(\vv{x}) e^{-\frac{i}{\hbar}(\vv{p} - \vv{p}^\prime)\cdot\vv{x}} \\
	 &= - \frac{m}{2\pi \hbar^3} \hat V(\vv{q}) \label{eq:pwba_scat_amp}
\end{align}
where $\hat{V}$ is the Fourier transform of the potential $V(x)$ and $\vv q = \vv p' - \vv p$. Since we consider elastic scattering only, the total momentum remains unchanged $\lvert \vv p' \rvert = \lvert \vv p \rvert $ and

\begin{equation}
	\lvert \vv q \rvert = 2 \lvert \vv p \rvert \sin\frac\theta 2,
\end{equation}
where $\theta$ is the angle between the initial and final momentum.


Inserting (\ref{eq:pwba_scat_amp}) into (\ref{eq:dcs_scat_amp}) yields
\begin{equation}
	\frac{\mathrm{d} \sigma}{\mathrm{d} \Omega} (\vv{p}' \leftarrow \vv{p}) = \left ( \frac{m}{2 \pi \hslash} \right )^2 \lvert \hat{V}(\vv{q}) \rvert^2
	\label{eq:pwba_dcs}
\end{equation}
That is, within the first Born approximation the differential cross section for plane wave scattering (plane wave Born approximation - PWBA) is given solely by the Fourier transform of the interaction potential evaluated at momentum transfer $\vv q$.

\subsection{Wentzel potential and Rutherford formula}

To avoid treating the full potential of general atom and complications connected with purely Coulombic interaction, on can described the atom with an effective, screened, Yukawa-type potential \cite{nikjoo2008, fernandez1993, wentzel1926}
\begin{equation}
	V(r) = -\frac{Z}{r}e^{-\frac{r}{R}}
	\label{eq:wentzel_pot}
\end{equation}
where $r = \sqrt{x^2 + y^2 + z^2}$ is the radial distance from the atom, $Z$ is the atomic number and $R$ is the screening radius, which may be estimated from Thomas-Fermi model of the atom
\begin{equation}
	R \simeq 0.885 Z^{-1/3} a_0
\end{equation}

The differential cross section calculated from (\ref{eq:pwba_dcs}) and (\ref{eq:wentzel_pot}) is
\begin{equation}
	\frac{\mathrm{d}\sigma}{\mathrm{d}\Omega} = Z^2 r_e^2 \left( \frac{1 - \beta^2}{\beta^4} \right) \frac{1}{(1- \cos\theta + 2\eta_W)^2}
\end{equation}
with $r_e$ being the classical electron radius, $\beta$ is the incident velocity (in units of the speed of light) and $\eta_W$ is the Wentzel screening parameter \cite{wentzel1926} given by
\begin{equation}
	\eta_W = \frac{1.7\times 10^{-5} Z^{2/3}}{\tau(\tau+2)}
\end{equation}
where $\tau = E_k / E_0$ is the ration between the electron's kinetic energy $E_k$ and rest energy $E_0$.

\subsection{Scattering factor fits}
Another way to estimate the scattering amplitudes or the atomic potentials is to use precalculated or measured data. Since the scattering amplitude and the potentials are related within PWBA through the Fourier transform, one can obtain the atomic potential by calculating the inverse Fourier transform of the scattering amplitude. Doyle et al. \cite{doyle1968} calculated X-ray and electron scattering amplitudes for 76 atoms using the relativistic Hartree-Fock method and Kirkland \cite{kirkland2010} provided a such parametrization of these scattering amplitudes that is easily transform by the inverse Fourier transform:

\begin{align}
	% V(\vv x) = 2\pi^2 a_0 \sum_{i=1}^3 \frac{a_i}{r}e^{-2\pi r \sqrt{b_i}} + 2\pi^{5/2} a_0 \sum_{i=1}^3 c_i d_i^{-3/2}e^{-\pi^2 r^2 / d_i}
	V(r) &= 2\pi^2 a_0 \sum_{i=1}^3 \left(\frac{a_i}{r}e^{-2\pi r \sqrt{b_i}} + \sqrt{\pi} \frac{c_i}{d_i^{1.5}} e^{-\pi^2 \frac{r^2}{d_i}}\right) \\
	f(q) &= \sum_{i=1}^3 \left( \frac{a_i}{q^2 + b_i} + c_i e^{-d_i q^2} \right )
\end{align}

\section{Image formation}
The images within this model is constructed by sending individual electrons through the specimen. Each electron gets scattered a few times and lands somewhere in the image plane. The final electron distribution then constitutes the resulting image.


\bibliography{bibliography}% Produces the bibliography via BibTeX.

\end{document}
%
% ****** End of file apssamp.tex ******
