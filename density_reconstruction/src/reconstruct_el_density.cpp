#include <algorithm>

#include "reconstruct_el_density.h"

#include "gemmi/mmread.hpp"
#include "gemmi/ccp4.hpp"

#include "periodic_table.h"

static const double MAX_RADIUS = 3; //angstroms

#include "scattering-factors/sf_params.h"
#include "scattering-factors/sfp_loader.hpp"

ScatFacParamsList PARAMS_LIST = read_sfp_list_from_const_char(PARAMS);

density::ElectronDensityField density::reconstructElectronDensityFast(
	const gemmi::Structure& structure,
	double voxel_size_ang,
	bool scattering_f)
{
	auto init_atom = structure.models[0].chains[0].residues[0].atoms[0];
	double min_x = init_atom.pos.x;
	double max_x = init_atom.pos.x;
	double min_y = init_atom.pos.y;
	double max_y = init_atom.pos.y;
	double min_z = init_atom.pos.z;
	double max_z = init_atom.pos.z;

	for (const auto& model : structure.models) {
		for (const auto& chain : model.chains) {
			for (const auto& residue : chain.residues) {
				for (const auto& atom : residue.atoms) {
					min_x = std::min(min_x, atom.pos.x);
					max_x = std::max(max_x, atom.pos.x);
					min_y = std::min(min_y, atom.pos.y);
					max_y = std::max(max_y, atom.pos.y);
					min_z = std::min(min_z, atom.pos.z);
					max_z = std::max(max_z, atom.pos.z);
				}
			}
		}
	}

	auto max_c = std::max(std::max(max_x, max_y), max_z);
	auto min_c = std::min(std::min(min_x, min_y), min_z);

	auto min_x_margins = min_c - MAX_RADIUS;
	auto min_y_margins = min_c - MAX_RADIUS;
	auto min_z_margins = min_c - MAX_RADIUS;
	auto max_x_margins = max_c + MAX_RADIUS;
	auto max_y_margins = max_c + MAX_RADIUS;
	auto max_z_margins = max_c + MAX_RADIUS;

	double diff_x = max_x_margins - min_x_margins;
	double diff_y = max_y_margins - min_y_margins;
	double diff_z = max_z_margins - min_z_margins;

	size_t volume_size_x = std::ceil(diff_x / voxel_size_ang);
	size_t volume_size_y = std::ceil(diff_y / voxel_size_ang);
	size_t volume_size_z = std::ceil(diff_z / voxel_size_ang);

	std::vector<float> density_map(volume_size_x * volume_size_y * volume_size_z, 0);
//    for (auto& val : density_map) {
//        val = (float(random()) / RAND_MAX) * 0.001;
//    }
	int voxel_radius = std::ceil(MAX_RADIUS / voxel_size_ang);

	for (const auto& model : structure.models) {
		for (const auto& chain : model.chains) {
			for (const auto& residue : chain.residues) {
				for (const auto& atom : residue.atoms) {
					int closest_coord_x = std::round((atom.pos.x - min_x_margins) / voxel_size_ang);
					int closest_coord_y = std::round((atom.pos.y - min_y_margins) / voxel_size_ang);
					int closest_coord_z = std::round((atom.pos.z - min_z_margins) / voxel_size_ang);

					for (int x_off = -voxel_radius; x_off <= voxel_radius; ++x_off) {
						for (int y_off = -voxel_radius; y_off <= voxel_radius; ++y_off) {
							for (int z_off = -voxel_radius; z_off <= voxel_radius; ++z_off) {

								int voxel_coords_x = (closest_coord_x + x_off);
								int voxel_coords_y = (closest_coord_y + y_off);
								int voxel_coords_z = (closest_coord_z + z_off);

								double coords_x = voxel_coords_x * voxel_size_ang + min_x_margins;
								double coords_y = voxel_coords_y * voxel_size_ang + min_y_margins;
								double coords_z = voxel_coords_z * voxel_size_ang + min_z_margins;

								double dis_ang = std::sqrt(
									(coords_x - atom.pos.x) * (coords_x - atom.pos.x) + (coords_y - atom.pos.y) * (coords_y - atom.pos.y)
										+ (coords_z - atom.pos.z) * (coords_z - atom.pos.z));

								if (voxel_coords_x >= volume_size_x || voxel_coords_x < 0 || voxel_coords_y >= volume_size_y
									|| voxel_coords_y < 0 || voxel_coords_z >= volume_size_z || voxel_coords_z < 0) {
									//outside of the volume, continue
									continue;
								}

								int density_map_index = voxel_coords_z * volume_size_x * volume_size_y + voxel_coords_y * volume_size_x
									+ voxel_coords_x;
								auto z = (size_t) atom.element.atomic_number();
								double value = 0;
								if (z > PARAMS_LIST.size()) {
									std::cout << "potential generation error, no parameters for atomic number  " << z << std::endl;
								} else {
									auto dis = std::max(ANG_TO_BOHR * dis_ang, 0.1);
									if (scattering_f) {
										value = electron_scattering_factor(dis, PARAMS_LIST[z]);
									} else {
										value = potential(dis, PARAMS_LIST[z]);
									}
									density_map[density_map_index] += value;
								}
							}
						}
					}
				}
			}
		}
	}

	if (scattering_f) {
		for (auto& val : density_map) {
			val = val * val;
		}
	}

	density::ElectronDensityField result { };
	result.electron_density = density_map;
	result.min_corner[0] = min_x_margins;
	result.min_corner[1] = min_y_margins;
	result.min_corner[2] = min_z_margins;

	result.size_voxels[0] = volume_size_x;
	result.size_voxels[1] = volume_size_y;
	result.size_voxels[2] = volume_size_z;

	result.voxel_size = voxel_size_ang;

	return result;
}

density::ElectronDensityField density::reconstructElectronDensityFast(const std::string& filepath, double voxel_size_ang, bool scattering_f)
{
	auto structure = gemmi::read_structure_file(filepath);
	return reconstructElectronDensityFast(structure, voxel_size_ang, scattering_f);
}
