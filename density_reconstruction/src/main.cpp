#include <iostream>
#include <string>

#include "reconstruct_el_density.h"

//int main(int argc, char** argv) {
//    if (argc < 3) {
//        std::cout << "please provide pdb file and desired volume size" << std::endl;
//        exit(1);
//    }
//
//    size_t size = std::stoi(argv[2]);
//    auto field = density::reconstructElectronDensity(std::string(argv[1]), size, 3);
//
//    std::cout << "ang per voxel: " << field.voxel_size << std::endl;
//    for (auto v : field.electron_density) {
//        std::cout << v << std::endl;
//    }
//}

int main(int argc, char** argv) {
    if (argc < 3) {
        std::cout << "please provide pdb file and desired voxel_size" << std::endl;
        exit(1);
    }

    double voxel_size = std::stod(argv[2]);
    auto field = density::reconstructElectronDensityFast(std::string(argv[1]), voxel_size);

    std::cout << "volume_size: " << field.size_voxels[0] << "  " << field.size_voxels[1] << "  " << field.size_voxels[2] << std::endl;
    std::cout << "min corner: " << field.min_corner[0] << "  " << field.min_corner[1] << "  " <<  field.min_corner[2] << std::endl;



}
