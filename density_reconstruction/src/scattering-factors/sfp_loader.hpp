#pragma once

#include <stdio.h>
#include <sstream>
#include <iterator>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <vector>
#include <string>

#include "scattering_factors.hpp"

ScatFacParamsList read_sfp_list_from_const_char(const char* data);

ScatFacParamsList read_sfp_list_from_file(std::string filename);
void save_sfp_list_to_file(std::string filename, const ScatFacParamsList& sfp_list);

ScatFacParamsList read_sfp_list_from_stupid_file(std::string filename);
void print_sfp_list_stupid(const ScatFacParamsList& sfp_list);

