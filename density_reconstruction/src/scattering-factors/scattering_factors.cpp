#include "scattering_factors.hpp"


double electron_scattering_factor(double q, const ScatFacParams &p) {
	double ff = 0.0;
	// Lorenzians
	for (int i = 0; i < N_L; ++i)
		ff += p.a[i] / (q * q + p.b[i]);
	// Gaussians
	for (int i = 0; i < N_G; ++i)
		ff += p.c[i] * exp(-p.d[i] * q * q);
	return ff / BOHR_TO_ANG;
}


double xray_scattering_factor(double q, int Z, const ScatFacParams &p) {
	return Z - 2 * M_PI * M_PI * BOHR_A0 * q * q * electron_scattering_factor(q, p);
}


double potential(double r, const ScatFacParams &p) {
	double pot = 0.0;
	// Lorenzians
	for (int i = 0; i < N_L; ++i)
		pot += p.a[i] * exp(-2.0 * M_PI * r * sqrt(p.b[i])) / r;
	// Gaussians
	for (int i = 0; i < N_G; ++i)
		pot += sqrt(M_PI) * p.c[i] * exp(-M_PI * M_PI * r * r / p.d[i]) / pow(p.d[i], 1.5);
	
	return 2 * M_PI * M_PI * BOHR_A0 * EL_CHARGE * pot * BOHR_TO_ANG * BOHR_TO_ANG;
}
