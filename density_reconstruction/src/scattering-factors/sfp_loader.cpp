#include "sfp_loader.hpp"
#include <streambuf>
#include <cstring>

void split(const std::string& str, std::vector<std::string>& tokens, char delim = ' ');

ScatFacParamsList read_sfp_list_from_const_char(const char* data) {
    std::istringstream file(std::string(data, std::strlen(data)));

    std::string line { };
    ScatFacParamsList sfp_list { };
    while (std::getline(file, line)) {
        if (line[0] == '#')
            continue;

        std::vector<std::string> tokens { };
        split(line, tokens);

        ScatFacParams p;
        for (int i = 0; i < N_L; ++i) {
            p.a[i] = stod(tokens[1 + i]);
            p.b[i] = stod(tokens[1 + N_L + i]);
        }

        for (int i = 0; i < N_G; ++i) {
            p.c[i] = stod(tokens[1 + 2 * N_L + i]);
            p.d[i] = stod(tokens[1 + 2 * N_L + N_G + i]);
        }
        sfp_list.push_back(p);
    }
    return sfp_list;
}

ScatFacParamsList read_sfp_list_from_file(std::string filename) {
    std::ifstream file;
    file.open(filename);

    std::string line;
    ScatFacParamsList sfp_list;
    while (std::getline(file, line)) {
        if (line[0] == '#')
            continue;

        std::vector<std::string> tokens;
        split(line, tokens);

        ScatFacParams p;
        for (int i = 0; i < N_L; ++i) {
            p.a[i] = stod(tokens[1 + i]);
            p.b[i] = stod(tokens[1 + N_L + i]);
        }

        for (int i = 0; i < N_G; ++i) {
            p.c[i] = stod(tokens[1 + 2 * N_L + i]);
            p.d[i] = stod(tokens[1 + 2 * N_L + N_G + i]);
        }
        sfp_list.push_back(p);
    }
    file.close();
    return sfp_list;
}

void save_sfp_list_to_file(std::string filename, const ScatFacParamsList& sfp_list) {
    std::ofstream of;
    of.open(filename);
    int i = 1;
    of << "# Z a1 a2 a3 b1 b2 b3 c1 c2 c3 d1 d2 d3" << std::endl;
    for (const auto& p : sfp_list) {
        of << i << " " << std::scientific << std::setprecision(8) << p.a[0] << " " << std::scientific << std::setprecision(8) << p.a[1] << " " << std::scientific
            << std::setprecision(8) << p.a[2] << " " << std::scientific << std::setprecision(8) << p.b[0] << " " << std::scientific << std::setprecision(8) << p.b[1] << " "
            << std::scientific << std::setprecision(8) << p.b[2] << " " << std::scientific << std::setprecision(8) << p.c[0] << " " << std::scientific << std::setprecision(8)
            << p.c[1] << " " << std::scientific << std::setprecision(8) << p.c[2] << " " << std::scientific << std::setprecision(8) << p.d[0] << " " << std::scientific
            << std::setprecision(8) << p.d[1] << " " << std::scientific << std::setprecision(8) << p.d[2] << std::endl;
        ++i;
    }
    of.close();
}

ScatFacParamsList read_sfp_list_from_stupid_file(std::string filename) {
    std::string line;
    std::ifstream file;
    file.open(filename);

    ScatFacParamsList sfp_list;

    while (file.peek() != EOF) {
        std::getline(file, line);
        auto Z = line.substr(line.find("=") + 1, line.find(",") - 2);

        ScatFacParams p;

        std::getline(file, line); // skip chisq
        std::getline(file, line);
        auto s = line.find(" ");
        p.a[0] = stod(line.substr(0, s));
        p.b[0] = stod(line.substr(s + 1, line.size()));

        std::getline(file, line);
        s = line.find(" ");
        p.a[2] = stod(line.substr(0, s));
        p.b[2] = stod(line.substr(s + 1, line.size()));

        std::getline(file, line);
        s = line.find(" ");
        p.c[1] = stod(line.substr(0, s));
        p.d[1] = stod(line.substr(s + 1, line.size()));
        std::getline(file, line);

        std::getline(file, line);
        p.a[1] = stod(line);

        std::getline(file, line);
        p.c[0] = stod(line);

        std::getline(file, line);
        p.c[2] = stod(line);

        std::getline(file, line);

        std::getline(file, line);
        p.b[1] = stod(line);

        std::getline(file, line);
        p.d[0] = stod(line);

        std::getline(file, line);
        p.d[2] = stod(line);

        std::getline(file, line);

        sfp_list.push_back(p);
    }

    file.close();
    return sfp_list;
}

void print_sfp_list_stupid(const ScatFacParamsList& sfp_list) {
    // format presented in The Book
    int i = 1;
    for (const auto& p : sfp_list) {
        printf("Z=%3d, chisq= N/A\n", i);
        printf("%16.8e %15.8e %15.8e %15.8e\n", p.a[0], p.b[0], p.a[1], p.b[1]);
        printf("%16.8e %15.8e %15.8e %15.8e\n", p.a[2], p.b[2], p.c[0], p.d[0]);
        printf("%16.8e %15.8e %15.8e %15.8e\n", p.c[1], p.d[1], p.c[2], p.d[2]);
        printf("\n");
        ++i;
    }
}

void split(const std::string& str, std::vector<std::string>& tokens, char delim) {
    std::stringstream ss(str);
    std::string token;
    while (std::getline(ss, token, delim)) {
        tokens.push_back(token);
    }
}

