#pragma once

#include <cmath>
#include <vector>


// number of Lorenzian terms
#define N_L 3
// number of Gaussian terms
#define N_G 3

// Choose this if you want everything in atomic units. You want to choose this.
#define BOHR_A0 1.0
#define BOHR_TO_ANG 0.529177210903
#define ANG_TO_BOHR 1.88972612463
#define EL_CHARGE 1.0

// Choose this if you want the scattering factors in Anstroms and the potential
// in god-knows-what units
// #define BOHR_A0 0.529177210903
// #define BOHR_TO_ANG 1.0
// #define EL_CHARGE 1.0

struct ScatFacParams {
	double a[N_L];
	double b[N_L];
	double c[N_G];
	double d[N_G];
};

using ScatFacParamsList = std::vector<ScatFacParams>;

// scattering factor f_e(q) used to calculate the differential cross section
// d\sigma / d\Omega = | f_e(q) |^2
double electron_scattering_factor(double q, const ScatFacParams &p);

// for completeness, we won't probably need this
double xray_scattering_factor(double q, int Z, const ScatFacParams &p);

// potential corresponding to the scattering factor: related through the Fourier transform
double potential(double r, const ScatFacParams &p);
