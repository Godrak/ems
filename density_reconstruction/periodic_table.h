/*
 * periodic_table.h
 *
 *  Created on: Mar 9, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef DENSITY_RECONSTRUCTION_PERIODIC_TABLE_H_
#define DENSITY_RECONSTRUCTION_PERIODIC_TABLE_H_

#include <vector>
#include <cmath>

namespace table {

//Initializing structures.
struct Element {
    short atomicno;		//Atomic Number
    char name[50];		//Name
    char ec[50];		//Electronic Configuration
    short group;		//Group
    short period;		//Period
    char block;			//Block
    float atomicw;		//Atomic Weight, also Mass number, also A
    float en;			//Electronegativity
    short atomicr;		//Atomic Radius  [pm]
    char origin[100];	//Origin Of Name
    float meltp;		//Melting Point
    float boilp;		//Boiling Point
    float density;		//Density

    //added custom values
    float vdw = 0; 		//Van der Waals radius, [pm]  WARNING - for most elements not known
};

std::vector<Element> initElements();

const std::vector<Element> elements = initElements();

Element getByAtomicNumber(short atomic_number);

}

#endif /* DENSITY_RECONSTRUCTION_PERIODIC_TABLE_H_ */
