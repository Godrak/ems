#ifndef SRC_RECONSTRUCT_EL_DENSITY_H_
#define SRC_RECONSTRUCT_EL_DENSITY_H_

#include <array>
#include <istream>
#include <ostream>
#include <iostream>
#include <string>
#include <numeric>
#include "gemmi/model.hpp"
#include "gemmi/ccp4.hpp"

namespace density {

struct ElectronDensityField {
	std::vector<float> electron_density;
	std::array<size_t, 3> size_voxels;
	std::array<float, 3> min_corner;
	double voxel_size; //in angstroms
};

struct Stats {
	std::array<float, 2> minmax;
};

/**
 volume_size: size of one side of the constructed volume cube
 padding: number of Angstroms between the atom and the side of the volume
 **/
ElectronDensityField reconstructElectronDensityFast(const gemmi::Structure& structure, double voxel_size_ang, bool scatterinig_f = false);
ElectronDensityField reconstructElectronDensityFast(const std::string& filepath, double voxel_size_ang, bool scatterinig_f = false);

template<typename T>
density::ElectronDensityField Ccp4ToEelectronDensityField(const gemmi::Ccp4<T>& data) {
	density::ElectronDensityField result { };
	const_cast<gemmi::Ccp4<T>&>(data).grid.calculate_spacing();

	auto axis_order = data.axis_positions();

	for (int x = 0; x < data.grid.nu; ++x) {
		for (int y = 0; y < data.grid.nv; ++y) {
			for (int z = 0; z < data.grid.nw; ++z) {
				int coords[3] = { x, y, z };
				result.electron_density.push_back(data.grid.get_value(coords[axis_order[0]], coords[axis_order[1]], coords[axis_order[2]]));
			}
		}
	}

	result.voxel_size = data.grid.spacing[0];
	if (result.voxel_size <= 0) {
		result.voxel_size = 1;
	}

	result.size_voxels[0] = data.grid.nu;
	result.size_voxels[1] = data.grid.nv;
	result.size_voxels[2] = data.grid.nw;

	result.min_corner[0] = -data.grid.nu * result.voxel_size / 2;
	result.min_corner[1] = -data.grid.nv * result.voxel_size / 2;
	result.min_corner[2] = -data.grid.nw * result.voxel_size / 2;

	return result;
}

}

#endif /* SRC_RECONSTRUCT_EL_DENSITY_H_ */
