/*
 * helper_func.h
 *
 *  Created on: Sep 30, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_HELPER_FUNC_H_
#define SRC_HELPER_FUNC_H_

#include "boltview/host_image.h"
#include "boltview/texture_image.h"
#include "boltview/procedural_views.h"
#include "boltview/reduce.h"
#include "boltview/copy.h"
#include "boltview/image_io.h"

#include <iostream>
#include <vector>
#include <sstream>
#include "ems/params.h"

PARAM(projectionsFile, std::string, "path to mrc file with projections");

PARAM(projectionsStarFile, std::string, "path to star file describing the projections");

PARAMWD(samples, size_t, 1, "number of rendered samples per direction");

PARAMWD(particlesRange, bolt::Int2, bolt::Int2(0, -1), "number of used projection directions");

PARAM(detectorPixelSize, ems::IPixel, "size of the detector - resolution of final image");

json_params::JsonParamsHandler loadParams(int argc, char** argv) {
	if (argc < 2) {
		std::cout << "error: expected json parameter file path as argument" << std::endl;
		exit(-1);
	}

	json_params::JsonParamsHandler result { argv[1] };

	std::stringstream params_overrides { };

	params_overrides << "{";

	for (size_t i = 3; i < argc; i += 2) {

		auto key = std::string { argv[i - 1] };
		if (key[0] == '-') {
			key.erase(key.begin());
		}

		auto value = argv[i];

		params_overrides << "\"" << key << "\":" << value << ",";
	}
	//Remove the last comma, and finalize the json with '}'
	params_overrides.seekp(-1, params_overrides.cur);
	params_overrides << "}";
	auto overrides = json::JSON::Load(params_overrides.str());

	result.overrideWith(overrides);

	return result;
}

template<typename TView>
float normalizeView(TView view, bolt::Float2 minmax = bolt::Float2 { 0, 1 }, bool log = false) {
	auto min_max_mean = bolt::minMaxMean(view);
	auto min = min_max_mean.template get<0>();
	auto max = min_max_mean.template get<1>();
	auto mean = min_max_mean.template get<2>();

	if (log) {
		LOG("min: " << min << "  max: " << max << " mean: " << mean);
	}
	bolt::copy(bolt::addValue(minmax[0], bolt::multiplyByFactor((minmax[1] - minmax[0]) / (max - min), bolt::addValue(-min, view))), view);
	return max;
}

template<typename TType>
void printView(bolt::HostImageView<TType, 2> view) {
	for (int x = 0; x < view.size()[0]; ++x) {
		for (int y = 0; y < view.size()[1]; ++y) {
			std::cout << view[bolt::Int2 { x, y }] << " ";
		}
		std::cout << std::endl;
	}
}

template<typename TView>
float normalizedCrossCorrelation(TView first, TView second) {
	auto first_mean = bolt::minMaxMean(first).template get<2>();
	auto second_mean = bolt::minMaxMean(second).template get<2>();

	auto f = bolt::addValue(-first_mean, first);
	auto s = bolt::addValue(-second_mean, second);

	auto numerator = bolt::sum(bolt::multiply(f, s), double(0));
	auto denominator_f = bolt::sum(bolt::square(f), double(0));
	auto denominator_s = bolt::sum(bolt::square(s), double(0));

	auto x = denominator_f * denominator_s;
	if (x == 0) {
		return 0;
	}
	return numerator / std::sqrt(x);

}

template<typename TView>
float normalizedDiffSminF(TView first, TView second) {
	auto first_mean = bolt::minMaxMean(first).template get<2>();
	auto second_mean = bolt::minMaxMean(second).template get<2>();

	auto f = bolt::addValue(-first_mean, first);
	auto s = bolt::addValue(-second_mean, second);

	auto numerator = bolt::sum(bolt::multiply(f, s), double(0));
	auto denominator_f = bolt::sum(bolt::square(f), double(0));
	auto denominator_s = bolt::sum(bolt::square(s), double(0));

	f = bolt::multiplyByFactor(1.0 / denominator_f, f);
	s = bolt::multiplyByFactor(1.0 / denominator_s, s);

	bolt::copy(s - f, first);

	auto x = denominator_f * denominator_s;
	if (x == 0) {
		return 0;
	}
	return numerator / std::sqrt(x);
}

#endif /* SRC_HELPER_FUNC_H_ */
