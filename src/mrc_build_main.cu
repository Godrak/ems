/*
 * mrc_build_main.cu
 *
 *  Created on: Oct 22, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#include "helper_func.h"
#include "reconstruct_el_density.h"
#include <vector>
#include <algorithm>

#include "external/mrc_io/mrc_io.h"

#include "gemmi/mmread.hpp"
#include "gemmi/model.hpp"
#include "gemmi/ccp4.hpp"
#include "periodic_table.h"

void analyzePDB(std::string pdbpath) {
	size_t distribution[256] = { 0 };

	size_t count = 0;
	auto structure = gemmi::read_structure_file(pdbpath);
	for (const auto& model : structure.models) {
		for (const auto& chain : model.chains) {
			for (const auto& residue : chain.residues) {
				for (const auto& atom : residue.atoms) {
					auto z = atom.element.atomic_number();
					distribution[z]++;
					count++;
				}
			}
		}
	}

	std::vector<std::pair<size_t, size_t>> counts { };
	for (size_t x = 0; x < 256; ++x) {
		if (distribution[x] > 0) {
			counts.emplace_back(distribution[x], x);
		}
	}

	std::sort(
		counts.begin(),
		counts.end(),
		[](const std::pair<size_t, size_t>& a, const std::pair<size_t, size_t>& b) {return a.first > b.first;});

	std::cout << "Molecule summary: " << std::endl;
	for (auto pair : counts) {
		auto z = pair.second;
		auto element = table::getByAtomicNumber(z);

		std::cout << pair.first << "  %" << (pair.first * 100.0f / float(count)) << "  " << element.name << "  z: " << z << std::endl;
	}

}

int main(int argc, char** argv) {
	if (argc < 3) {
		if (argc == 2) {
			std::cout << argv[1] << std::endl;
			if (argv[1][0] == 't' && argv[1][1] == 'e' && argv[1][2] == 's' && argv[1][3] == 't') {
				bolt::HostImage<float, 3> data { 50, 50, 50 };
				auto view = data.view();
				mrc::Write(std::string("test0.mrc"), view, 0.6);
				view[bolt::Int3(23, 25, 25)] = 1;
				mrc::Write(std::string("test1.mrc"), view, 0.6);
				view[bolt::Int3(27, 25, 25)] = 1;
				mrc::Write(std::string("test2.mrc"), view, 0.6);
				exit(0);
			}
			analyzePDB(std::string(argv[1]));
		}

		std::cout << "please provide pdb file and desired voxel_size" << std::endl;
		exit(1);
	}

	double voxel_size = std::stod(argv[2]);
	auto field = density::reconstructElectronDensityFast(std::string(argv[1]), voxel_size, argc > 3);

	std::cout << "volume_size: " << field.size_voxels[0] << "  " << field.size_voxels[1] << "  " << field.size_voxels[2] << std::endl;
	std::cout << "min corner: " << field.min_corner[0] << "  " << field.min_corner[1] << "  " << field.min_corner[2] << std::endl;

	bolt::HostImage<float, 3> data { field.size_voxels[0], field.size_voxels[1], field.size_voxels[2] };
	auto view = data.view();

	memcpy(view.pointer(), field.electron_density.data(), field.electron_density.size() * sizeof(float));

	mrc::Write(std::string(argv[1]) + ".mrc", view, field.voxel_size);
}

