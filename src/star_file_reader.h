/*
 * star_file_reader.h
 *
 *  Created on: May 10, 2021
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_STAR_FILE_READER_H_
#define SRC_STAR_FILE_READER_H_

#include "external/projection.h"
#include "../density_reconstruction/gemmi/include/gemmi/cif.hpp"
#include "../density_reconstruction/gemmi/include/gemmi/numb.hpp"
#include <iostream>
#include "external/ctfParameters.h"
#include "ems/ems_setup.h"
#include "ems/math_utils.h"

namespace ctftools {

#define STR( x ) RemoveScope(#x)

inline std::string RemoveScope(std::string s) {
	auto rpos = s.rfind(":");
	if (rpos == std::string::npos) {
		return s;
	} else {
		return s.substr(rpos + 1);
	}
}

std::vector<Projection> readStarFile(std::string starfile, bolt::Int2 image_size, const ems::ElectronMicroscopeSetup& setup) {

	using namespace gemmi::cif;
	Document doc = read_file(starfile);

	enum Optics {
		SphericalAberration, AmplitudeContrast
	};

	auto spherical_aberration = 0.0;
	auto amplitude_contrast = 0.0;
	auto optics_block = doc.find_block("optics");
	for (auto& b : doc.blocks) {
		std::cout << "block: " << b.name << std::endl;
	}
	if (optics_block != nullptr) {
		auto op_table = optics_block->find("_rln", { STR(Optics::SphericalAberration), STR(Optics::AmplitudeContrast) });
		auto row = op_table.at(0);
		std::cout << "Optics block found in input star file " << std::endl;
		amplitude_contrast = as_number(row.at(Optics::AmplitudeContrast));
		spherical_aberration = as_number(row.at(Optics::SphericalAberration));
	}

	auto data_block = doc.find_block("Particles");
	if (data_block == nullptr) {
		data_block = doc.find_block("particles");
	}
	if (data_block == nullptr) {
		std::cerr << "ERROR: particles data block not found" << std::endl;
		return {};
	}

	enum Labels {
		DefocusU, DefocusV, DefocusAngle, AngleRot, AngleTilt, AnglePsi, OriginXAngst, OriginYAngst, OriginZAngst,
	};

	auto table = data_block->find(
		"_rln",
		{ STR(Labels::DefocusU), STR(Labels::DefocusV), STR(Labels::DefocusAngle), "?" + STR(Labels::AngleRot),
			"?" + STR(Labels::AngleTilt), "?" + STR(Labels::AnglePsi), "?" + STR(Labels::OriginXAngst), "?" + STR(Labels::OriginYAngst), "?"
				+ STR(Labels::OriginZAngst) });

	std::vector<Projection> projections { };
	size_t proj_index = 0;
	for (auto row : table) {
		auto defocusU = as_number(row.at(Labels::DefocusU));
		auto defocusV = as_number(row.at(Labels::DefocusV));
		auto defocusAngle = as_number(row.at(Labels::DefocusAngle));

		bolt::Quaternion<float> orientation;
		float angleRotDeg, angleTiltDeg, anglePsiDeg = 0.0;
		if (row.has2(Labels::AngleRot) && row.has2(Labels::AngleTilt) && row.has2(AnglePsi)) {
			angleRotDeg = as_number(row.at(Labels::AngleRot));
			angleTiltDeg = as_number(row.at(Labels::AngleTilt));
			anglePsiDeg = as_number(row.at(Labels::AnglePsi));
			orientation = bolt::eulerAnglesZYZToQuaternion(
				angleRotDeg / 180.0 * M_PI,
				angleTiltDeg / 180.0 * M_PI,
				anglePsiDeg / 180.0 * M_PI);
		} else {
			auto dir = ems::sampleSphereUniform(float(rand()) / float(RAND_MAX), float(rand()) / float(RAND_MAX), nullptr);
			orientation = bolt::rotation2VectorsToQuaternion(ems::Num3 { 0, 0, 1 }, dir);
		}

		float originXangst, originYangst, originZangst = 0.0;
		if (row.has(Labels::OriginXAngst) && row.has2(Labels::OriginYAngst) && row.has2(OriginZAngst)) {
			originXangst = as_number(row.at(Labels::OriginXAngst));
			originYangst = as_number(row.at(Labels::OriginYAngst));
			originZangst = as_number(row.at(Labels::OriginZAngst));
		}
		auto pixel_size = BOHR_TO_ANG * bolt::norm(setup.default_detector_x_vector) / image_size[0];

		CtfParameters ctfparams;
		ctfparams.defocus = (defocusU + defocusV) / 2.0;

		ctfparams.defocusAngle = (defocusAngle / 180.0) * M_PI;
		ctfparams.defocusU = defocusU;
		ctfparams.defocusV = defocusV;

		ctfparams.amp_cont = amplitude_contrast;

		// Copied from relion - modified de Broglie equation - relativistic effects are taken into account!
		// See http://en.wikipedia.org/wiki/Electron_diffraction
		ctfparams.lambda = 12.2643247 / sqrt(setup.energy_keV * 1e3 * (1. + setup.energy_keV * 1e3 * 0.978466e-6));

		ctfparams.spher_ab = spherical_aberration * 1e7;
		ctfparams.bfactor = 0;
		ctfparams.width = image_size[0];
		ctfparams.height = image_size[1];

		ctfparams.angpix = pixel_size;

		bolt::Float3 shift(originXangst / pixel_size, originYangst / pixel_size, originZangst / pixel_size);

		Projection newProjection( { image_size[0], image_size[1] }, ctfparams, orientation, shift, proj_index, 1);
		projections.push_back(newProjection);
		proj_index++;

	}

	return projections;
}
}

#endif /* SRC_STAR_FILE_READER_H_ */
