/*
 * test_main.cpp
 *
 *  Created on: Nov 26, 2019
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#include "external/StarFileReader.h"
#include "external/projection.h"
#include "external/ctf_convolve.h"
#include "star_file_reader.h"

#include "helper_func.h"
#include "atomic_hepler_func.h"
#include "ems/atomic_ems_components.h"
#include "ems/ems.h"

#include "gemmi/model.hpp"
#include "gemmi/ccp4.hpp"

PARAM(pdbFilePath, std::string, "filename of the PDB input");

PARAM(projectionsFile, std::string, "filename of the projections mrc");
PARAMWD(outputFile, std::string, "", "filename of the output");

PARAM(iterations, size_t, "number of iterations to do");

using namespace ems;

constexpr bool USE_DEVICE = true;

template<typename TType, int TDimension>
using ImgType = typename std::conditional_t<USE_DEVICE, bolt::DeviceImage<TType, TDimension>, bolt::HostImage<TType, TDimension>>;
using RenderViewType = typename ImgType<float,2>::ViewType;

template<typename TType, int TDimension>
using IntImgType = typename std::conditional_t<USE_DEVICE, bolt::TextureImage<TType, TDimension>, bolt::HostImage<TType, TDimension>>;
using ComplexImage = typename std::conditional_t<USE_DEVICE, bolt::DeviceImage<bolt::DeviceComplexType,2>,bolt::HostImage<bolt::HostComplexType,2>>;
using GeneratorsImage = decltype(createRandomGeneratorsImage<USE_DEVICE>(typename ImgType<float,2>::SizeType {},0));

int main(int argc, char** argv) {
	auto params = loadParams(argc, argv);

	std::unique_ptr<GeneratorsImage> generators_image;

	std::string pdb_file = params.get<pdbFilePath>();

	auto model_name = params.get<ems_Model>();

	//TODO load projections, compute diff, update and so on, test pdb export
	auto projections_mrc = params.get<projectionsFile>();
//	auto vol_dims = mrc::GetDimensions(projections_mrc);
	auto header = mrc::GetHeader(projections_mrc);
	float apix = mrc::GetApix(header);

	unsigned char max_at_number;
	size_t volume_size_single;
	auto atoms = prepareAtomsVector(pdb_file, AVERAGE_BOND_LENGTH_ANG, volume_size_single, max_at_number);
	IVoxel volume_size = t<IVoxel>(volume_size_single, volume_size_single, volume_size_single);

	auto setup = ElectronMicroscopeSetup { volume_size, t<Bohr>(ANG_TO_BOHR * AVERAGE_BOND_LENGTH_ANG), params };

	//ATOMS IMAGE VIEW
	bolt::HostImage<EmsAtom, 1> host_atoms_image { atoms.size() + 1 };
	auto host_atoms_view = host_atoms_image.view();
	host_atoms_view[0] = EmsAtom { };
	for (size_t index = 0; index < atoms.size(); ++index) {
		host_atoms_view[index + 1] = atoms[index];
	}
	ImgType<EmsAtom, 1> atoms_image { atoms.size() + 1 };
	auto atoms_view = atoms_image.view();
	bolt::copy(host_atoms_view, atoms_view);

	//POTENTIAL VALUES VIEW
	Bohr max_distance = Bohr { 6 };
	bolt::HostImage<Num, 2> potential_values_image { bolt::Int2 { max_at_number + 1, 20 } };
	fillPotentialValuesView(potential_values_image.view(), max_distance);
	IntImgType<Num, 2> int_potential_values_image { potential_values_image.size() };
	bolt::copy(potential_values_image.view(), int_potential_values_image.view());
	auto potential_values_view = utils::makeInterpolated(int_potential_values_image.view());
	auto potential_values = ems::PotentialValues<decltype(potential_values_view)> { potential_values_view, max_distance };

	//CELL IMAGE VIEW
	ImgType<size_t, 3> cell_image { u(volume_size) };
	auto cell_int_view = cell_image.view();

	auto atomic_physical_volume = makeAtomicPhysicalVolume(cell_int_view, atoms_view, potential_values, setup);
	atomic_physical_volume.UpdateCells();

	//SHIFTING VIEW
	ImgType<Num3, 2> shifting_image { u(setup.emitter_size) };
	auto shifting_view = shifting_image.view();

	//MAJORANT

	auto majorant = potential_values_image.view()[bolt::Int2 { max_at_number, 10 }];
	std::cout << "majorant is: " << majorant << std::endl;
	std::cout << "volume size: " << volume_size_single << std::endl;

	generators_image = std::make_unique<GeneratorsImage>(
		createRandomGeneratorsImage<USE_DEVICE>(u(setup.emitter_size), params.get<ems_RandomSeed>()));

	PeriodicTable p { };

	auto rutherford_scattering = RutherfordAtomicShiftingAndScattering<decltype(atomic_physical_volume), decltype(shifting_view)> {
		atomic_physical_volume, shifting_view, RutherfordFormulae { p.get(params.get<ems_RutherSamplingAtomicNumber>()), params.get<
			ems_ElectronEnergy>() }, setup.rutherford.meanFreePath(), majorant };

	auto em_rutherford = constructElectronMicroscope(setup, rutherford_scattering, CounterImageFormationAtomic { });

	//Projections
	std::string star_file = params.get<projectionsStarFile>();

	bolt::Int2 image_size = bolt::Int2(u(params.get<detectorPixelSize>()), u(params.get<detectorPixelSize>()));

	std::vector<ctftools::Projection> projections { };
	projections = ctftools::readStarFile(star_file, image_size, setup);

	ems::Ems<true>::ImgType<float, 2> rendered_image { image_size };
	auto rendered_view = rendered_image.view();

	ems::Ems<false>::ImgType<float, 2> host_image { image_size };
	auto host_view = host_image.view();

	size_t start = params.get<particlesRange>()[0];
	size_t end = std::min(size_t(params.get<particlesRange>()[1]), projections.size());

	CtfConvolver<decltype(rendered_view)> convolver { image_size };

	auto output = params.get<outputFile>();
	if (output == "") {
		output = "rendered_particles_" + std::to_string(start) + "_" + std::to_string(end) + ".mrc";
	}

	auto output_apix = BOHR_TO_ANG * bolt::norm(u(setup.default_detector_x_vector)) / rendered_view.size()[0];

	auto dim3D = bolt::Vector<int, 3>(rendered_view.size()[0], rendered_view.size()[1], end - start);
	mrc::FormatDescription::Header header = mrc::SimpleVolumeHeader(
		dim3D[0],
		dim3D[1],
		dim3D[2],
		mrc::GetPixelType<float>(),
		0,
		0,
		0,
		output_apix);

	std::ofstream mrcFile(output, std::ios_base::out | std::ios_base::binary);
	if (!mrcFile.good()) {
		MRC_THROW(mrc::FileNotFoundError() << mrc::GetFileNotFoundInfo(output));
	}
	mrcFile << header;

	bool applyCtf = params.get<ems::ems_ApplyCTF>();
	auto iterations_count = params.get<iterations>();
	for (size_t iter = 0; iter < iterations_count; ++iter) {
		size_t particle_index = start + iter % (end - start);

		auto ctf_params = projections[particle_index].PsfParams<ctftools::CtfParameters>();
		auto detector_distance = ems::Bohr { 0 };
		if (params.get<ems::ems_ApplyDefocus>()) {
			detector_distance = max(ems::Bohr { ems::angToBohr(abs(ctf_params.defocus)) }, ems::Bohr { 0 });
		}

		auto detector = em_rutherford.render(
			rendered_view,
			projections[particle_index].Orientation(),
			{ },
			detector_distance,
			generators_image->view());
		if (applyCtf) {
			convolver.ctfConvolve2D(rendered_view, projections[particle_index]);
		}




		bolt::copy(rendered_view, host_view);
		using namespace mrc;
		mrcFile << host_view;

		std::cout << "particle: " << particle_index << " done " << std::endl;
	}

	mrcFile.close();

	return 0;
}
