#pragma once

#include <iostream>
#include <cmath>
#include <random>

#include <boltview/fft/fft_utils.h>
#include <boltview/convolution.h>
#include "ctfParameters.h"

namespace ctftools {

template<typename TType>
class CtfGen {
	typedef bolt::Int2 SizeType;

public:
	CtfGen(CtfParameters params) : ctf_parameters_(params) {
		center_[0] = 0;
		center_[1] = ctf_parameters_.height / 2;
	}

	BOLT_DECL_HYBRID
	cufftComplex shifted(int i, int j) const {
		if (j < (ctf_parameters_.height - 1) / 2 + 1) {
			return (*this)(i, j + (ctf_parameters_.height) / 2);
		} else {
			return (*this)(i, j - (ctf_parameters_.height - 1) / 2 - 1);
		}
	}

	BOLT_DECL_HYBRID
	cufftComplex operator()(int i, int j) const;

	BOLT_DECL_HYBRID
	void operator()(cufftComplex& val, bolt::Int2 pos) const;

private:
	BOLT_DECL_HYBRID
	TType Phase(TType s) const;
	BOLT_DECL_HYBRID
	TType Ctf(TType s) const;
	BOLT_DECL_HYBRID
	TType CtfRelion(TType sx, TType sy) const;
	BOLT_DECL_HYBRID
	TType Envelope(TType s) const;
	BOLT_DECL_HYBRID
	TType FermiCutoff(TType s, TType threshold, TType sharpness) const;
	BOLT_DECL_HYBRID
	TType PolynomialCutoff(TType s, TType low, TType high) const;

	SizeType center_;
	CtfParameters ctf_parameters_;
};

template<typename TType, int tDimension>
class MultiDimGaussGenerator {
public:
	static const int kDimension = tDimension;
	typedef bolt::Vector<int, tDimension> SizeType;
	typedef bolt::Vector<int, tDimension> IndexType;

	MultiDimGaussGenerator(GaussParameters params) : params_(params), gg_(params.width, params.numStdDevAtEdge) {
	}
	MultiDimGaussGenerator(MultiDimGaussGenerator&& other) : params_(other.params_), gg_(std::move(other.gg_)) {
	}

	TType operator()(IndexType idx) const {
		TType value = params_.normalizationFactor;
		for (int i = 0; i < kDimension; ++i) {
			value *= gg_.Get(idx[i]);
		}
		return value;
	}

	SizeType Size() const {
		SizeType size;
		for (int i = 0; i < kDimension; ++i) {
			size[i] = gg_.Size();
		}
		return size;
	}
private:
	GaussParameters params_;
	bolt::detail::GaussGenerator<TType> gg_;
};

} // namespace ctftools

#include "ctfgen.tcc"
