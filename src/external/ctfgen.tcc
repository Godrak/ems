#pragma once

namespace ctftools {

BOLT_DECL_HYBRID
inline cufftComplex operator*(cufftComplex b, cufftComplex a) {
	return make_cuComplex(b.x * a.x - b.y * a.y, a.x * b.y + b.x * a.y);
}

template<typename TType>
cufftComplex CtfGen<TType>::operator()(int i, int j) const {
	/*
	 int height= ctf_parameters_.height;
	 int width= ctf_parameters_.width;

	 if (ctf_parameters_.height%2==0){
	 // if the size is even, ctf is generated for (size-1) - because of symmetry
	 height= ctf_parameters_.height-1;
	 width= ctf_parameters_.width-1;
	 }
	 */
	TType angpix = ctf_parameters_.angpix;
	TType sx = (i - center_[0]) / (ctf_parameters_.width * angpix);
	TType sy = (j - center_[1]) / (ctf_parameters_.height * angpix);

	TType s = sqrt(sx * sx + sy * sy);

	cufftComplex z = make_cuComplex(CtfRelion(sx, sy) * Envelope(s), 0);
	/*
	 //Phase shift
	 int vertShift=(j-center_[1])*(height+1)/2; // number of pixels - by how many to shift vertically (down)
	 int horShift=(i-center_[0])*(width+1)/2; // number of pixels- by how many to shift horizontally (right)
	 float phase=2*3.14159265358979323846*(float(horShift)/ctf_parameters_.width+float(vertShift)/ctf_parameters_.height); // phase corresponding to chosen shifts

	 cufftComplex phaseshift=make_cuComplex(cos(phase),sin(phase));
	 */
	/*
	 // "circular cuttof" - makes the result rotationally symmetric (vanishes values in corners)
	 if (sqrt( pow((i - center_[0]),2)+pow((j - center_[1]),2))>((width)/2)){
	 z.x=0;z.y=0;
	 }
	 */
	return z; //*phaseshift;
}

template<typename TType>
void CtfGen<TType>::operator()(cufftComplex& val, bolt::Int2 pos) const {

	val = operator()(pos[0], pos[1]);
}

template<typename TType>
TType CtfGen<TType>::Phase(TType s) const {
	TType ssq = s * s;
	TType lsq = ctf_parameters_.lambda * ctf_parameters_.lambda;
	TType pi = 3.14159265358979323846;
	TType value = pi * ctf_parameters_.lambda * ssq * (ctf_parameters_.spher_ab * lsq * ssq / 2 - ctf_parameters_.defocus);

	return value;
}

template<typename TType>
TType CtfGen<TType>::CtfRelion(TType sx, TType sy) const {

	TType s = sqrtf(sx * sx + sy * sy);

	TType ssq = s * s;
	TType lsq = ctf_parameters_.lambda * ctf_parameters_.lambda;
	TType pi = 3.14159265358979323846;

	TType ellipsoid_ang = atan2(sy, sx) - ctf_parameters_.defocusAngle;
	TType cos_ellipsoid_ang_2 = cos(2 * ellipsoid_ang);
	TType deltaf = (-1 * (ctf_parameters_.defocusU + ctf_parameters_.defocusV) * 0.5
		- (ctf_parameters_.defocusU - ctf_parameters_.defocusV) * 0.5 * cos_ellipsoid_ang_2);

	TType Q0 = ctf_parameters_.amp_cont;

	TType value = -sin(
		pi * ctf_parameters_.lambda * ssq * (ctf_parameters_.spher_ab * lsq * ssq / 2.0) + (pi * ctf_parameters_.lambda * ssq * deltaf)
			- atan(Q0 / sqrt(1.0 - Q0 * Q0)));
	return value;
}

template<typename TType>
TType CtfGen<TType>::Ctf(TType s) const {
	TType ampsq = ctf_parameters_.amp_cont * ctf_parameters_.amp_cont;
	TType prefac = sqrt(1 - ampsq);
	TType phase = Phase(s);
	TType value = prefac * sin(phase) - ctf_parameters_.amp_cont * cos(phase);
	return value;
}

template<typename TType>
TType CtfGen<TType>::Envelope(TType s) const {
	return exp(-ctf_parameters_.bfactor * s * s / 4);
}

template<typename TType>
TType CtfGen<TType>::FermiCutoff(TType s, TType threshold, TType sharpness) const {
	TType value = 1 / (1 + exp((s - threshold) / sharpness));
	return (value >= 1e-15) ? value : 0;
}

template<typename TType>
TType CtfGen<TType>::PolynomialCutoff(TType s, TType low, TType high) const {
	if (s <= low) {
		return 1;
	} else if (s >= high) {
		return 0;
	} else {
		TType B = high * high * (high - 3 * low) / 2;
		TType A = high - low;
		A = A * A * A / 2;
		TType result = (s * (3 * low * high + s * (s - 3 * (low + high) / 2)) + B) / A;
		return result;
	}
}
// template<typename TPrecision>
// TPrecision gamma(TPrecision sx, TPrecision sy, TPrecision defoc, TPrecision spherab, TPrecision lambda) {
//   TPrecision ssq = sx*sx+sy*sy;
//   TPrecision lsq = lambda*lambda;
//   TPrecision value = constants::pi * lambda * ssq * (spherab * lsq * ssq / 2 - defoc);
//   return value;
// }
//
// template<typename TPrecision>
// TPrecision ctf(TPrecision ampconst, TPrecision phase) {
//   TPrecision prefac = sqrt(1-ampconst*ampconst);
//   TPrecision value = prefac*sin(phase) - ampconst * cos(phase);
//   return value;
// }
}// namespace ctftools
