#pragma once

#include "../external/projection.h"
#include "mrc_io.h"

namespace ctftools {

template<typename TElement>
class StarFileReader {
public:

	static void Read(std::string starfilename, std::vector<Projection>& projections, bolt::Int2 dataSize) {

		std::fstream infoInput(starfilename, std::fstream::in);
		std::string line;

		std::getline(infoInput, line);
		std::stringstream ss(line);
		std::string word;

		// skip lines containing white spaces only
		ss >> std::ws;

		while (ss.eof()) {
			std::getline(infoInput, line);
			ss.clear();
			ss.str(line);
			ss >> std::ws;
		}

		ss >> word; // this should be "#" in any starfile

		bool version31 = false;

		ss >> word;
		if (word == "version") {
			ss >> word;
			if (word == "30001") {
				//star file version 3.1
				version31 = true;
			}
		}

		if (version31) {
			Read_version31(starfilename, projections, dataSize);
		} else {
			Read_versionOld(starfilename, projections, dataSize);
		}

		infoInput.close();
	}

private:

	static constexpr TElement bfactor_ = 0;

	static void Read_versionOld(std::string starfilename, std::vector<Projection>& projections, bolt::Int2 dataSize) {
		// Reads information about projections from starfile and stores them to a vector of Projections (needs to be prealocated to at least the size given by numProjections)

		bool ignoreSubset = true;

		std::fstream infoInput(starfilename, std::fstream::in);
		std::string line;

		std::getline(infoInput, line);
		std::stringstream ss(line);
		std::string word;

		// get rid of the file header
		while (word[0] != '_') {
			std::getline(infoInput, line);
			ss.clear();
			ss.str(line);
			ss >> word;
		}

		// load column labels to map
		std::map<std::string, int> labels;
		int index = 0;
		while (word[0] == '_') {
			labels[word] = index;
			std::getline(infoInput, line);
			ss.clear();
			ss.str(line);
			ss >> word;
			index++;
		}

		// projection information load
		ss.clear();
		ss.str(line);

		std::setlocale(LC_NUMERIC, "en_US.UTF-8");

		float defocusU;
		float defocusV;
		float defocusAngle;
		float amplitudeContrast;
		float sphericalAberation;
		float voltage;

		float angleRot;
		float angleTilt;
		float anglePsi;

		float shiftX;
		float shiftY;
		float shiftZ;

		float magnification;
		float pixelSize;

		int projectionNumber;

		ss.str(line);
		while (!infoInput.eof()) {

			for (int j = 0; j < labels.size(); ++j) {
				ss >> word;
				if (j == labels["_rlnImageName"]) {
					projectionNumber = std::stoi(word) - 1;
				}
				if (j == labels["_rlnDefocusU"]) {
					defocusU = std::stof(word);
				}
				if (j == labels["_rlnDefocusV"]) {
					defocusV = std::stof(word);
				}
				if (j == labels["_rlnDefocusAngle"]) {
					defocusAngle = std::stof(word);
				}
				if (j == labels["_rlnAngleRot"]) {
					angleRot = std::stof(word);
				}
				if (j == labels["_rlnAngleTilt"]) {
					angleTilt = std::stof(word);
				}
				if (j == labels["_rlnAnglePsi"]) {
					anglePsi = std::stof(word);
				}
				if (j == labels["_rlnAmplitudeContrast"]) {
					amplitudeContrast = std::stof(word);
				}
				if (j == labels["_rlnSphericalAberration"]) {
					sphericalAberation = std::stof(word);
				}
				if (j == labels["_rlnVoltage"]) {
					voltage = std::stof(word);
				}
				if (j == labels["_rlnOriginX"]) {
					shiftX = std::stof(word);
				}
				if (j == labels["_rlnOriginY"]) {
					shiftY = std::stof(word);
				}
				if (j == labels["_rlnOriginZ"]) {
					shiftZ = std::stof(word);
				}
				if (j == labels["_rlnMagnification"]) {
					magnification = std::stof(word);
				}
				if (j == labels["_rlnDetectorPixelSize"]) {
					pixelSize = std::stof(word);
				}
				if (!ignoreSubset) {
					if (j == labels["_rlnRandomSubset"]) {
//						currentSubsetIndex = std::stoi(word);
					}
				}
			}

			if (ignoreSubset) {

				const float pi = 3.141592653589;
				CtfParameters ctfparams;

				ctfparams.defocus = (defocusU + defocusV) / 2.0;

				ctfparams.defocusAngle = (defocusAngle / 180.0) * pi;
				ctfparams.defocusU = defocusU;
				ctfparams.defocusV = defocusV;

				ctfparams.amp_cont = amplitudeContrast;

				// Copied from relion - modified de Broglie equation - relativistic effects are taken into account!
				// See http://en.wikipedia.org/wiki/Electron_diffraction
				ctfparams.lambda = 12.2643247 / sqrt(voltage * 1e3 * (1. + voltage * 1e3 * 0.978466e-6));

				ctfparams.spher_ab = sphericalAberation * 1e7;
				ctfparams.bfactor = bfactor_;
				ctfparams.width = dataSize[0];
				ctfparams.height = dataSize[1];
				ctfparams.angpix = (pixelSize / magnification) * 1e4;

				std::getline(infoInput, line);
				ss.clear();
				ss.str(line);

				bolt::Float3 shift(shiftX, shiftY, shiftZ);

				auto orientation = bolt::eulerAnglesZYZToQuaternion(angleRot / 180.0 * pi, angleTilt / 180.0 * pi, anglePsi / 180.0 * pi);

				Projection newProjection( { dataSize[0], dataSize[1] }, ctfparams, orientation, shift, projectionNumber);
				projections.push_back(newProjection);
			}

		}

		infoInput.close();
	}

	static void Read_version31(std::string starfilename, std::vector<Projection>& projections, bolt::Int2 dataSize) {
		// Reads information about projections from starfile and stores them to a vector of Projections (needs to be prealocated to at least the size given by numProjections)

		std::setlocale(LC_NUMERIC, "en_US.UTF-8");

		bool ignoreSubset = true;

		std::fstream infoInput(starfilename, std::fstream::in);
		std::string line;

		std::getline(infoInput, line);
		std::stringstream ss(line);
		std::string word;
		ss >> word;

		// get rid of the file header
		while (word[0] != '_') {
			std::getline(infoInput, line);
			ss.clear();
			ss.str(line);
			ss >> word;
			//   std::cout<<"header word "<<word<<std::endl;
		}

		// load column labels to map
		std::map<std::string, int> labels;
		int index = 0;
		while (word[0] == '_') {
			// std::cout<<"label word "<<word<<" index "<<index<<std::endl;
			labels[word] = index;
			std::getline(infoInput, line);
			ss.clear();
			ss.str(line);
			ss >> word;
			index++;
		}

		float amplitudeContrast;
		float sphericalAberation;
		float voltage;
		float pixelSize;

		ss.str(line);
		for (int j = 0; j < labels.size(); ++j) {
			ss >> word;
			if (j == labels["_rlnAmplitudeContrast"]) {
				amplitudeContrast = std::stof(word);
			}
			if (j == labels["_rlnSphericalAberration"]) {
				sphericalAberation = std::stof(word);
			}
			if (j == labels["_rlnVoltage"]) {
				voltage = std::stof(word);
			}
			if (j == labels["_rlnImagePixelSize"]) {
				pixelSize = std::stof(word);
			}
		}

		ss.clear();
		ss.str(line);

		float defocusU;
		float defocusV;
		float defocusAngle;

		float angleRot;
		float angleTilt;
		float anglePsi;

		float shiftX;
		float shiftY;
		float shiftZ;

		float loglikelihood;
		int projectionNumber;

		//get rid of "second header"
		while (word[0] != '_') {
			std::getline(infoInput, line);
			ss.clear();
			ss.str(line);
			ss >> word;
		}

		// load column labels to map
		labels.clear();
		index = 0;
		while (word[0] == '_') {
			labels[word] = index;
			std::getline(infoInput, line);
			ss.clear();
			ss.str(line);
			ss >> word;
			index++;
		}

		ss.str(line);

		int counter = 0;
		int counterFull = 0;

		while (!infoInput.eof()) {

			for (int j = 0; j < labels.size(); ++j) {
				ss >> word;
				if (j == labels["_rlnImageName"]) {
					projectionNumber = std::stoi(word) - 1;
				}
				if (j == labels["_rlnDefocusU"]) {
					defocusU = std::stof(word);
				}
				if (j == labels["_rlnDefocusV"]) {
					defocusV = std::stof(word);
				}
				if (j == labels["_rlnDefocusAngle"]) {
					defocusAngle = std::stof(word);
				}
				if (j == labels["_rlnAngleRot"]) {
					angleRot = std::stof(word);
				}
				if (j == labels["_rlnAngleTilt"]) {
					angleTilt = std::stof(word);
				}
				if (j == labels["_rlnAnglePsi"]) {
					anglePsi = std::stof(word);
				}
				if (j == labels["_rlnOriginXAngst"]) {
					shiftX = std::stof(word);
				}
				if (j == labels["_rlnOriginYAngst"]) {
					shiftY = std::stof(word);
				}
				if (j == labels["_rlnOriginZAngst"]) {
					shiftZ = std::stof(word);
				}
				if (j == labels["_rlnMaxValueProbDistribution"]) {
					loglikelihood = 1;                //std::stof(word);
					//std::cout<<"projection number "<< counter<<" loglikelihood "<<loglikelihood<<std::endl;
				}
				if (!ignoreSubset) {
					if (j == labels["_rlnRandomSubset"]) {
//						currentSubsetIndex = std::stoi(word);
					}
				}

			}
			counterFull++;
			if (ignoreSubset) {
				counter++;

				const float pi = 3.141592653589;
				CtfParameters ctfparams;

				ctfparams.defocus = (defocusU + defocusV) / 2.0;

				ctfparams.defocusAngle = (defocusAngle / 180.0) * pi;
				ctfparams.defocusU = defocusU;
				ctfparams.defocusV = defocusV;

				ctfparams.amp_cont = amplitudeContrast;

				// Copied from relion - modified de Broglie equation - relativistic effects are taken into account!
				// See http://en.wikipedia.org/wiki/Electron_diffraction
				ctfparams.lambda = 12.2643247 / sqrt(voltage * 1e3 * (1. + voltage * 1e3 * 0.978466e-6));

				ctfparams.spher_ab = sphericalAberation * 1e7;
				ctfparams.bfactor = bfactor_;
				ctfparams.width = dataSize[0];
				ctfparams.height = dataSize[1];

				ctfparams.angpix = pixelSize;

				bolt::Float3 shift(shiftX / pixelSize, shiftY / pixelSize, shiftZ / pixelSize);

				auto orientation = bolt::eulerAnglesZYZToQuaternion(angleRot / 180.0 * pi, angleTilt / 180.0 * pi, anglePsi / 180.0 * pi);

				Projection newProjection( { dataSize[0], dataSize[1] }, ctfparams, orientation, shift, projectionNumber, loglikelihood);
				projections.push_back(newProjection);
			}

			std::getline(infoInput, line);
			ss.clear();
			ss.str(line);

			// skip lines containing white spaces only (rarely happens in relion star files)
			ss >> std::ws;
			if (ss.eof()) {
				std::getline(infoInput, line);
				ss.clear();
				ss.str(line);
			}

		}
		infoInput.close();
	}

};

} //namespace ctftools
