//
// (c) Lukas Marsalek, lukas.marsalek@eyen.eu, 2018
//

#pragma once

#include <tuple>
#include <tiffio.h>
#include "boltview/exceptions.h"

namespace mrc {

using namespace bolt;

struct UnsupportedTiffFormat: ExceptionBase {
};

typedef boost::error_info<struct tag_tiff_error, std::string> UnsupportedTiffFormatInfo;

inline UnsupportedTiffFormatInfo GetUnsuppportedTiffFormatInfo(const std::string& message) {
	return UnsupportedTiffFormatInfo(message);
}

#define TR_THROW(x) BOOST_THROW_EXCEPTION(x)

using namespace ctftools;

namespace libDip {

std::tuple<int, int, int> TiffDimensions(const std::string filename) {
	TIFF* tiffIn = TIFFOpen(filename.c_str(), "r");
	try {
		if (!tiffIn) {
			TR_THROW(InputFileError() << GetInputFileErrorInfo(filename));
		}

		int width, height;
		TIFFGetField(tiffIn, TIFFTAG_IMAGEWIDTH, &width);
		TIFFGetField(tiffIn, TIFFTAG_IMAGELENGTH, &height);
		TIFFClose(tiffIn);
		return std::make_tuple(width, height, 1);

	} catch (std::exception& e) {
		TIFFClose(tiffIn);
		throw e;
	}
}

std::vector<float> TiffData(const std::string filename) {
	TIFF* tiffIn = TIFFOpen(filename.c_str(), "r");
	try {
		if (!tiffIn) {
			TR_THROW(InputFileError() << GetInputFileErrorInfo(filename));
		}

		int width, height;
		TIFFGetField(tiffIn, TIFFTAG_IMAGEWIDTH, &width);
		TIFFGetField(tiffIn, TIFFTAG_IMAGELENGTH, &height);

		uint16_t spp, photometricTag, planarConfigTag, formatTag, orientationTag;
		TIFFGetField(tiffIn, TIFFTAG_SAMPLESPERPIXEL, &spp);
		TIFFGetField(tiffIn, TIFFTAG_PHOTOMETRIC, &photometricTag);
		TIFFGetField(tiffIn, TIFFTAG_PLANARCONFIG, &planarConfigTag);
		TIFFGetField(tiffIn, TIFFTAG_SAMPLEFORMAT, &formatTag);
		TIFFGetField(tiffIn, TIFFTAG_ORIENTATION, &orientationTag);

		std::vector<uint16_t> bpp(spp);
		TIFFGetField(tiffIn, TIFFTAG_BITSPERSAMPLE, &bpp[0]);

		if (spp != 1 || photometricTag != PHOTOMETRIC_MINISBLACK || formatTag != SAMPLEFORMAT_IEEEFP
			|| orientationTag != ORIENTATION_TOPLEFT || bpp[0] != sizeof(float) * 8) {
			TR_THROW(UnsupportedTiffFormat() << GetUnsuppportedTiffFormatInfo("Unsupported TIFF format"));
		}

		std::vector<float> result((TIFFStripSize(tiffIn) * TIFFNumberOfStrips(tiffIn)) / sizeof(float));
		char* buffer = reinterpret_cast<char*>(&result[0]);

		for (tstrip_t strip = 0; strip < TIFFNumberOfStrips(tiffIn); ++strip) {
			buffer += TIFFReadEncodedStrip(tiffIn, strip, buffer, static_cast<tsize_t>(-1));
		}

		TIFFClose(tiffIn);
		return std::move(result);

	} catch (std::exception& e) {
		TIFFClose(tiffIn);
		throw e;
	}
}

//template <typename TView>
//void WriteViewTo8bitTiff(const TView& imageView, const boost::filesystem::path& filename){
//	using SourceGilView_type = typename GilViewTypeFromImageViewType<TView>::type;
//	SourceGilView_type gilView;
//	if(imageView.kIsDeviceView){
//		GilAdaptorImage<SourceGilView_type> gilImage(imageView.Size());
//		Copy(imageView, gilImage.HostImageView());
//		gilView = gilImage.GilView();
//	} else {
//		gilView = bg::interleaved_view(imageView.Size()[0], imageView.Size()[1], reinterpret_cast<typename GilViewTraits<SourceGilView_type>::Pixel_type*>(imageView.Pointer()), imageView.Size()[0] * sizeof(typename TView::Element));
//	}

//	auto byteView = bg::color_converted_view<bg::gray8_pixel_t>(gilView, MaxRangeConverter<bg::gray8_pixel_t, SourceGilView_type>(gilView));
//	bg::write_view(filename.string(), byteView, bg::tiff_tag());
//}

}
//namespace libDip

}
