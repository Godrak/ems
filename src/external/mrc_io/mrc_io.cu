//
// (c) Lukas Marsalek, lukas.marsalek@eyen.eu, 2018
//

#include "boltview/device_image.h"
#include "boltview/host_image.h"

#include "file_error.h"
#include "mrc_format.h"

namespace mrc {

FormatDescription::Header GetHeader(const std::string filename) {
	std::ifstream mrcFile(filename, std::ios::binary);
	if (!mrcFile.good()) {
		MRC_THROW(InputFileError() << GetInputFileErrorInfo(filename));
	}
	FormatDescription::Header header;
	mrcFile.read(reinterpret_cast<char*>(&header), sizeof(FormatDescription::Header));
	return header;
}

bool IsMrc(const FormatDescription::Header& header) {
	return HasCmapId(header);
}

bool IsMrc(const std::string filename) {
	return IsMrc(GetHeader(filename));
}

std::tuple<int, int, int> GetDimensions(const std::string filename) {
	FormatDescription::Header header = GetHeader(filename);
	if (!IsMrc(header)) {
		MRC_THROW(
			UnsupportedFileFormat() << GetUnsuppportedFileFormatInfo((boost::format("File %1% is not in MRC format") % filename).str()));
	}
	return GetDimensions(header);
}

} //namespace mrc
