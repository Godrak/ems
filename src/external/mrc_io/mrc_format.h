//
// (c) Lukas Marsalek, l.marsalek@seznam.cz, 2015
//

#pragma once

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <stdexcept>
#include <tuple>
#include <boost/format.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/vector_c.hpp>
#include <boost/mpl/integral_c.hpp>
#include <boost/bind.hpp>
#include <type_traits>

#include "file_error.h"

#define MRC_THROW(x) GENERIC_THROW(x)

namespace mrc {

using namespace bolt;

struct MrcModeError: ExceptionBase {
};
inline MessageErrorInfo GetMrcModeErrorInfo(int mrcMode) {
	return MessageErrorInfo((boost::format("Unknown MRC mode value : %1%") % mrcMode).str());
}

/*
 * Description of the MRC file format
 * Old-style MRC is modeled after http://www.2dx.unibas.ch/documentation/mrc-software/mrc-file-format-specifications
 * IMOD-style MRC is modeled after http://bio3d.colorado.edu/imod/doc/mrc_format.txt
 */
struct FormatDescription {
	typedef std::ofstream OutputFileStream_type;
	/* Types of pixel in MRC as used by IMOD. These values are given in the "mode" field.
	 * 0 = unsigned or signed bytes depending on flag in imodStamp, only unsigned bytes before IMOD 4.2.23
	 * 1 = signed short integers (16 bits)
	 * 2 = float
	 * 3 = short * 2, (used for complex data)
	 * 4 = float * 2, (used for complex data)
	 * 6 = unsigned 16-bit integers (non-standard)
	 * 16 = unsigned char * 3 (for rgb data, non-standard) */

	//Purpose of having this MPL vector is to have the non-consecutive enum type iterable
	typedef boost::mpl::vector_c<int, 0, 1, 2, 3, 4, 6, 16, 35> PixelTypeValuesList;

	/* There is one special value - Signed bytes - that does not correspond to the standard.
	 * This value is never written to the file directly, it is written as a combination of
	 * mode == 0 & ImodBitFlag == 1.
	 * */
	enum class PixelType : int {
		Bytes = boost::mpl::at<PixelTypeValuesList, boost::mpl::int_<0> >::type::value,
		SignedShort = boost::mpl::at<PixelTypeValuesList, boost::mpl::int_<1> >::type::value,
		Float = boost::mpl::at<PixelTypeValuesList, boost::mpl::int_<2> >::type::value,
		ComplexShort = boost::mpl::at<PixelTypeValuesList, boost::mpl::int_<3> >::type::value,
		ComplexFloat = boost::mpl::at<PixelTypeValuesList, boost::mpl::int_<4> >::type::value,
		UnsignedShort = boost::mpl::at<PixelTypeValuesList, boost::mpl::int_<5> >::type::value,
		RGB = boost::mpl::at<PixelTypeValuesList, boost::mpl::int_<6> >::type::value,
		SignedBytes = boost::mpl::at<PixelTypeValuesList, boost::mpl::int_<7> >::type::value,
	};

	/* Bit flags:
	 * 1 = bytes are stored as signed
	 * 2 = pixel spacing was set from size in extended header
	 * 4 = origin is stored with sign inverted from definition xorg, yorg and zorg fields */
	struct ImodBitFlags {
		enum {
			SignedBytes = 1, PixelSpacingFromExtHeader = 2, InvertedOrigin = 4
		};
	};

	struct Header {

		int nx;             //Number of Columns
		int ny;             //Number of Rows
		int nz;             //Number of Sections

		int mode;

		int nxstart;        //Starting point of sub image (not used in IMOD)
		int nystart;
		int nzstart;

		int mx;             //Grid size in X, Y, and Z
		int my;
		int mz;

		float xlen;           //Cell size; pixel spacing = xlen/mx, ylen/my, zlen/mz
		float ylen;
		float zlen;

		float alpha;          //cell angles - ignored by IMOD
		float beta;
		float gamma;

		//These need to be set to 1, 2, and 3 for pixel spacing to be interpreted correctly
		int mapc;           //map column  1=x,2=y,3=z.
		int mapr;           //map row     1=x,2=y,3=z.
		int maps;           //map section 1=x,2=y,3=z.

		//These need to be set for proper scaling of data
		float amin;           //Minimum pixel value.
		float amax;           //Maximum pixel value.
		float amean;          //Mean pixel value.

		int ispg;           //space group number (ignored by IMOD)
		int next;           //number of bytes in extended header (called nsymbt in MRC standard)
		short creatid;        //used to be an ID number, is 0 as of IMOD 4.2.23
		char extraA[30];     //extra data  (not used, first two bytes should be 0)

		/* These two values specify the structure of data in the extended header;
		 * their meaning depend on whether the extended header has the Agard format,
		 * a series of 4-byte integers then real numbers, or has data produced by SerialEM, a series of short integers.
		 * SerialEM stores a float as two shorts, s1 and s2, by:
		 * value = (sign of s1)*(|s1|*256 + (|s2| modulo 256)) 2**((sign of s2) * (|s2|/256))*/
		short nint;           //Number of integers per section (Agard format) or number of bytes per section (SerialEM format)

		/*Number of reals per section (Agard format) or bit flags for which types of short data (SerialEM format):
		 * 1 = tilt angle * 100  (2 bytes)
		 * 2 = piece coordinates for montage  (6 bytes)
		 * 4 = Stage position * 25    (4 bytes)
		 * 8 = Magnification / 100 (2 bytes)
		 * 16 = Intensity * 25000  (2 bytes)
		 * 32 = Exposure dose in e-/A2, a float in 4 bytes
		 * 128, 512: Reserved for 4-byte items
		 * 64, 256, 1024: Reserved for 2-byte items
		 * If the number of bytes implied by these flags does not add up to the value in nint,
		 * then nint and nreal are interpreted as ints and reals per section */
		short nreal;

		char extraB[20];     //extra data (not used)

		/* 1146047817 indicates that file was created by IMOD or other software that uses bit flags in the following field */
		int imodStamp;

		int imodFlags;

		short idtype;         //( 0 = mono, 1 = tilt, 2 = tilts, 3 = lina, 4 = lins)
		short lens;
		short nd1;            //for idtype = 1, nd1 = axis (1, 2, or 3)
		short nd2;
		short vd1;            //vd1 = 100. * tilt increment
		short vd2;            //vd2 = 100. * starting angle

		/* Current angles are used to rotate a model to match a new rotated image.
		 * The three values in each set are rotations about X, Y, and Z axes, applied in the order
		 * Z, Y, X. 0,1,2 = original, 3,4,5 = current */
		float tiltangles[6];

		/* The image origin is the location of the origin of the coordinate system relative to the first pixel in the file.
		 * It is in pixel spacing units rather than in pixels.  If an original volume has an origin of 0,
		 * a subvolume should have negative origin values. NEW-STYLE MRC image2000 HEADER - IMOD 2.6.20 and above */
		float xorg;           //Origin of image
		float yorg;
		float zorg;
		char cmap[4];        //Contains "MAP "
		char stamp[4];       //First two bytes have 17 and 17 for big-endian or 68 and 65 for little-endian
		float rms;            //RMS deviation of densities from mean density

		int nlabl;          //Number of labels with useful data.
		char extraC[10][80]; //10 labels of 80 characters, blank-padded to end
	};
	//struct Header
};
//struct FormatDescription

float GetApix(const FormatDescription::Header& header);
std::string Extension();
FormatDescription::PixelType GetPixelType(const FormatDescription::Header& header);

template<typename TImageViewElement>
FormatDescription::PixelType GetPixelType();
template<>
FormatDescription::PixelType GetPixelType<float>();
template<>
FormatDescription::PixelType GetPixelType<char>();
template<>
FormatDescription::PixelType GetPixelType<unsigned char>();
template<>
FormatDescription::PixelType GetPixelType<short>();
template<>
FormatDescription::PixelType GetPixelType<unsigned short>();
template<>
FormatDescription::PixelType GetPixelType<int>();

int GetPixelSizeInBytes(const FormatDescription::Header& header);
void PrintPixelType(const FormatDescription::Header& header, std::ostream& stream);
void SetPixelType(FormatDescription::Header* header, FormatDescription::PixelType value);
std::string GetPixelTypeAsString(const FormatDescription::Header& header);
bool IsBitFlagSet(int field, int flag);
void SetBitFlag(int* field, int flag);
void UnsetBitFlag(int* field, int flag);
bool HasExtendedHeader(const FormatDescription::Header& header);
bool CreatedByImod(const FormatDescription::Header& header);
bool ContainsSignedBytes(const FormatDescription::Header& header);
bool HasCmapId(const FormatDescription::Header& header);
void SetCmapId(FormatDescription::Header* header);
bool IsValid(const FormatDescription::Header& header);
void SetMinMaxMean(FormatDescription::Header* header, const float minimum, const float maximum, const float mean);
void SetGridProperties(FormatDescription::Header* header, const float gridSizeInX, const float gridSizeInY, const float gridSizeInZ);
void SetDimensions(FormatDescription::Header* header, const int width, const int height, const int depth, const float apix);
std::tuple<int, int, int> GetDimensions(const FormatDescription::Header& header);
void ClearHeader(FormatDescription::Header* header);
int64_t DataSizeInBytes(const FormatDescription::Header& header);
int64_t SliceSizeInBytes(const FormatDescription::Header& header);
FormatDescription::Header SimpleVolumeHeader(
	const int width,
	const int height,
	const int depth,
	FormatDescription::PixelType pType,
	const float amin,
	const float amax,
	const float amean,
	const float apix);
std::ostream& operator <<(std::ostream& out, const FormatDescription::Header& header);
std::ofstream& operator <<(std::ofstream& out, const FormatDescription::Header& header);

} //namespace mrc

