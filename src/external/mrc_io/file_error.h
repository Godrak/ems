//
// (c) Lukas Marsalek, lukas.marsalek@eyen.eu, 2018
//

#pragma once

#include <boost/format.hpp>
#include "error.h"

namespace mrc {

using namespace bolt;

struct GenericFileError: ExceptionBase {
};
inline MessageErrorInfo GetFileErrorInfo(const std::string& errorMessage, const std::string& filename) {
	return MessageErrorInfo((boost::format("%1% : %2%") % errorMessage % filename).str());
}

struct FileNotFoundError: ExceptionBase {
};
inline MessageErrorInfo GetFileNotFoundInfo(const std::string& filename) {
	return GetFileErrorInfo("File not found", filename);
}

struct OutputFileError: ExceptionBase {
};
inline MessageErrorInfo GetOutputFileErrorInfo(const std::string& filename) {
	return GetFileErrorInfo("Error in output file", filename);
}

struct InputFileError: ExceptionBase {
};
inline MessageErrorInfo GetInputFileErrorInfo(const std::string& filename) {
	return GetFileErrorInfo("Error in input file", filename);
}

struct UnsupportedFileFormat: ExceptionBase {
};
typedef boost::error_info<struct tag_file_error, std::string> UnsupportedFileFormatInfo;
inline UnsupportedFileFormatInfo GetUnsuppportedFileFormatInfo(const std::string& message) {
	return UnsupportedFileFormatInfo(message);
}
}

