//
// (c) Lukas Marsalek, lukas.marsalek@eyen.eu, 2018
//

#pragma once


#include "boltview/exceptions.h"

#define GENERIC_THROW(x) BOOST_THROW_EXCEPTION(x)


