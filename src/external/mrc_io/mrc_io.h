//
// (c) Lukas Marsalek, lukas.marsalek@eyen.eu, 2018
//

#pragma once

#include "boltview/device_image.h"
#include "boltview/host_image.h"
#include "boltview/view_iterators.h"
#include "boltview/reduce.h"

#include "mrc_format.h"

namespace mrc {

using namespace bolt;

struct SliceRangeError: ExceptionBase {
};
inline MessageErrorInfo GetSliceRangeErrorInfo(int64_t startIndex, int64_t endIndex) {
	return MessageErrorInfo((boost::format("Invalid slice range: (%1%, %2%)") % startIndex % endIndex).str());
}

namespace detail {

template<typename TType, int tDimension>
Vector<int, 3> GetDimension3D(HostImageConstView<TType, tDimension> view);

template<typename TType>
Vector<int, 3> GetDimension3D(HostImageConstView<TType, 1> view) {
	return Vector<int, 3>(view.size()[0], 1, 1);
}

template<typename TType>
Vector<int, 3> GetDimension3D(HostImageConstView<TType, 2> view) {
	return Vector<int, 3>(view.size()[0], view.size()[1], 1);
}

template<typename TType>
Vector<int, 3> GetDimension3D(HostImageConstView<TType, 3> view) {
	return view.size();
}

bool IsValidSliceRange(const FormatDescription::Header& header, int64_t startIndex, int64_t endIndex) {
	auto dims = GetDimensions(header);
	return startIndex <= endIndex && startIndex >= 0 && endIndex < std::get<2>(dims);
}

} //namespace detail

template<typename TView>
std::ofstream& operator <<(std::ofstream& out, TView view) {
	out.write(reinterpret_cast<const char*>(view.pointer()), view.elementCount() * sizeof(typename TView::Element));
	return out;
}

FormatDescription::Header GetHeader(const std::string filename);

bool IsMrc(const FormatDescription::Header& header);
bool IsMrc(const std::string filename);

std::tuple<int, int, int> GetDimensions(const std::string filename);

template<typename TType, int tDimension>
void Slices(
	const std::string filename,
	HostImageView<TType, tDimension> view,
	typename HostImageView<TType, tDimension>::IndexType::Element startIndexZeroBased,
	typename HostImageView<TType, tDimension>::IndexType::Element endIndexZeroBased)
{
	std::ifstream mrcFile(filename, std::ios::binary);
	if (!mrcFile.good()) {
		MRC_THROW(InputFileError() << GetInputFileErrorInfo(filename));
	}
	FormatDescription::Header header;
	mrcFile.read(reinterpret_cast<char*>(&header), sizeof(FormatDescription::Header));
	if (!detail::IsValidSliceRange(header, startIndexZeroBased, endIndexZeroBased)) {
		MRC_THROW(SliceRangeError() << GetSliceRangeErrorInfo(startIndexZeroBased, endIndexZeroBased));
	}
	auto sliceSizeInBytes = SliceSizeInBytes(header);
	int64_t viewSizeInBytes = view.elementCount() * sizeof(typename HostImageView<TType, tDimension>::Element);
	int64_t bytesToRead = sliceSizeInBytes * (endIndexZeroBased - startIndexZeroBased + 1);

	if (viewSizeInBytes < bytesToRead) {
		std::cerr
			<< (boost::format("View size in bytes is smaller than slices size%1%. Reading only corresponding subset.") % filename).str()
			<< std::endl;
		if (viewSizeInBytes % sliceSizeInBytes != 0) {
			std::cerr
				<< (boost::format("View size in bytes is not a multiple of slice size (%1%). Reading nearest smaller amount of slices.")
					% sliceSizeInBytes).str() << std::endl;
			viewSizeInBytes = (viewSizeInBytes / sliceSizeInBytes) * sliceSizeInBytes;
		}
	} else if (viewSizeInBytes > bytesToRead) {
		std::cerr << (boost::format("Slices size in bytes %1% is smaller than view size. Reading in what is available.") % filename).str()
			<< std::endl;
	}

	mrcFile.seekg(sliceSizeInBytes * startIndexZeroBased, mrcFile.cur);
	auto sizeToRead = std::min(viewSizeInBytes, bytesToRead);
	mrcFile.read(reinterpret_cast<char*>(view.pointer()), sizeToRead);
}

template<typename TType, int tDimension>
void Data(const std::string filename, HostImageView<TType, tDimension> view) {
	auto dims = GetDimensions(filename);
	Slices(filename, view, 0, std::get<2>(dims) - 1);
}

template<typename TType, int tDimension>
void Write(const std::string filename, HostImageConstView<TType, tDimension> view, float apix) {
	auto dim3D = detail::GetDimension3D(view);
	auto minMaxMean = bolt::minMaxMean(view);
	FormatDescription::Header header = SimpleVolumeHeader(
		dim3D[0],
		dim3D[1],
		dim3D[2],
		GetPixelType<typename std::remove_const<TType>::type>(),
		static_cast<float>(minMaxMean.template get<0>()),
		static_cast<float>(minMaxMean.template get<1>()),
		static_cast<float>(minMaxMean.template get<2>()),
		apix);
	std::ofstream mrcFile(filename, std::ios_base::out | std::ios_base::binary);
	if (!mrcFile.good()) {
		MRC_THROW(FileNotFoundError() << GetFileNotFoundInfo(filename));
	}
	mrcFile << header << view;
	mrcFile.close();
}

} //namespace mrc
