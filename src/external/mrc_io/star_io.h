//
// (c) Lukas Marsalek, lukas.marsalek@eyen.eu, 2019
//

#pragma once

#include <fstream>
#include "boltview/exceptions.h"
#include <vector>
#include <string>

#define SFR_THROW(x) BOOST_THROW_EXCEPTION(x)

using namespace ctftools;

namespace libDip {

class StarFile {
public:
	static std::vector<std::string> ListDataBlocks(const std::string& filename) {

	}

	static std::vector<std::string> ListTagsForBlock(const std::string& filename, const std::string& blockName) {

	}
};

ImodMRCFormatDescription::Header GetMrcHeader(const std::string& filename) {
	std::ifstream mrcFile(filename, std::ios::binary);
	if (!mrcFile.good()) {
		EMR_THROW(InputFileError() << GetInputFileErrorInfo(filename));
	}
	ImodMRCFormatDescription::Header header;
	mrcFile.read(reinterpret_cast<char*>(&header), sizeof(ImodMRCFormatDescription::Header));
	return std::move(header);
}

bool isMrc(const ImodMRCFormatDescription::Header& header) {
	return ImodMRCFormatDescription::HasCmapId(header);
}

bool isMrc(const std::string& filename) {
	return isMrc(GetMrcHeader(filename));
}

std::tuple<int, int, int> MrcDimensions(const std::string& filename) {
	ImodMRCFormatDescription::Header header = GetMrcHeader(filename);
	if (!isMrc(header)) {
		EMR_THROW(
			UnsupportedFileFormat() << GetUnsuppportedFileFormatInfo((boost::format("File %1% is not in MRC format") % filename).str()));
	}
	return ImodMRCFormatDescription::GetDimensions(header);
}

std::vector<float> MrcData(const std::string& filename) {
	std::ifstream mrcFile(filename, std::ios::binary);
	if (!mrcFile.good()) {
		EMR_THROW(InputFileError() << GetInputFileErrorInfo(filename));
	}
	ImodMRCFormatDescription::Header header;
	mrcFile.read(reinterpret_cast<char*>(&header), sizeof(ImodMRCFormatDescription::Header));
	auto dims = ImodMRCFormatDescription::GetDimensions(header);
	if (ImodMRCFormatDescription::GetPixelType(header) != ImodMRCFormatDescription::PixelType::Float) {
		EMR_THROW(UnsupportedFileFormat() << GetUnsuppportedFileFormatInfo("Reader currently supports only float type MRC"));
	}
	std::vector<float> data(ImodMRCFormatDescription::DataSizeInBytes(header) / sizeof(float));
	mrcFile.read(reinterpret_cast<char*>(&data[0]), data.size() * sizeof(float));
	return std::move(data);
}

}
;
//namespace libDip
