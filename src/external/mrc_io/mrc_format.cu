//
// (c) Lukas Marsalek, l.marsalek@seznam.cz, 2015
//

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <stdexcept>
#include <tuple>
#include <boost/format.hpp>
#include <boost/mpl/for_each.hpp>
#include <boost/mpl/at.hpp>
#include <boost/mpl/vector_c.hpp>
#include <boost/mpl/integral_c.hpp>
#include <boost/bind.hpp>
#include <type_traits>

#include "file_error.h"
#include "mrc_format.h"

#define MRC_THROW(x) GENERIC_THROW(x)

namespace mrc {

float GetApix(const FormatDescription::Header& header) {
	const float apixX = header.xlen / header.mx;
	const float apixY = header.ylen / header.my;
	const float apixZ = header.zlen / header.mz;
	if (apixX != apixY || apixY != apixZ) {
		std::cerr << "Warning: The MRC file has non-uniform pixel size (Apix). Returning Apix in X dimension" << std::endl;
	}
	return apixX;
}

std::string Extension() {
	return "mrc";
}

FormatDescription::PixelType GetPixelType(const FormatDescription::Header& header) {
	if (ContainsSignedBytes(header)) {
		return FormatDescription::PixelType::SignedBytes;
	}
	;

	FormatDescription::PixelType result;
	switch (header.mode) {
		case static_cast<int>(FormatDescription::PixelType::Bytes):
			result = FormatDescription::PixelType::Bytes;
			break;
		case static_cast<int>(FormatDescription::PixelType::SignedShort):
			result = FormatDescription::PixelType::SignedShort;
			break;
		case static_cast<int>(FormatDescription::PixelType::Float):
			result = FormatDescription::PixelType::Float;
			break;
		case static_cast<int>(FormatDescription::PixelType::ComplexShort):
			result = FormatDescription::PixelType::ComplexShort;
			break;
		case static_cast<int>(FormatDescription::PixelType::ComplexFloat):
			result = FormatDescription::PixelType::ComplexFloat;
			break;
		case static_cast<int>(FormatDescription::PixelType::UnsignedShort):
			result = FormatDescription::PixelType::UnsignedShort;
			break;
		case static_cast<int>(FormatDescription::PixelType::RGB):
			result = FormatDescription::PixelType::RGB;
			break;
		case static_cast<int>(FormatDescription::PixelType::SignedBytes):
			result = FormatDescription::PixelType::SignedBytes;
			break;
		default:
			MRC_THROW(MrcModeError() << GetMrcModeErrorInfo(header.mode));
			break;
	}
	return result;
}

template<>
FormatDescription::PixelType GetPixelType<float>() {
	return FormatDescription::PixelType::Float;
}

template<>
FormatDescription::PixelType GetPixelType<char>() {
	return FormatDescription::PixelType::SignedBytes;
}

template<>
FormatDescription::PixelType GetPixelType<unsigned char>() {
	return FormatDescription::PixelType::Bytes;
}

template<>
FormatDescription::PixelType GetPixelType<short>() {
	return FormatDescription::PixelType::SignedShort;
}

template<>
FormatDescription::PixelType GetPixelType<unsigned short>() {
	return FormatDescription::PixelType::UnsignedShort;
}

template<>
FormatDescription::PixelType GetPixelType<int>() {
	return FormatDescription::PixelType::Float;
}

int GetPixelSizeInBytes(const FormatDescription::Header& header) {
	int result = 0;
	switch (GetPixelType(header)) {
		case FormatDescription::PixelType::Bytes:
			result = 1;
			break;
		case FormatDescription::PixelType::SignedShort:
			result = 2;
			break;
		case FormatDescription::PixelType::Float:
			result = 4;
			break;
		case FormatDescription::PixelType::ComplexShort:
			result = 4;
			break;
		case FormatDescription::PixelType::ComplexFloat:
			result = 8;
			break;
		case FormatDescription::PixelType::UnsignedShort:
			result = 2;
			break;
		case FormatDescription::PixelType::RGB:
			result = 3;
			break;
		case FormatDescription::PixelType::SignedBytes:
			result = 1;
			break;
		default:
			MRC_THROW(MrcModeError() << GetMrcModeErrorInfo(header.mode));
			break;
	}
	return result;
}

void PrintPixelType(const FormatDescription::Header& header, std::ostream& stream) {
	switch (GetPixelType(header)) {
		case FormatDescription::PixelType::Bytes:
			stream << "Unsigned char (mode " << static_cast<int>(FormatDescription::PixelType::Bytes) << ")";
			break;
		case FormatDescription::PixelType::SignedShort:
			stream << "Signed short (mode " << static_cast<int>(FormatDescription::PixelType::SignedShort) << ")";
			break;
		case FormatDescription::PixelType::Float:
			stream << "Float (mode " << static_cast<int>(FormatDescription::PixelType::Float) << ")";
			break;
		case FormatDescription::PixelType::ComplexShort:
			stream << "Complex short (mode " << static_cast<int>(FormatDescription::PixelType::ComplexShort) << ")";
			break;
		case FormatDescription::PixelType::ComplexFloat:
			stream << "Complex float (mode " << static_cast<int>(FormatDescription::PixelType::ComplexFloat) << ")";
			break;
		case FormatDescription::PixelType::UnsignedShort:
			stream << "Unsigned short (mode " << static_cast<int>(FormatDescription::PixelType::UnsignedShort) << ")";
			break;
		case FormatDescription::PixelType::RGB:
			stream << "RGB (mode " << static_cast<int>(FormatDescription::PixelType::RGB) << ")";
			break;
		case FormatDescription::PixelType::SignedBytes:
			stream << "Signed char (mode " << static_cast<int>(FormatDescription::PixelType::Bytes) << ")";
			break;
		default:
			MRC_THROW(MrcModeError() << GetMrcModeErrorInfo(static_cast<int>(GetPixelType(header))));
	}
}

void SetPixelType(FormatDescription::Header* header, FormatDescription::PixelType value) {
	if (value == FormatDescription::PixelType::SignedBytes) {
		header->mode = static_cast<int>(FormatDescription::PixelType::Bytes);
		SetBitFlag(&(header->imodFlags), FormatDescription::ImodBitFlags::SignedBytes);
	} else {
		header->mode = static_cast<int>(value);
	}
}

std::string GetPixelTypeAsString(const FormatDescription::Header& header) {
	std::stringstream ss;
	PrintPixelType(header, ss);
	return ss.str();
}

bool IsBitFlagSet(int field, int flag) {
	return (field & flag) != 0;
}

void SetBitFlag(int* field, int flag) {
	*field = (*field) | flag;
}

void UnsetBitFlag(int* field, int flag) {
	*field = (*field) & ~flag;
}

bool HasExtendedHeader(const FormatDescription::Header& header) {
	return header.next != 0;
}

bool CreatedByImod(const FormatDescription::Header& header) {
	return header.imodStamp == 1146047817;
}

bool ContainsSignedBytes(const FormatDescription::Header& header) {
	return (header.mode == static_cast<int>(FormatDescription::PixelType::Bytes)) && CreatedByImod(header)
		&& IsBitFlagSet(header.imodFlags, FormatDescription::ImodBitFlags::SignedBytes);
}

bool HasCmapId(const FormatDescription::Header& header) {
	return header.cmap[0] == 'M' && header.cmap[1] == 'A' && header.cmap[2] == 'P' && header.cmap[3] == ' ';
}

void SetCmapId(FormatDescription::Header* header) {
	header->cmap[0] = 'M';
	header->cmap[1] = 'A';
	header->cmap[2] = 'P';
	header->cmap[3] = ' ';
}

bool IsValid(const FormatDescription::Header& header) {
	return HasCmapId(header);
}

void SetMinMaxMean(FormatDescription::Header* header, const float minimum, const float maximum, const float mean) {
	header->amin = minimum;
	header->amax = maximum;
	header->amean = mean;
}

void SetGridProperties(FormatDescription::Header* header, const float gridSizeInX, const float gridSizeInY, const float gridSizeInZ) {
	header->xlen = gridSizeInX;
	header->ylen = gridSizeInY;
	header->zlen = gridSizeInZ;

	header->mapc = 1;
	header->mapr = 2;
	header->maps = 3;

	header->alpha = 90;
	header->beta = 90;
	header->gamma = 90;
}

void SetDimensions(FormatDescription::Header* header, const int width, const int height, const int depth, const float apix) {
	header->nx = header->mx = width;
	header->ny = header->my = height;
	header->nz = header->mz = depth;
	SetGridProperties(header, width * apix, height * apix, depth * apix);
}

std::tuple<int, int, int> GetDimensions(const FormatDescription::Header& header) {
	return std::make_tuple(header.nx, header.ny, header.nz);
}

void ClearHeader(FormatDescription::Header* header) {
	memset(header, '\0', sizeof(FormatDescription::Header));
}

int64_t DataSizeInBytes(const FormatDescription::Header& header) {
	auto dims = GetDimensions(header);
	return int64_t(std::get<0>(dims)) * int64_t(std::get<1>(dims)) * int64_t(std::get<2>(dims)) * int64_t(GetPixelSizeInBytes(header));
}

int64_t SliceSizeInBytes(const FormatDescription::Header& header) {
	auto dims = GetDimensions(header);
	return int64_t(std::get<0>(dims)) * int64_t(std::get<1>(dims)) * int64_t(GetPixelSizeInBytes(header));
}

FormatDescription::Header SimpleVolumeHeader(
	const int width,
	const int height,
	const int depth,
	FormatDescription::PixelType pType,
	const float amin,
	const float amax,
	const float amean,
	const float apix)
{
	FormatDescription::Header header;
	ClearHeader(&header);

	SetCmapId(&header);
	SetPixelType(&header, pType);
	SetMinMaxMean(&header, amin, amax, amean);
	SetDimensions(&header, width, height, depth, apix);
	return std::move(header);
}

std::ostream& operator <<(std::ostream& out, const FormatDescription::Header& header) {
	out << "=== MRC header content ==";
	out << std::endl << "Created by: ";
	out << std::string(CreatedByImod(header) ? "Imod or compatible" : "Unspecified");
	out << std::endl << "Pixel type: ";
	PrintPixelType(header, out);
	out << std::endl << "Dimensions: " << header.nx << " x " << header.ny << " x " << header.nz;
	out << std::endl << "Values: ";
	out << "min=" << header.amin << ", max=" << header.amax << ", mean=" << header.amean << ", rms=" << header.rms;
	out << std::endl << "Extended header present: ";
	out << std::string(HasExtendedHeader(header) ? "Yes" : "No");
	out << std::endl << "=== ------------------ ==" << std::endl;
	return out;
}

std::ofstream& operator <<(std::ofstream& out, const FormatDescription::Header& header) {
	out.write(reinterpret_cast<const char*>(&header), sizeof(FormatDescription::Header));
	return out;
}

} //namespace mrc

