//
// (c) Lukas Marsalek, lukas.marsalek@eyen.eu, 2019
//

#pragma once

#include "star_io.h"

namespace dipLab {
namespace starFormat {
namespace parser {

BOOST_SPIRIT_INSTANTIATE(rexpr_type, iterator_type, context_type);

};//namespace parser
};//namespace starFormar
};//namespace dipLab
