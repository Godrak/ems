#pragma once

#include <tuple>

#include <boost/format.hpp>
#include <boltview/math/quaternion.h>
#include <boltview/device_image.h>
#include <boltview/host_image.h>
#include "../external/ctfParameters.h"
#include "mrc_io.h"

namespace ctftools {

class Projection {
public:
	typedef bolt::Quaternion<float> OrientationType;
	typedef bolt::Int2 SizeType;

	Projection() : size_(-1, -1), psfParams_(std::make_tuple(CtfParameters(), GaussParameters())), orientation_(0.f, 0.f, 0.f, 0.f) {
	}

	template<typename TPsfParams>
	Projection(SizeType size, TPsfParams psfParams, OrientationType orientation) :
		size_(size),
		psfParams_(std::make_tuple(CtfParameters(), GaussParameters())),
		orientation_(orientation),
		shift_(0, 0, 0)
	{
		std::get<TPsfParams>(psfParams_) = psfParams;
	}

	template<typename TPsfParams>
	Projection(
		SizeType size,
		TPsfParams psfParams,
		OrientationType orientation,
		bolt::Float3 shift,
		int projectionNumber,
		float loglikelihood = 1.0) :
		size_(size),
		psfParams_(std::make_tuple(CtfParameters(), GaussParameters())),
		orientation_(orientation),
		shift_(shift),
		projectionNumber_(projectionNumber),
		loglikelihood_(loglikelihood)
	{
		std::get<TPsfParams>(psfParams_) = psfParams;
	}

	Projection(const Projection& other) :
		size_(other.size_),
		orientation_(other.orientation_),
		shift_(other.shift_),
		projectionNumber_(other.projectionNumber_),
		loglikelihood_(other.loglikelihood_)
	{
		std::get<0>(psfParams_) = std::get<0>(other.psfParams_);
		std::get<1>(psfParams_) = std::get<1>(other.psfParams_);
	}

	Projection(Projection&& other) {
		std::swap(size_, other.size_);
		std::swap(psfParams_, other.psfParams_);
		std::swap(orientation_, other.orientation_);
		std::swap(shift_, other.shift_);
		std::swap(projectionNumber_, other.projectionNumber_);
		std::swap(loglikelihood_, other.loglikelihood_);
	}

	Projection& operator=(const Projection& other) {
		size_ = other.size_;
		std::get<0>(psfParams_) = std::get<0>(other.psfParams_);
		std::get<1>(psfParams_) = std::get<1>(other.psfParams_);
		orientation_ = other.orientation_;
		shift_ = other.shift_;
		projectionNumber_ = other.projectionNumber_;
		loglikelihood_ = other.loglikelihood_;
		return *this;
	}

	Projection& operator=(Projection&& other) {
		std::swap(size_, other.size_);
		std::swap(psfParams_, other.psfParams_);
		std::swap(orientation_, other.orientation_);
		std::swap(shift_, other.shift_);
		std::swap(projectionNumber_, other.projectionNumber_);
		std::swap(loglikelihood_, other.loglikelihood_);
		return *this;
	}

	template<typename TPsfParams>
	TPsfParams PsfParams() {
		return std::get<TPsfParams>(psfParams_);
	}

	OrientationType Orientation() {
		return orientation_;
	}

	SizeType Size() {
		return size_;
	}

	bolt::Float3 Shift() {
		return shift_;
	}

	float Loglikelihood() {
		return loglikelihood_;
	}

	int ProjectioNumber() const {
		return projectionNumber_;
	}

	std::string AsText() {
		return (boost::format("[%5%, %6%][%1%, %2%, %3%, %4%]") % Orientation()[0] % Orientation()[1] % Orientation()[2] % Orientation()[3]
			% Size()[0] % Size()[1]).str();
	}

	template<typename Element>
	void ReadFromFile(std::string& filename, bolt::HostImageView<Element, 2> view) {
		mrc::Slices(filename, view, ProjectioNumber(), ProjectioNumber());
	}

private:
	SizeType size_;
	std::tuple<CtfParameters, GaussParameters> psfParams_;
	OrientationType orientation_;
	bolt::Float3 shift_;
	int projectionNumber_;
	float loglikelihood_;
};
} //namespace ctftools
