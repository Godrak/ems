#pragma once

#include <random>
#include <vector>

namespace ctftools {

struct CtfParameters {
	int width;
	int height;
	float defocus;
	float defocusU;
	float defocusV;
	float defocusAngle;
	float angpix;
	float spher_ab;
	float lambda;
	float amp_cont;
	float bfactor;
};

/*
 1000, // defocus
 2e7, // spherab
 2.75e-2, // lambda 200kV
 0.1, // amp_cont
 0, // bfactor
 };
 */

class TemCtfParamsSequenceGenerator {
public:
	typedef std::vector<CtfParameters> ContainerType;

	TemCtfParamsSequenceGenerator(CtfParameters masterParams, float varRate) : masterParams_(masterParams), varRate_(varRate) {
	}

	ContainerType Generate(int amount) {
		ContainerType result;

		std::random_device randomDevice { };
		std::mt19937 randomGenerator { randomDevice() };
		std::normal_distribution<> normalDistribution { masterParams_.defocus, masterParams_.defocus * varRate_ };

		for (int i = 0; i < amount; ++i) {
			CtfParameters params(masterParams_);
			params.defocus = normalDistribution(randomGenerator);
			result.push_back(params);
		}
		return result;
	}

private:
	float varRate_;
	CtfParameters masterParams_;
};

struct GaussParameters {
	int width;
	int height;
	float normalizationFactor;
	float standardDeviation;
	float numStdDevAtEdge;
};

class GaussParamsSequenceGenerator {
public:
	typedef std::vector<GaussParameters> ContainerType;

	GaussParamsSequenceGenerator(GaussParameters masterParams, float variationRate) : varRate_(variationRate), masterParams_(masterParams) {
	}

	ContainerType Generate(int amount) {
		ContainerType result;

		std::random_device randomDevice { };
		std::mt19937 randomGenerator { randomDevice() };
		std::normal_distribution<> normalDistribution { masterParams_.numStdDevAtEdge, masterParams_.numStdDevAtEdge * varRate_ / 3 };

		for (int i = 0; i < amount; ++i) {
			GaussParameters params(masterParams_);
			params.numStdDevAtEdge = normalDistribution(randomGenerator);
			result.push_back(params);
		}
		return result;
	}

private:
	float varRate_;
	GaussParameters masterParams_;
};

} //namespace ctftools
