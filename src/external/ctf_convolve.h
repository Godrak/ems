/*
 * ctf_convolve.h
 *
 *  Created on: Feb 18, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EXTERNAL_CTF_CONVOLVE_H_
#define SRC_EXTERNAL_CTF_CONVOLVE_H_

#include "projection.h"

#include "boltview/math/vector.h"
#include "boltview/host_image.h"
#include "boltview/copy.h"
#include "boltview/for_each.h"
#include "boltview/image_io.h"

#include "boltview/device_image.h"
#include "boltview/fft/fft_calculator.h"
#include "ctfgen.cuh"

template<typename T>
struct PsfFunctor {
	ctftools::CtfGen<float> gen;
	PsfFunctor(ctftools::CtfParameters params) : gen(params) {
	}
	BOLT_DECL_HYBRID
	auto operator()(bolt::Int2 index) const
	{
		return gen.shifted(index[0], index[1]);
	}
};

template<typename TView>
struct CtfConvolver {
	using ImageType = typename std::conditional_t<bolt::IsDeviceImageView<TView>::value, bolt::DeviceImage<bolt::DeviceComplexType, 2>, bolt::HostImage<bolt::HostComplexType, 2>>;
	template<typename TDirection>
	using FftPolicy = typename std::conditional_t<bolt::IsDeviceImageView<TView>::value, bolt::DeviceFftPolicy<TDirection>, bolt::HostFftPolicy<TDirection>>;

	ImageType fourier_image;
	bolt::Int2 fft_image_size;

	CtfConvolver(bolt::Int2 projection_size) :
		fourier_image(bolt::getFftImageSize(projection_size)),
		fft_image_size(bolt::getFftImageSize(projection_size))
	{
	}

	void ctfConvolve2D(TView projection, ctftools::Projection& projection_info) {
		//forward fft
		auto fourier_view = fourier_image.view();
		bolt::FftCalculator<2, FftPolicy<bolt::Forward>> forward_fft_calc { projection.size() };
		forward_fft_calc.calculate(projection, fourier_view);

		//create psf in f space
		bolt::IndexView<2> psf_index_image { fft_image_size };
		bolt::UnaryOperatorImageView<bolt::IndexView<2>, PsfFunctor<float>> psf { psf_index_image, PsfFunctor<float> {
			projection_info.PsfParams<ctftools::CtfParameters>() } };

		//convolution
		auto convolved = bolt::multiply(fourier_view, psf);

		bolt::copy(convolved, fourier_view);
		bolt::FftCalculator<2, FftPolicy<bolt::Inverse>> inverse_fft_calc { projection.size() };
		inverse_fft_calc.calculateAndNormalize(fourier_view, projection);

	}

};

#endif /* SRC_EXTERNAL_CTF_CONVOLVE_H_ */
