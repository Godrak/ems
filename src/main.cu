/*
 * test_main.cpp
 *
 *  Created on: Nov 26, 2019
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#include "external/StarFileReader.h"
#include "external/projection.h"
#include "external/ctf_convolve.h"

#include "helper_func.h"
#include "ems/ems.h"


int main(int argc, char** argv) {

	auto params = loadParams(argc, argv);

	auto ems = ems::Ems<true> { params };

	if (params.get<particlesRange>()[1] == 0) {
		return 0;
	}

	std::string star_file = params.get<projectionsStarFile>();

	//Projections
	bolt::Int2 image_size = bolt::Int2(u(params.get<detectorPixelSize>()), u(params.get<detectorPixelSize>()));

	std::vector<ctftools::Projection> projections { };
	ctftools::StarFileReader<float>::Read(star_file, projections, image_size);

	ems::Ems<true>::ImgType<float, 2> rendered_image { image_size };
	auto rendered_view = rendered_image.view();

	size_t start = params.get<particlesRange>()[0];
	size_t end = std::min(size_t(params.get<particlesRange>()[1]), projections.size());

	CtfConvolver<decltype(rendered_view)> convolver { image_size };

	for (size_t particle_index = start; particle_index < end; ++particle_index) {
		for (size_t sample = 0; sample < params.get<samples>(); ++sample) {
			auto ctf_params = projections[particle_index].PsfParams<ctftools::CtfParameters>();
			auto detector_distance = ems::Bohr { 0 };
			if (params.get<ems::ems_ApplyDefocus>()) {
				detector_distance = max(ems::Bohr { ems::angToBohr(abs(ctf_params.defocus)) }, ems::Bohr { 0 });
			}

			ems.render(rendered_view, projections[particle_index].Orientation(), { }, detector_distance);
			bolt::dump(rendered_view, ems.modelName() + std::to_string(particle_index) + "_" + std::to_string(sample));
			convolver.ctfConvolve2D(rendered_view, projections[particle_index]);
			bolt::dump(rendered_view, ems.modelName() + "_ctf" + std::to_string(particle_index) + "_" + std::to_string(sample));

			std::cout << "direction: " << particle_index << "  sample: " << sample << " done " << std::endl;
		}
	}

	return 0;
}
