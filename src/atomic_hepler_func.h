/*
 * atomic_hepler_func.h
 *
 *  Created on: May 13, 2021
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_ATOMIC_HEPLER_FUNC_H_
#define SRC_ATOMIC_HEPLER_FUNC_H_

#include "gemmi/model.hpp"
#include "gemmi/mmread.hpp"
#include "gemmi/to_pdb.hpp"
#include <string>
#include <vector>
#include "ems/atomic_ems_components.h"
#include "ems/ems.h"
#include "src/scattering-factors/sf_params.h"
#include "src/scattering-factors/sfp_loader.hpp"

ScatFacParamsList LOADED_PARAMS_LIST = read_sfp_list_from_const_char(PARAMS);

std::vector<ems::EmsAtom> prepareAtomsVector(
	const std::string& filepath,
	float ang_per_voxel,
	size_t& volume_size,
	unsigned char& max_at_number)
{
	gemmi::Structure structure = gemmi::read_structure_file(filepath);

	auto init_atom = structure.models[0].chains[0].residues[0].atoms[0];
	double min_x = init_atom.pos.x;
	double max_x = init_atom.pos.x;
	double min_y = init_atom.pos.y;
	double max_y = init_atom.pos.y;
	double min_z = init_atom.pos.z;
	double max_z = init_atom.pos.z;
	max_at_number = init_atom.element.atomic_number();

	for (const auto& model : structure.models) {
		for (const auto& chain : model.chains) {
			for (const auto& residue : chain.residues) {
				for (const auto& atom : residue.atoms) {
					min_x = std::min(min_x, atom.pos.x);
					max_x = std::max(max_x, atom.pos.x);
					min_y = std::min(min_y, atom.pos.y);
					max_y = std::max(max_y, atom.pos.y);
					min_z = std::min(min_z, atom.pos.z);
					max_z = std::max(max_z, atom.pos.z);
					max_at_number = max(max_at_number, atom.element.atomic_number());
				}
			}
		}
	}

	auto max_c = std::max(std::max(max_x, max_y), max_z);
	auto min_c = std::min(std::min(min_x, min_y), min_z);

	auto MAX_RADIUS = 5;
	auto min_x_margins = min_c - MAX_RADIUS;
	auto min_y_margins = min_c - MAX_RADIUS;
	auto min_z_margins = min_c - MAX_RADIUS;
	auto max_x_margins = max_c + MAX_RADIUS;
	auto max_y_margins = max_c + MAX_RADIUS;
	auto max_z_margins = max_c + MAX_RADIUS;

	double diff_x = max_x_margins - min_x_margins;
	double diff_y = max_y_margins - min_y_margins;
	double diff_z = max_z_margins - min_z_margins;

	auto volume_size_x = diff_x / ang_per_voxel;
	auto volume_size_y = diff_x / ang_per_voxel;
	auto volume_size_z = diff_x / ang_per_voxel;

	volume_size = size_t(std::max(std::max(volume_size_x, volume_size_y), volume_size_z) + 1);

	std::vector<ems::EmsAtom> result { };

	//CENTERING
	auto center_x = (max_x_margins + min_x_margins) / 2.0;
	auto center_y = (max_y_margins + min_y_margins) / 2.0;
	auto center_z = (max_z_margins + min_z_margins) / 2.0;

	for (const auto& model : structure.models) {
		for (const auto& chain : model.chains) {
			for (const auto& residue : chain.residues) {
				for (const auto& atom : residue.atoms) {
					ems::EmsAtom res_atom { };
					res_atom.atomic_number = atom.element.atomic_number();
					res_atom.position = t<ems::Bohr3>(
					ANG_TO_BOHR * (atom.pos.x - center_x),
					ANG_TO_BOHR * (atom.pos.y - center_y),
					ANG_TO_BOHR * (atom.pos.z - center_z));
					result.push_back(res_atom);
				}
			}
		}
	}

	return result;
}

template<typename THostAtomsView>
void atomsViewToFile(const std::string& filepath, THostAtomsView atoms_view) {
	gemmi::Residue residue { };
	for (size_t index = 1; index < atoms_view.size(); ++index) {
		gemmi::Atom atom { };
		atom.element = gemmi::Element { atoms_view[index].atomic_number };
		gemmi::Position pos { };
		pos.x = atoms_view[index].position[0];
		pos.y = atoms_view[index].position[1];
		pos.z = atoms_view[index].position[2];
		atom.pos = pos;
		residue.atoms.push_back(atom);
	}

	gemmi::Chain chain { "chain" };
	chain.residues.push_back(residue);

	gemmi::Model model { "model" };
	model.chains.push_back(chain);

	gemmi::Structure structure { "structure" };
	structure.models.push_back(model);

	std::ofstream output(filepath, std::ios_base::out);
	gemmi::write_minimal_pdb(structure, output);
	output.close();

}

template<typename THostView>
void fillPotentialValuesView(THostView values_view, ems::Bohr max_distance) {
	using namespace ems;
	auto dis_per_voxel = max_distance / ems::Num { values_view.size()[1] };
	for (size_t dis = 0; dis < values_view.size()[1]; ++dis) {
		values_view[bolt::Int2 { 0, dis }] = 0;
	}
	for (size_t atom = 0; atom < values_view.size()[0]; ++atom) {
		for (size_t dis = 0; dis < values_view.size()[1]; ++dis) {
			values_view[bolt::Int2 { atom + 1, dis }] = potential((dis + 1) * u(dis_per_voxel), LOADED_PARAMS_LIST[atom]);
		}
	}
}

template<typename TProjectionView>
class ShiftingFunctor {
public:
	ShiftingFunctor(
		bolt::Int2 shifting_view_size,
		TProjectionView projection_view,
		ems::Bohr location_size,
		size_t samples_count,
		ems::ElectronMicroscopeSetup setup) :
		shifting_view_size(shifting_view_size),
		projection_view(projection_view),
		location_size(location_size),
		samples_count(samples_count),
		setup(setup)
	{
		bohr_per_pixel = bolt::norm(setup.default_emitter_x_vector) / shifting_view_size[0];
		proj_bohr_per_pixel = bolt::norm(setup.default_detector_x_vector) / projection_view.size()[0];
	}

	void operator()(ems::Num3& val, const bolt::Int2& index) const {
		for (size_t sample_one = 0; sample_one < samples_count; ++sample_one) {
			for (size_t sample_two = 0; sample_two < samples_count; ++sample_two) {
				auto dir = ems::sampleDiscUniform(sample_one / float(samples_count), sample_two / float(samples_count));
				auto pos = t<ems::Num2>(index) * bohr_per_pixel + dir * location_size;
				auto pixel_val = projection_view.access(bolt::div(u(pos), u(proj_bohr_per_pixel)));
				dir = bolt::normalize(dir);
				val += dir[0] * bolt::normalize(u(setup.default_emitter_x_vector)) * pixel_val
					+ dir[1] * bolt::normalize(u(setup.default_emitter_y_vector)) * pixel_val;
			}
		}
	}

	bolt::Int2 shifting_view_size;
	TProjectionView projection_view;
	ems::Bohr location_size;
	size_t samples_count;
	ems::ElectronMicroscopeSetup setup;

	ems::Bohr bohr_per_pixel;
	ems::Bohr proj_bohr_per_pixel;

};

template<typename TShiftingView, typename TProjectionView>
void computeShiftingView(
	TShiftingView shifting_view,
	TProjectionView projection_view,
	ems::ElectronMicroscopeSetup setup,
	ems::Bohr blur_size)
{
	using namespace ems;
	bolt::fill(shifting_view, Num3 { });
	auto interpolated_proj = utils::makeInterpolated(projection_view);
	auto functor = ShiftingFunctor<decltype(interpolated_proj)>(
		shifting_view.size(),
		interpolated_proj,
		blur_size,
		max(size_t(2), t<size_t>(blur_size)),
		setup);
	bolt::forEachPosition(shifting_view, functor);
}

#endif /* SRC_ATOMIC_HEPLER_FUNC_H_ */
