/*
 * visualizer.cpp
 *
 *  Created on: Feb 11, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#include "visualizer.h"

#include "boltview/math/vector.h"
#include "boltview/math/quaternion.h"
#include "boltview/cuda_defines.h"

#include "epoxy/gl.h"
#include <GLFW/glfw3.h>

#include <string>
#include <iostream>

namespace ems {

namespace impl {

std::string vertex_shader_code = ""
	"#version 450                                                       \n"
	"                                                                   \n"
	"void main()                                                        \n"
	"{                                                                  \n"
	"    float x = float(((uint(gl_VertexID) + 2u) / 3u)%2u);           \n"
	"    float y = float(((uint(gl_VertexID) + 1u) / 3u)%2u);           \n"
	"                                                                   \n"
	"    gl_Position = vec4(-1.0f + x*2.0f, -1.0f+y*2.0f, 0.0f, 1.0f);  \n"
	"};                                                                 \n";

std::string fragment_shader_code = ""
	"#version 450                                                       \n"
	"                                                                   \n"
	"layout (std430, binding=0) buffer image_buffer                     \n"
	"{                                                                  \n"
	"	float image_data[];                                             \n"
	"};                                                                 \n"
	"                                                                   \n"
	"layout(location = 0) uniform ivec2 screenResolution;               \n"
	"layout(location = 1) uniform ivec2 imageSize;                      \n"
	"                                                                   \n"
	"in vec4 gl_FragCoord;                                              \n"
	"out vec4 fragColor;                                                \n"
	"                                                                   \n"
	"void main(){                                                       \n"
	"	vec2 uv = gl_FragCoord.xy/screenResolution;                     \n"
	"	ivec2 coords = ivec2(floor(uv*imageSize));                      \n"
	"	float value = image_data[coords.x*imageSize.y + coords.y];      \n"
	"	value = clamp(value,0,1);                                       \n"
	"	fragColor = vec4(value,value,value,1.0);                        \n"
	"}                                                                  \n";

static bool checkOpengl() {
	GLenum err = glGetError();
	if (err == GL_NO_ERROR) {
		return true;
	}
	std::cout << "OpenGL message: " << (int) err << std::endl;
	return false;
}

static void checkGl() {
	if (!checkOpengl()) {
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
}

namespace globals {

bolt::Int2 screen_resolution = { 900, 900 };
bool looping = true;
//bolt::Quaternion<float> orientation { 1, 0, 0, 0 };
//bolt::Quaternion<float> orientation { 0.911477, 0.065751, -0.160718, -0.372895 };
bolt::Quaternion<float> orientation { -0.242652, 0.255120, 0.657212, 0.666278 };
float step_size = 2 * M_PI / (60 * 12);

bool getFlag(char flag) {
	int i = int(flag) - int('0');
	return i % 2 == 1;
}

void changeDirection(char c) {
	switch (c) {
		case 'd':
			orientation = orientation * rotationQuaternion(step_size, bolt::Float3 { 1, 0, 0 });
			break;
		case 'a':
			orientation = orientation * rotationQuaternion(-step_size, bolt::Float3 { 1, 0, 0 });
			break;
		case 's':
			orientation = orientation * rotationQuaternion(step_size, bolt::Float3 { 0, 1, 0 });
			break;
		case 'w':
			orientation = orientation * rotationQuaternion(-step_size, bolt::Float3 { 0, 1, 0 });
			break;
		case 'q':
			orientation = orientation * rotationQuaternion(step_size, bolt::Float3 { 0, 0, 1 });
			break;
		case 'e':
			orientation = orientation * rotationQuaternion(-step_size, bolt::Float3 { 0, 0, 1 });
			break;
		default:
			break;
	}
}
}

namespace glfw_context {

double last_xpos, last_ypos;
GLFWwindow* window;
int vsync = 1;

bool left_mouse_button_down = false;
char forth_back = ' ';
char left_right = ' ';
char up_down = ' ';

char flag1 = ' ';
char flag2 = ' ';
char flag3 = ' ';

static void errorCallback(int /*error*/, const char* description) {
	std::cout << "Error: " << description << std::endl;
}

static void keyCallback(GLFWwindow* window, int key, int /*scancode*/, int action, int /*mods*/) {
	if (action == GLFW_PRESS) {
		switch (key) {
			case GLFW_KEY_ESCAPE:
				glfwSetWindowShouldClose(window, GLFW_TRUE);
				break;
			case GLFW_KEY_SPACE:
				globals::looping = false;
				break;
			case GLFW_KEY_F3:
				vsync = 1 - vsync;
				break;
			case GLFW_KEY_W:
				forth_back = ('w');
				break;
			case GLFW_KEY_S:
				forth_back = ('s');
				break;
			case GLFW_KEY_A:
				left_right = ('a');
				break;
			case GLFW_KEY_D:
				left_right = ('d');
				break;
			case GLFW_KEY_Q:
				up_down = ('q');
				break;
			case GLFW_KEY_E:
				up_down = ('e');
				break;
			case GLFW_KEY_1:
				flag1 = ('1');
				break;
			case GLFW_KEY_2:
				flag1 = ('2');
				break;
			case GLFW_KEY_3:
				flag2 = ('3');
				break;
			case GLFW_KEY_4:
				flag2 = ('4');
				break;
			case GLFW_KEY_5:
				flag3 = ('5');
				break;
			case GLFW_KEY_6:
				flag3 = ('6');
				break;
		}
	}

	if (action == GLFW_RELEASE) {
		switch (key) {
			case GLFW_KEY_W:
			case GLFW_KEY_S:
				forth_back = ' ';
				break;
			case GLFW_KEY_A:
			case GLFW_KEY_D:
				left_right = ' ';
				break;
			case GLFW_KEY_Q:
			case GLFW_KEY_E:
				up_down = ' ';
				break;
		}
	}
}

static void initGlfw() {
	glfwSetErrorCallback(errorCallback);

	if (!glfwInit()) {
		exit(EXIT_FAILURE);
	}

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	window = glfwCreateWindow(globals::screen_resolution[0], globals::screen_resolution[1], "Visualizer", nullptr, nullptr);
	if (!window) {
		exit(-1);
	}

	glfwSetKeyCallback(window, keyCallback);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

	glfwMakeContextCurrent(window);
	glfwSwapInterval(vsync);

}

}  // namespace glfw_context

namespace shader_program {
GLuint v_shader, f_shader, program;

bool checkShader(std::string source, GLuint id, GLenum st) {
	GLint log_length;
	glGetShaderiv(id, GL_INFO_LOG_LENGTH, &log_length);
	if (log_length > 2) {
		std::vector<GLchar> log(log_length);
		glGetShaderInfoLog(id, log_length, &log_length, log.data());
		log[log_length - 1] = (GLchar) 0;
		std::cout << source << std::endl;
		std::cout << "Shader message: " << log.data() << std::endl;
	}

	GLint status;
	glGetShaderiv(id, st, &status);
	return (status == GL_TRUE);
}

bool checkProgram(GLuint id, GLenum st) {
	GLint log_length;
	glGetProgramiv(id, GL_INFO_LOG_LENGTH, &log_length);
	if (log_length > 2) {
		std::vector<GLchar> log(log_length);
		glGetProgramInfoLog(id, log_length, &log_length, log.data());
		log[log_length - 1] = (GLchar) 0;
		std::cout << "Program message: " << log.data() << std::endl;
	}

	GLint status;
	glGetProgramiv(id, st, &status);
	return (status == GL_TRUE);
}

void loadAndCompileShader(std::string source, GLuint& destination, GLenum type) {
	std::ifstream shader_source(source);
	std::string shader_code;
	getline(shader_source, shader_code, (char) shader_source.eof());

	const char* shader_code_c_string = shader_code.data();

	destination = glCreateShader(type);
	glShaderSource(destination, 1, &shader_code_c_string, nullptr);
	glCompileShader(destination);
	if (!checkShader(source, destination, GL_COMPILE_STATUS)) {
		exit(-1);
	}
}

void readAndCompileShader(std::string shader_code, GLuint& destination, GLenum type) {
	const char* shader_code_c_string = shader_code.data();

	destination = glCreateShader(type);
	glShaderSource(destination, 1, &shader_code_c_string, nullptr);
	glCompileShader(destination);
	if (!checkShader("EMBEDDED", destination, GL_COMPILE_STATUS)) {
		exit(-1);
	}
}

void createProgram() {
	readAndCompileShader(vertex_shader_code, v_shader, GL_VERTEX_SHADER);
	readAndCompileShader(fragment_shader_code, f_shader, GL_FRAGMENT_SHADER);

	program = glCreateProgram();
	glAttachShader(program, v_shader);
	glAttachShader(program, f_shader);
	glLinkProgram(program);
	if (!checkProgram(program, GL_LINK_STATUS)) {
		exit(-1);
	}
}

}

namespace data {
GLuint vao, image_buffer;
bolt::Int2 image_size;

void init(bolt::Int2 tex_size) {
	image_size = tex_size;
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);

	glGenBuffers(1, &image_buffer);
	glBindBuffer(GL_SHADER_STORAGE_BUFFER, image_buffer);
	glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 0, image_buffer);

	checkGl();

	float init_data[tex_size[0] * tex_size[1]];
	for (size_t index = 0; index < tex_size[0] * tex_size[1]; ++index) {
		init_data[index] = rand() / float(RAND_MAX);
	}

	glBufferData(GL_SHADER_STORAGE_BUFFER, tex_size[0] * tex_size[1] * sizeof(float), init_data, GL_STATIC_DRAW);

	checkGl();
}

}

namespace rendering {

void render() {
	globals::changeDirection(glfw_context::forth_back);
	globals::changeDirection(glfw_context::up_down);
	globals::changeDirection(glfw_context::left_right);

	glfwGetFramebufferSize(glfw_context::window, &globals::screen_resolution[0], &globals::screen_resolution[1]);
	glViewport(0, 0, globals::screen_resolution[0], globals::screen_resolution[1]);

	glClear(GL_COLOR_BUFFER_BIT);
	glUniform2iv(0, 1, globals::screen_resolution.pointer());
	glUniform2iv(1, 1, data::image_size.pointer());

	glDrawArrays(GL_TRIANGLES, 0, 6);

	glfwSwapBuffers(glfw_context::window);
	glfwPollEvents();
}

void setup(bolt::Int2 tex_size) {
	shader_program::createProgram();
	data::init(tex_size);
	glBindVertexArray(data::vao);
	glUseProgram(shader_program::program);
	glClearColor(0.0, 0.0, 0.0, 1.0);
	checkGl();
}

}

}

Visualizer::Visualizer(bolt::Int2 image_size) : image_size(image_size) {
	impl::glfw_context::initGlfw();
	impl::rendering::setup(image_size);
	impl::globals::looping = true;

	BOLT_CHECK(cudaGraphicsGLRegisterBuffer(&image_resource, impl::data::image_buffer, cudaGraphicsRegisterFlagsWriteDiscard));

}

bool Visualizer::render() {
	impl::globals::looping = true;
	impl::checkGl();
	impl::rendering::render();
	orientation = impl::globals::orientation;

	flag_one = impl::globals::getFlag(impl::glfw_context::flag1);
	flag_two = impl::globals::getFlag(impl::glfw_context::flag2);
	flag_three = impl::globals::getFlag(impl::glfw_context::flag3);

	return impl::globals::looping;
}

Visualizer::~Visualizer() {
	glfwDestroyWindow(impl::glfw_context::window);
	glfwTerminate();
	cudaGraphicsUnmapResources(1, &image_resource);
}
}
