/*
 * ncc_main.cu
 *
 *  Created on: Oct 22, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#include "boltview/host_image.h"
#include "boltview/texture_image.h"
#include "boltview/copy.h"
#include "boltview/for_each.h"
#include "boltview/image_io.h"
#include "boltview/device_image.h"
#include "boltview/interpolation.h"
#include "boltview/view_iterators.h"
#include "boltview/procedural_views.h"
#include "mrc_io.h"

void loadRaw(bolt::HostImageView<float, 3> view, std::string filename) {
	std::ifstream in;
	in.exceptions(std::ifstream::failbit | std::ifstream::badbit);
	in.open(filename, std::ifstream::in | std::ifstream::binary);

	auto num_elements = bolt::product(view.size());
	for (int64_t i = 0; i < num_elements; ++i) {
		auto& element = bolt::linearAccess(view, i);
		in.read(reinterpret_cast<char*>(&element), sizeof(element));
	}
}

int main(int argc, char** argv) {
	if (argc < 4) {
		std::cout << "computes normalized cross sections of two data files of same size in either mrc or bolt-compatible raw format"
			<< std::endl;
		std::cout << "uasge: ncc file1 file2 xsize [ysize] [zsize]" << std::endl;
		exit(0);
	}

	auto filepath1 = argv[1];
	auto filepath2 = argv[2];
	auto xsize = std::stoul(argv[3]);
	unsigned long ysize = 1;
	unsigned long zsize = 1;
	if (argc >= 5) {
		ysize = std::stoul(argv[4]);
	}
	if (argc >= 6) {
		zsize = std::stoul(argv[5]);
	}

	bolt::HostImage<float, 3> data1 { xsize, ysize, zsize };
	bolt::HostImage<float, 3> data2 { xsize, ysize, zsize };

	double voxel_size = 0;
	if (mrc::IsMrc(filepath1)) {
		voxel_size = mrc::GetApix(mrc::GetHeader(filepath1));
		mrc::Data(filepath1, data1.view());
	} else {
		loadRaw(data1.view(), filepath1);
	}

	if (mrc::IsMrc(filepath2)) {
		voxel_size = mrc::GetApix(mrc::GetHeader(filepath2));
		mrc::Data(filepath2, data2.view());
	} else {
		loadRaw(data2.view(), filepath2);
	}

	double ncc = 0;
	auto first_mean = bolt::minMaxMean(data1.view()).template get<2>();
	auto second_mean = bolt::minMaxMean(data2.view()).template get<2>();

	auto f = bolt::addValue(-first_mean, data1.view());
	auto s = bolt::addValue(-second_mean, data2.view());

	auto numerator = bolt::sum(bolt::multiply(f, s), double(0));
	auto denominator_f = bolt::sum(bolt::square(f), double(0));
	auto denominator_s = bolt::sum(bolt::square(s), double(0));

	auto x = denominator_f * denominator_s;
	if (x == 0) {
		ncc = 0;
	} else {
		ncc = numerator / std::sqrt(x);
	}

	std::cout << "Normalized cross correlaction is: " << ncc << std::endl;

	if (voxel_size > 0) {
		bolt::copy(bolt::multiply(f, s), data1.view());
		mrc::Write("nccmap_" + std::to_string(ncc) + ".mrc", data1.view(), voxel_size);
	} else {
		bolt::dump(bolt::multiply(f, s), "nccmap_" + std::to_string(ncc) + "_");
	}
}

