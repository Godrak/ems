/*
 * optimizing_main.cpp
 *
 *  Created on: Nov 26, 2019
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#include "../external/StarFileReader.h"
#include "../external/projection.h"
#include "../external/ctf_convolve.h"

#include "../helper_func.h"
#include "../ems/ems.h"

int main(int argc, char** argv) {
	auto params = loadParams(argc, argv);
	auto ems = ems::Ems<true> { params };
	std::string proj_file = params.get<projectionsFile>();
	std::string star_file = params.get<projectionsStarFile>();

	//Projections
	auto dims = mrc::GetDimensions(proj_file);
	bolt::Int2 image_size = u(params.get<detectorPixelSize>());
	if (!(image_size > bolt::Int2 { 0, 0 })) {
		image_size = { std::get<0>(dims), std::get<1>(dims) };
	}

	std::vector<ctftools::Projection> projections { };
	ctftools::StarFileReader<float>::Read(star_file, projections, image_size);

	ems::Ems<true>::ImgType<float, 2> rendered_image { image_size };
	auto rendered_view = rendered_image.view();

	CtfConvolver<decltype(rendered_view)> convolver { image_size };

	auto dirs = params.get<directions>();
	dirs = dirs <= 0 ? projections.size() : dirs;
	bolt::HostImage<float, 3> images_memory { image_size[0], image_size[1], dirs * params.get<samples>() };
	auto images = images_memory.view();

	ems::Ems<true>::ImgType<float, 2> render_memory { image_size };
	auto rendered = render_memory.view();

	float pixel_size_ang;

	for (size_t direction = 0; direction < dirs; ++direction) {
		for (size_t sample = 0; sample < params.get<samples>(); ++sample) {
			auto slice = images.slice<2>(direction * params.get<samples>() + sample);
			auto ctf_params = projections[direction].PsfParams<ctftools::CtfParameters>();
			auto detector_distance = ems::Bohr { 0 };
			if (params.get<ems::ems_ApplyDefocus>()) {
				detector_distance = max(ems::Bohr { ems::angToBohr(abs(ctf_params.defocus)) }, ems::Bohr { 0 });
			}

			auto detector = ems.render(rendered, projections[direction].Orientation(), { }, detector_distance);
			pixel_size_ang = ems::bohrToAng(u(detector.pixel_size[0]));

			bolt::copy(rendered, slice);
			std::cout << "direction: " << direction << "  sample: " << sample << " done " << std::endl;
		}
	}

	bolt::HostImage<float, 2> host_memory { image_size };
	auto host_view = host_memory.view();

	bolt::dimensionReduce(images, host_view, bolt::DimensionValue<2> { }, 0.0f, thrust::plus<float> { }, bolt::ExecutionPolicy { });
	bolt::copy(bolt::multiplyByFactor(1.0f / dirs, host_view), host_view);
	bolt::dump(host_view, ems.modelName() + "_average");
	bolt::copy(bolt::addValue(-host_view[bolt::Int2 { 0, 0 }], host_view), host_view);
	bolt::dump(host_view, ems.modelName() + "_averageZeroBG");
	mrc::Write(ems.modelName() + "_projections.mrc", images, pixel_size_ang);

	return 0;
}
