/*
 * fourier_main.cu
 *
 *  Created on: Mar 10, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#include "helper_func.h"

PARAM(ft_normalization, float, "normalize volume");
PARAM(ft_position, ems::Bohr3, "position inside the sample");
PARAM(ft_direction, ems::Num3, "direction of the incoming electron inside the sample");
PARAM(ft_orientation, bolt::Quaternion<float>, "orientation of the camera/scattering direction");

PARAMWD(scatterEL_spreadAngle, float, 0.0349066, "max spread angle of the scattering function [radians], default is approx 2 degrees");

template<typename TView, typename SCATTERING_FUNC>
class DifferentialCrossSectionFunctor {
public:
	DifferentialCrossSectionFunctor(
		bolt::Int2 view_size,
		bolt::Float3 direction,
		ems::Bohr3 position,
		SCATTERING_FUNC scattering,
		bolt::Quaternion<float> rotation,
		float spread_angle,
		float total_cs) :
		view_size(bolt::Int2 { view_size }),
		direction(direction),
		position(position),
		scattering(scattering),
		rotation(rotation),
		spread_angle(spread_angle),
		total_cs(total_cs)
	{
	}

	BOLT_DECL_HYBRID
	void operator()(typename TView::AccessType val, const bolt::Int2& index) const {
		auto theta_phi = getThetaPhiFromIndex(index);
		auto theta = theta_phi[0];
		auto phi = theta_phi[1];

		auto local_dir = bolt::Float3 { sin(theta) * cos(phi), sin(theta) * sin(phi), cos(theta) };
		bolt::Float3 sampled_dir = bolt::rotate(local_dir, rotation);
		auto value = scattering.getPdfForDir(ems::Ray { position, direction }, sampled_dir, total_cs);
		val = value;
	}

//	bolt::Float2 getThetaPhiFromIndex(const bolt::Int2& index) const {
//		bolt::Float2 samples = bolt::div(bolt::Float2 { index[0], index[1] }, view_size);
//		auto theta = samples[0] * M_PI;
//		auto phi = samples[1] * 2 * M_PI; //atan(exp(-samples[1] * 2 * M_PI));
//		return {theta, phi};
//	}

	BOLT_DECL_HYBRID
	bolt::Float2 getThetaPhiFromIndex(const bolt::Int2& index) const {
		bolt::Float2 dir = bolt::div(bolt::Float2 { (float) index[0], (float) index[1] }, view_size) - bolt::Float2 { 0.5, 0.5 };
		auto theta = bolt::norm(dir) * spread_angle;
		auto phi = atan2f(dir[0], dir[1]) + M_PIf32;

		return {theta, phi};
	}

	ems::Bohr3 position;
	bolt::Float2 view_size;
	bolt::Float3 direction;
	SCATTERING_FUNC scattering;
	bolt::Quaternion<float> rotation;
	float spread_angle;
	float total_cs;
};

class HeyneyGreensteinFunctor {
public:
	HeyneyGreensteinFunctor(bolt::Int2 view_size, float spread_angle, float g_param) :
		view_size(bolt::Int2 { view_size }),
		spread_angle(spread_angle),
		g_param(g_param)
	{
	}

	BOLT_DECL_HYBRID
	void operator()(float& val, const bolt::Int2& index) const {
		auto theta_phi = getThetaPhiFromIndex(index);
		auto theta = theta_phi[0];
		auto phi = theta_phi[1];

		auto g2 = g_param * g_param;
		val = (1.0 - g2) / (4.0 * M_PI * pow(1.0 + g2 - 2.0 * g_param * cos(theta), 3.0 / 2.0));
	}

	BOLT_DECL_HYBRID
	bolt::Float2 getThetaPhiFromIndex(const bolt::Int2& index) const {
		bolt::Float2 dir = bolt::div(bolt::Float2 { (float) index[0], (float) index[1] }, view_size) - bolt::Float2 { 0.5, 0.5 };
		auto theta = bolt::norm(dir) * spread_angle;
		auto phi = atan2f(dir[0], dir[1]) + M_PIf32;

		return {theta, phi};
	}

	bolt::Float2 view_size;
	bolt::Quaternion<float> rotation;
	float spread_angle;
	float g_param;
};

class RutherfordFunctor {
public:
	RutherfordFunctor(bolt::Int2 view_size, float spread_angle, ems::RutherfordFormulae rutherford) :
		view_size(bolt::Int2 { view_size }),
		spread_angle(spread_angle),
		rutherford(rutherford)
	{
	}

	BOLT_DECL_HYBRID
	void operator()(float& val, const bolt::Int2& index) const {
		auto theta_phi = getThetaPhiFromIndex(index);
		auto theta = theta_phi[0];
		auto phi = theta_phi[1];
		val = rutherford.pdf(cos(theta));
		val = val / (ems::Num { 2 } * ems::Num { M_PI });
	}

	BOLT_DECL_HYBRID
	bolt::Float2 getThetaPhiFromIndex(const bolt::Int2& index) const {
		bolt::Float2 dir = bolt::div(bolt::Float2 { (float) index[0], (float) index[1] }, view_size) - bolt::Float2 { 0.5, 0.5 };
		auto theta = bolt::norm(dir) * spread_angle;
		auto phi = atan2f(dir[0], dir[1]) + M_PIf32;

		return {theta, phi};
	}

	bolt::Float2 view_size;
	float spread_angle;
	ems::RutherfordFormulae rutherford;
};

int main(int argc, char** argv) {
	auto params = loadParams(argc, argv);
	auto spread_angle = params.get<scatterEL_spreadAngle>();

	std::string proj_file = params.get<ems::ems_projectionsFile>();
	std::string star_file = params.get<ems::ems_projectionsStarFile>();

	std::string potential_file = params.get<ems::ems_potentialFile>();
	std::string scattering_file = params.get<ems::ems_scatteringFile>();
	std::string cross_sections_file = params.get<ems::crossSection_crossSectionsFile>();
	std::string dump_path = params.get<ems::crossSection_dumpPath>();

	std::string volume_file;
	if (scattering_file != "") {
		volume_file = scattering_file;
	} else {
		volume_file = potential_file;
	}

	ems::IVoxel volume_size { };
	ems::Bohr bohr_per_voxel { };
	parseMRCHeader(volume_file, volume_size, bohr_per_voxel);
	ems::ElectronMicroscopeSetup setup { volume_size, bohr_per_voxel, params };

	//memory holder
	bolt::HostImage<float, 3> host_volume { u(volume_size) };
	auto host = host_volume.view();

	//loading potential
	IntImgType<float, 3> potential_volume { u(volume_size) };
	if (potential_file != "") {
		load(potential_file, host, potential_volume.view());
	}
	auto potential_int_view = makeInterpolated(potential_volume.view());
	auto physical_potential_int_view = ems::makePhysicalVolume(potential_int_view, setup);

	//loading / computing cross sections volume
	IntImgType<float, 3> cs_volume { u(volume_size) };
	if (cross_sections_file != "") {
		load(cross_sections_file, host, cs_volume.view());
	} else {
		load(potential_file, host, cs_volume.view());
		ImgType<float, 3> tmp_cross_sections { u(volume_size) };
		ems::precomputeLocalCrossSections(
			ems::makePhysicalVolume(makeInterpolated(cs_volume.view()), setup),
			tmp_cross_sections.view(),
			params);
		if (dump_path != "") {
			bolt::copy(tmp_cross_sections.view(), host_volume.view());
			mrc::Write(dump_path, host_volume.view(), u(BOHR_TO_ANG * bohr_per_voxel));
		}
		bolt::copy(tmp_cross_sections.view(), cs_volume.view());
	}
	auto cs_int_view = makeInterpolated(cs_volume.view());
	auto physical_cs_int_view = ems::makePhysicalVolume(cs_int_view, setup);

	//loading scattering
	IntImgType<float, 3> scattering_volume { u(volume_size) };
	if (scattering_file != "") {
		load(scattering_file, host, scattering_volume.view());
	} else {
		load(potential_file, host, host);
		bolt::copy(bolt::square(host), host);
		bolt::copy(host, scattering_volume.view());
	}
	auto scattering_int_view = makeInterpolated(scattering_volume.view());
	auto physical_scattering_int_view = ems::makePhysicalVolume(scattering_int_view, setup);

	//Projections
	auto dims = mrc::GetDimensions(proj_file);
	bolt::Int2 image_size = u(params.get<ems::ems_detectorPixelSize>());
	if (!(image_size > bolt::Int2 { 0, 0 })) {
		image_size = { std::get<0>(dims), std::get<1>(dims) };
	}

	std::vector<ctftools::Projection> projections { };
	ctftools::StarFileReader<float>::Read(star_file, projections, image_size);

	bolt::copy(scattering_int_view, host_volume.view());
	auto majorant = getValueOfPercentile(host_volume.view(), params.get<ems::ems_majorantPercentile>());

	RutherfordsImage rutherfords { params.get<ems::ems_majorantAtomicNumber>() };
	ems::fillRutherfordsView(rutherfords.view(), ems::PeriodicTable { }, params.get<ems::ems_electronEnergy>());
	auto rutherford_scatter = ems::RutherfordScattering<decltype(cs_int_view), decltype(rutherfords.view())> { physical_scattering_int_view,
		rutherfords.view(), majorant };

	ems::PeriodicTable p { };
	auto ruther_electron_scatter = ems::RutherElectronScattering<decltype(cs_int_view)> { physical_potential_int_view, physical_cs_int_view,
		physical_scattering_int_view, params, ems::RutherfordFormulae { p.get(params.get<ems::ems_samplingAtomicNumber>()), params.get<
			ems::ems_electronEnergy>() }, setup.rutherford.meanFreePath(), majorant };

	auto electron_scatter = ems::ElectronScattering<decltype(cs_int_view)> { physical_potential_int_view, physical_cs_int_view,
		physical_scattering_int_view, params, setup.rutherford.meanFreePath(), majorant };

	auto generators = ems::createRandomGeneratorsImage<decltype(cs_int_view)::kIsDeviceView>(
		u(setup.emitter_size),
		params.get<ems::ems_seed>());

	ImgType<float, 2> rendered_image { image_size };
	auto rendered_view = rendered_image.view();

	auto em_ruther = ems::constructElectronMicroscope(setup, rutherford_scatter, ems::CounterImageFormation { });

	auto em_electron = ems::constructElectronMicroscope(setup, electron_scatter, ems::CounterImageFormation { });

	auto em_rutherele = ems::constructElectronMicroscope(setup, ruther_electron_scatter, ems::CounterImageFormation { });

	auto classic = ems::constructElectronMicroscope(
		setup,
		ems::NoScattering { },
		ems::WeightedDistanceImageFormation { },
		ems::makeWeightingVolumeRayObserver(physical_potential_int_view, 0));

	CtfConvolver<decltype(rendered_view)> convolver { image_size };

	HeyneyGreensteinFunctor hg { image_size, spread_angle, params.get<ems::scatterHG_greensteinG>() };

	RutherfordFunctor ruther { image_size, spread_angle, setup.rutherford };

	auto cross_section_frame_radius = params.get<ems::scatterEL_crossSectionFrameRadius>();
	auto max_samples_per_axis = params.get<ems::scatterEL_maxSamplesPerAxis>();

	bolt::copy(potential_int_view, host_volume.view());
	auto host_int = makeInterpolated(host_volume.view());
	auto host_potential_phys = ems::makePhysicalVolume(host_int, setup);

	bolt::DeviceImage<float, 2> dimage { image_size };
	auto dview = dimage.view();

	bolt::forEachPosition(dview, ruther);
	bolt::dump(dview, "ruther_");

	std::cout << spread_angle << std::endl;
	for (size_t x = 0; x < 20; x++) {
		auto g = 0.4 + 0.5999 * (x / float(16));
		hg = { image_size, spread_angle, g };
		bolt::forEachPosition(dview, hg);
		bolt::dump(dview, "hg_" + std::to_string(g) + "_");
		ems::RutherfordFormulae r { p.get(x + 1), 300 };
		RutherfordFunctor ru { image_size, spread_angle, r };
		bolt::forEachPosition(dview, ru);
		bolt::dump(dview, "ruther_" + std::to_string(x) + "_");
	}

	for (size_t x = 0; x < 20; ++x) {
		auto p = 0.33 + x * (0.33 / 20.0);
		auto pos = setup.interaction_bounding_box.min() + bolt::product(bolt::Float3 { p, 0.5, p }, setup.interaction_bounding_box.size());

		ems::Num total_cs = 1;
		if (total_cs > 0) {
			bolt::Quaternion<float> orientation = { -0.242652, 0.255120, 0.657212, 0.666278 };
			auto dir = bolt::rotate(bolt::normalize(params.get<ft_direction>()), orientation);
			DifferentialCrossSectionFunctor<decltype(dview), decltype(electron_scatter)> dcsf { image_size, dir, pos, electron_scatter,
				orientation, spread_angle, total_cs };
			bolt::forEachPosition(dview, dcsf);

			if (bolt::sum(dview, 0) > 0) {
				bolt::dump(
					dview,
					"img_d" + std::to_string(x) + "_csrad_" + std::to_string(u(cross_section_frame_radius)) + "_spax_"
						+ std::to_string(max_samples_per_axis) + "_tcs_" + std::to_string(total_cs) + "_");
			}
		}
	}
	exit(0);
}

//OUTDATED
/// Test of localized fourier via inverse fourier and comparison of input and output
//	using FourierImageType = bolt::DeviceImage<bolt::DeviceComplexType, ImgType<float, 3>::kDimension>;
//
//	FourierImageType img { bolt::getFftImageSize(interaction_volume.size()) };
//
//	bolt::forEachPosition(img.view(), [localized_fp]BOLT_DECL_HYBRID(FourierImageType::Element& value, FourierImageType::IndexType index) {
//			value = localized_fp[index];
//		});
//
//	ImgType<float, 3> after_inverse_volume { host_volume.size() };
//	bolt::FftCalculator<3, bolt::DeviceFftPolicy<bolt::Inverse>> calc { interaction_volume.size() };
//	calc.calculateAndNormalize(img.view(), after_inverse_volume.view());
//	bolt::dump(after_inverse_volume.view(), "after");
