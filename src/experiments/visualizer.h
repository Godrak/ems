/*
 * visualizer.h
 *
 *  Created on: Feb 11, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_VISUALIZER_H_
#define SRC_VISUALIZER_H_

#include "boltview/math/vector.h"
#include "boltview/math/quaternion.h"
#include "boltview/cuda_defines.h"
#include "boltview/cuda_utils.h"
#include "boltview/copy.h"
#include "boltview/reduce.h"

#include "epoxy/gl.h"

#include "cuda_runtime.h"
#include "cuda_gl_interop.h"

namespace ems {

template<typename TView>
void scaleVisual(TView view) {
	auto minmax = bolt::minMax(view);
	auto pos = bolt::addValue(-minmax[0], view);
	minmax = bolt::minMax(pos);
	auto scaled = bolt::multiplyByFactor(1.0 / minmax[1], pos);
	bolt::copy(scaled, view);
}

class Visualizer {
public:
	bolt::Quaternion<float> orientation;
	bolt::Int2 image_size;
	cudaGraphicsResource_t image_resource;

	bool flag_one = false;
	bool flag_two = false;
	bool flag_three = false;

	Visualizer(const Visualizer& other) = delete;
	Visualizer& operator=(const Visualizer& other) = delete;
	Visualizer(Visualizer&& other) noexcept = delete;
	Visualizer& operator=(Visualizer&& other) noexcept = delete;
	~Visualizer();

	Visualizer(bolt::Int2 image_size);
	bool render();
	auto lockAndGetImageView() {
		BOLT_CHECK(cudaGraphicsResourceSetMapFlags(image_resource, cudaGraphicsMapFlagsWriteDiscard));
		BOLT_CHECK(cudaGraphicsMapResources(1, &image_resource));

		float* data_dptr;
		size_t data_size;
		BOLT_CHECK(cudaGraphicsResourceGetMappedPointer((void** ) &data_dptr, &data_size, image_resource));

		auto view = bolt::makeDeviceImageView(data_dptr, image_size, bolt::stridesFromSize(image_size));
		return view;
	}

	void unlock() {
		BOLT_CHECK(cudaGraphicsUnmapResources(1, &image_resource));
	}
};

}

#endif /* SRC_VISUALIZER_H_ */
