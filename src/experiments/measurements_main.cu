/*
 * measurements_main.cpp
 *
 *  Created on: Nov 26, 2019
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#include "../external/StarFileReader.h"
#include "../external/projection.h"
#include "../external/ctf_convolve.h"

#include "helper_func.h"
#include "ems/ems.h"

namespace lambda {
using namespace bolt;

using Element = float;
using TType = float;

void Compute(
	const Int2 projectionSize,
	DeviceImageView<Element, 2> projection,
	const Int3 volSize,
	DeviceImageConstView<const Element, 3> volume,
	DeviceImageView<Element, 2> matrix,
	const Quaternion<float> orientation,
	Float3 shift,
	DeviceImageConstView<const Element, 2> inputVector,
	DeviceImageView<Element, 2> inputOutputVector)
{
//        std::cout << "Projection size: " << projection.Size() << std::endl;
//        std::cout << "Volume size: " << volume.Size() << std::endl;
//		std::cout << "wMatrix size: " << matrix_.Size() << std::endl;
//        std::cout << "Orientation: " << orientation << std::endl;
	//DeviceImage<Int3, 3> voxelDebug(Int3(projectionSize[0], projectionSize[1], 50));
	//auto voxelDebugView = voxelDebug.View();
	//matrix_.Clear();

	//auto wMatrix = matrix_.View();
	//float boundingRadius = sqrtf(SquaredNorm(volSize));
	float boundingRadius = volSize[0];
	// Float2 projectionSizeFloat = Float2(projectionSize[0], projectionSize[1]);
	//DeviceImage<TType, 2> dProjection(projectionSize);

	// Float3 rayDir = -Rotate(Float3(0.f, 0.f, 1.f), orientation);
	//Float3 u = Rotate(Float3(1.f, 0.f, 0.f), orientation);
	//Float3 v = Rotate(Float3(0.f, 1.f, 0.f), orientation);

forEachPosition(
	projection,
	[orientation, shift, projectionSize, volSize, volume, boundingRadius, matrix, inputVector, inputOutputVector]BOLT_DECL_DEVICE(
		TType& val,
		Int2 idx) {

		bool computeProjection=product(volume.size())>0 ? true:false;
		bool computeMatrix=product(matrix.size())>0 ? true:false;
		bool computeMultipByTranspose=product(inputVector.size())>0 ? true:false;

		Float3 volumeSize = Float3(volSize);
		Float3 rayDir = -rotate(Float3(0.f, 0.f, 1.f), orientation);
		//Perturb the ray direction to get rid of axis-parallel components
		Float3 rayDirPertubed(rayDir);
		for(int i = 0; i < 3; ++i) {
			if(rayDirPertubed[i] == 0.0f) {
				rayDirPertubed[i] = 1e-6;
			}
		}

		//Float2 physicalProjectionExtent = Float2(idx[0], idx[1]) - 0.5f * projectionSizeFloat + Float2(.5f, .5f);
		Float2 physicalProjectionExtent = boundingRadius * (div(Float2(idx[0]+shift[0], idx[1]+shift[1]), Float2(projectionSize[0], projectionSize[1])) - Float2(.5f, .5f));
		Float3 u = rotate(Float3(1.f, 0.f, 0.f), orientation);
		Float3 v = rotate(Float3(0.f, 1.f, 0.f), orientation);

		Float3 rayOrig = -boundingRadius * rayDirPertubed + physicalProjectionExtent[0] * u + physicalProjectionExtent[1] * v;
		// shift to align the volume with Relion, probably relates to voxel origin (center in Relion, corner in our code) - needed for nearest neighbor, not trilinear interp.
		// rayOrig+= Float3(0.5f,0.5f,0.5f);

		Float3 voxelSize(1, 1, 1);
		Float3 boxMin = 0.5f * product(-volumeSize, voxelSize);
		Float3 boxMax = 0.5f * product(volumeSize, voxelSize);

		Float3 tEntry = div(boxMin - rayOrig, rayDirPertubed);
		Float3 tExit = div(boxMax - rayOrig, rayDirPertubed);

		Float3 tMin = min(tEntry, tExit);
		Float3 tMax = max(tEntry, tExit);

		float tIn = maxElement(tMin);
		float tOut = minElement(tMax);

		float tDistance = tOut - tIn;
		float epsilon = min(1e-4f, tDistance / 2);

		val = 0.0f;

		if(tDistance > 0.f)
		{

			Float3 entryPoint(rayOrig + (tIn + epsilon) * rayDirPertubed);
			//Int3 curVoxel(entryPoint - boxMin);
			Int3 curVoxel(floor(div((entryPoint - boxMin), voxelSize)));

			//step direction
			Int3 sign;
			for(int i = 0; i < 3; ++i) {
				sign[i] = rayDirPertubed[i] < 0.f? -1 : 1;
			}

			//step difference in each direction.
			Float3 stepDifference = abs(div(voxelSize, rayDirPertubed));

			//Does this: res = floor(entryPoint / voxelSize) * voxelSize;
			Float3 entryVoxelCorner = product(floor(div(entryPoint, voxelSize)), voxelSize);

			Float3 step;
			for(int i = 0; i < 3; ++i) {
				step[i] = rayDirPertubed[i] > 0.f ? voxelSize[i] : 0.f;
			}
			Float3 remainder = div(entryVoxelCorner + step - entryPoint, rayDirPertubed);

			//not needed because of ray perturbance
			//res = blend(rayDir == rep3f(0), rep3f(FLT_MAX), res);

			Float3 nearestDist;
			for(int i = 0; i < 3; ++i) {
				nearestDist[i] = remainder[i] == 0.f ? stepDifference[i] : remainder[i];
			}

			//step distance in unit of t
			float distance = 0.f;

			int numVisitedVoxels = 0;

			//float weightSum = 0.f;

			int64_t rowIndex = idx[0] + int64_t(projectionSize[0]) * idx[1];
			float tCurrent = tIn;
			while(tCurrent < tOut) {

				Float3 currentEntry(rayOrig + (tCurrent + epsilon) * rayDirPertubed);
				Float3 currentEntryRelative(currentEntry-floor(currentEntry));

				bool voxelIsInsideVolume = (curVoxel >= Int3(0,0,0) && curVoxel < volumeSize);
				if(!voxelIsInsideVolume) {
					break;
				}
				//gets incremented before use, since this total number itself is stored at index 0 in the array
				++numVisitedVoxels;
				float deltaT = minElement(nearestDist);

				distance = tCurrent;
				tCurrent = tIn + deltaT;
				distance = tCurrent - distance;

				Float3 currentExit(currentEntry+distance*rayDirPertubed);
				Float3 currentExitRelative(currentExit-floor(currentExit));

				//Trilinear interpolation

				int num=2;

				for(int i=0;i<num;++i) {
					for(int j=0;j<num;++j) {
						for(int k=0;k<num;++k) {

							//voxelDebugView[Int3(idx[0], idx[1], numVisitedVoxels)] = curVoxel;
							int64_t columnIndex = (curVoxel[0]+i) + int64_t(volSize[0]) * (curVoxel[1]+j) + int64_t(volSize[0])*int64_t(volSize[1]) * (curVoxel[2]+k);

							//weights computation using Trilinear interpolation
							float a0=(i>0)?currentEntryRelative[0]:(1-currentEntryRelative[0]);
							float b0=(j>0)?currentEntryRelative[1]:(1-currentEntryRelative[1]);
							float c0=(k>0)?currentEntryRelative[2]:(1-currentEntryRelative[2]);

							float a2=(i>0)?currentExitRelative[0]:(1-currentExitRelative[0]);
							float b2=(j>0)?currentExitRelative[1]:(1-currentExitRelative[1]);
							float c2=(k>0)?currentExitRelative[2]:(1-currentExitRelative[2]);

							float f0=a0*b0*c0;
							float f2=a2*b2*c2;

							float a1=(i>0)?0.5*(currentEntryRelative[0]+currentExitRelative[0]):(1-0.5*(currentEntryRelative[0]+currentExitRelative[0]));
							float b1=(j>0)?0.5*(currentEntryRelative[1]+currentExitRelative[1]):(1-0.5*(currentEntryRelative[1]+currentExitRelative[1]));
							float c1=(k>0)?0.5*(currentEntryRelative[2]+currentExitRelative[2]):(1-0.5*(currentEntryRelative[2]+currentExitRelative[2]));

							float f1=a1*b1*c1;

							// Simpsons rule
							float weight=distance*(f0+4*f1+f2)/6;

							if(computeMatrix) {matrix[Int2(columnIndex, rowIndex)] = weight;}

							if (computeProjection) {
								if((curVoxel+Int3(i,j,k))<volume.size())
								val += volume[(curVoxel+Int3(i,j,k))] * weight;
							}

							if (computeMultipByTranspose) {
								atomicAdd(&inputOutputVector[Int2(0,columnIndex)],weight*inputVector[Int2(0,rowIndex)]);
							}
						}
					}
				}
				//end Trilinear interpolation

				//DDA step
				for(int i = 0; i < 3; ++i) {
					if(deltaT == nearestDist[i]) {
						nearestDist[i] += stepDifference[i];
						curVoxel[i] += sign[i];
						break;
					}
				}
			}
			//voxelDebugView[Int3(idx[0], idx[1], 0)] = Int3(numVisitedVoxels, 0, 0);

//                    if(weightSum != 0){
//                       for(int i = 0; i < Product(volumeSize); ++i){
//                            wMatrix[Int2(i, rowIndex)] /= weightSum;
//                        }
//                        val = val / weightSum;
//                    }

		}
	});

//Copy(dProjection.ConstView(), projection_.View());
//Copy(wMatrix, matrix_.View());
/*
 HostImage<TType, 2> hostWMatrix(matrix_.Size());
 Copy(matrix_.ConstView(), hostWMatrix.View());
 auto hostWMatrixView = hostWMatrix.ConstView();
 HostImage<Int3, 3> hostVoxelDebug(voxelDebug.Size());
 Copy(voxelDebug.ConstView(), hostVoxelDebug.View());
 auto hostVoxelDebugView = hostVoxelDebug.ConstView();
 HostImage<TType, 2> iterationImage(projectionSize);
 ForEachPosition(iterationImage.View(), [projectionSize, volume, hostVoxelDebugView, hostWMatrixView](TType& val, Int2 idx){
 float crc = 0.f;
 int64_t numWeights = 0;
 std::cout << (boost::format("[%1%, %2%]:") % idx[0] % idx[1]).str();
 for(int i = 0; i < Product(volume.Size()); ++i){
 float weight = hostWMatrixView[Int2(i, idx[0] + idx[1] * projectionSize[0])];
 if(weight != 0.0f){
 crc += weight;
 numWeights++;
 Int3 voxelIndex;
 voxelIndex[2] = i / (volume.Size()[0]*volume.Size()[1]);
 voxelIndex[1] = (i % (volume.Size()[0]*volume.Size()[1])) / volume.Size()[0];
 voxelIndex[0] = i % volume.Size()[0];
 std::cout << (boost::format(" [%1%, %2%, %3%](%4%)") % voxelIndex[0] % voxelIndex[1] % voxelIndex[2] % weight).str();
 }
 }
 std::cout << (boost::format(" : %1% : %2%\n") % crc % numWeights).str();
 std::cout << (boost::format("[%1%, %2%]:") % idx[0] % idx[1]).str();
 float weightSum = 0.f;
 for(int i = 1; i <= hostVoxelDebugView[Int3(idx[0], idx[1], 0)][0]; ++i){
 Int3 voxelIndex = hostVoxelDebugView[Int3(idx[0], idx[1], i)];
 int64_t columnIndex = voxelIndex[0] + volume.Size()[0] * voxelIndex[1] + volume.Size()[0]*volume.Size()[1] * voxelIndex[2];
 float weight = hostWMatrixView[Int2(columnIndex, idx[0] + idx[1] * projectionSize[0])];
 weightSum += weight;
 std::cout << (boost::format(" [%1%, %2%, %3%](%4%)") % voxelIndex[0] % voxelIndex[1] % voxelIndex[2] % weight).str();
 }
 int numVisitedVoxels = std::max(0, hostVoxelDebugView[Int3(idx[0], idx[1], 0)][0]);
 std::cout << " : " << weightSum << " : " << numVisitedVoxels;
 std::cout << std::endl;
 });
 */
}
}

int main(int argc, char** argv) {
auto params = loadParams(argc, argv);
auto ems = ems::Ems<true> { params };
std::string proj_file = params.get<projectionsFile>();
std::string star_file = params.get<projectionsStarFile>();

//Projections
auto dims = mrc::GetDimensions(proj_file);
bolt::Int2 image_size = u(params.get<detectorPixelSize>());
if (!(image_size > bolt::Int2 { 0, 0 })) {
	image_size = { std::get<0>(dims), std::get<1>(dims) };
}

std::vector<ctftools::Projection> projections { };
ctftools::StarFileReader<float>::Read(star_file, projections, image_size);

ems::Ems<true>::ImgType<float, 2> rendered_image { image_size };
auto rendered_view = rendered_image.view();

CtfConvolver<decltype(rendered_view)> convolver { image_size };

//	ems::Ems<true>::ImgType<float, 3> deviceimg { u(volume_size) };
//	bolt::copy(potential_volume.view(), deviceimg.view());

for (size_t direction = 0; direction < params.get<directions>(); ++direction) {
	for (size_t sample = 0; sample < params.get<samples>(); ++sample) {
		auto ctf_params = projections[direction].PsfParams<ctftools::CtfParameters>();
		auto detector_distance = ems::Bohr { 0 };
		if (params.get<ems::ems_ApplyDefocus>()) {
			detector_distance = max(ems::Bohr { ems::angToBohr(abs(ctf_params.defocus)) }, ems::Bohr { 0 });
		}

		if (params.get<ems::ems_Model>() == "lambda") {
			cudaDeviceSynchronize();
			auto begin = std::chrono::high_resolution_clock::now();
			lambda::Compute(
				image_size,
				rendered_view,
				u(volume_size),
				deviceimg.constView(),
				{ },
				projections[direction].Orientation(),
				{ },
				{ },
				{ });
			cudaDeviceSynchronize();
			auto end = std::chrono::high_resolution_clock::now();
			std::cout << "lambda: " << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << "ns" << std::endl;
//			bolt::dump(rendered_view, "lambda" + std::to_string(direction) + "_" + std::to_string(sample));
		} else {

			// code to benchmark
			cudaDeviceSynchronize();
			auto begin = std::chrono::high_resolution_clock::now();
			ems.render(rendered_view, projections[direction].Orientation(), { }, detector_distance);
			cudaDeviceSynchronize();
			auto end = std::chrono::high_resolution_clock::now();
			std::cout << ems.modelName() << " " << std::chrono::duration_cast<std::chrono::nanoseconds>(end - begin).count() << "ns"
				<< std::endl;
			//			bolt::dump(rendered_view, "result" + std::to_string(direction) + "_" + std::to_string(sample));

		}

		std::cout << "direction: " << direction << "  sample: " << sample << " done " << std::endl;
	}
}

return 0;
}
