/*
 * test_main.cpp
 *
 *  Created on: Feb 11, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#include "../external/StarFileReader.h"
#include "../external/projection.h"
#include "../external/ctf_convolve.h"

#include "../helper_func.h"
#include "../ems/ems.h"

int main(int argc, char** argv) {
	auto params = loadParams(argc, argv);
	auto ems = ems::Ems<true> { params };
	std::string proj_file = params.get<projectionsFile>();
	std::string star_file = params.get<projectionsStarFile>();

	//Projections
	auto dims = mrc::GetDimensions(proj_file);
	bolt::Int2 image_size = u(params.get<detectorPixelSize>());
	if (!(image_size > bolt::Int2 { 0, 0 })) {
		image_size = { std::get<0>(dims), std::get<1>(dims) };
	}

	std::vector<ctftools::Projection> projections { };
	ctftools::StarFileReader<float>::Read(star_file, projections, image_size);

	ems::Ems<true>::ImgType<float, 2> rendered_image { image_size };
	auto rendered_view = rendered_image.view();

	CtfConvolver<decltype(rendered_view)> convolver { image_size };

	for (size_t direction = 0; direction < params.get<directions>(); ++direction) {
		for (size_t sample = 0; sample < params.get<samples>(); ++sample) {
			auto ctf_params = projections[direction].PsfParams<ctftools::CtfParameters>();
			auto detector_distance = ems::Bohr { 0 };
			if (params.get<ems::ems_ApplyDefocus>()) {
				detector_distance = max(ems::Bohr { ems::angToBohr(abs(ctf_params.defocus)) }, ems::Bohr { 0 });
			}

			ems.render(rendered_view, projections[direction].Orientation(), { }, detector_distance);
			bolt::dump(rendered_view, "result" + std::to_string(direction) + "_" + std::to_string(sample));

			std::cout << "direction: " << direction << "  sample: " << sample << " done " << std::endl;
		}
	}

	return 0;
}

