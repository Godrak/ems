/*
 * ems_setup.h
 *
 *  Created on: Oct 19, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_EMS_SETUP_H_
#define SRC_EMS_EMS_SETUP_H_

#include "defines.h"
#include "params.h"
#include "math_utils.h"
#include "boltview/math/vector.h"
#include "periodic_table.h"
#include  "screened_rutherford_formulae.h"

namespace ems {

struct ElectronMicroscopeSetup {
	ElectronMicroscopeSetup() = default;

	ElectronMicroscopeSetup(IVoxel interaction_volume_size, const Bohr& bohr_per_voxel, const json_params::JsonParamsHandler& params) {
		Num3 size = t<Num3>(u(interaction_volume_size));
		if (!(size[0] == size[1] && size[1] == size[2])) {
			LOG("WARNING: this is not cube volume, make sure that rendering is done from default direction");
		}
		this->bohr_per_voxel = bohr_per_voxel;

		LOG("bohr per voxel: " << u(bohr_per_voxel));
		randomized_electron_position = params.get<ems_RandomizedElectronEmission>();

		Bohr3 half_of_real_size = size * (this->bohr_per_voxel * Num { 0.5 });
		interaction_bounding_box = BoundingBox { -half_of_real_size, half_of_real_size };
		default_detector_origin = -half_of_real_size;
		default_detector_origin[2] = t<Bohr>(-bolt::length(u(half_of_real_size)));

		default_emitter_origin = -half_of_real_size;
		default_emitter_origin[2] = t<Bohr>(bolt::length(u(half_of_real_size)));

		distance_from_emitter_to_detector = abs(default_emitter_origin[2] - default_detector_origin[2]);
		LOG("distance from emitter to detector: " << u(distance_from_emitter_to_detector));

		default_detector_x_vector = t<Bohr3>(0.0, 0.0, 0.0);
		default_detector_x_vector[0] = Num { 2.0 } * half_of_real_size[0];

		default_detector_y_vector = t<Bohr3>(0.0, 0.0, 0.0);
		default_detector_y_vector[1] = Num { 2.0 } * half_of_real_size[1];

		default_emitter_x_vector = default_detector_x_vector;
		default_emitter_y_vector = default_detector_y_vector;

		default_ray_dir = t<Num3>(0.0f, 0.0f, -1.0f);

		dose = params.get<ems_ElectronDose>();

		PeriodicTable periodic_table { };
		auto majorant_element = periodic_table.get(params.get<ems_MajorantAtomicNumber>());
		rutherford = RutherfordFormulae { majorant_element, params.get<ems_ElectronEnergy>() };

		estimated_mean_free_path = rutherford.meanFreePath();
		LOG("estimated mean free path: " << u(estimated_mean_free_path));

		auto fraction_of_unscattered_particles = std::exp(-distance_from_emitter_to_detector / estimated_mean_free_path);
		LOG("fraction_of_unscattered_particles: " << fraction_of_unscattered_particles);
		unscattered_dose = dose * fraction_of_unscattered_particles;
		scattered_dose = dose - unscattered_dose;
		LOG("unscattered dose: " << unscattered_dose << "   scattered dose: " << scattered_dose);

		auto params_emitter_size = params.get<ems_EmitterPixelSize>();
		if (params_emitter_size > t<IPixel>(0)) {
			emitter_size = t<IPixel2>(params_emitter_size, params_emitter_size);
		} else {
			emitter_size = t<IPixel2>(size[0], size[1]);
		}
		energy_keV = params.get<ems_ElectronEnergy>();
	}

	Bohr bohr_per_voxel;
	BoundingBox interaction_bounding_box;
	Bohr3 default_detector_origin;
	Bohr3 default_detector_x_vector;
	Bohr3 default_detector_y_vector;
	Bohr3 default_emitter_origin;
	Bohr3 default_emitter_x_vector;
	Bohr3 default_emitter_y_vector;
	Bohr distance_from_emitter_to_detector;
	Num3 default_ray_dir;
	float dose;
	float unscattered_dose;
	float scattered_dose;
	Bohr estimated_mean_free_path;
	IPixel2 emitter_size;
	int randomized_electron_position;
	RutherfordFormulae rutherford;
	float energy_keV;
};

}

#endif /* SRC_EMS_EMS_SETUP_H_ */
