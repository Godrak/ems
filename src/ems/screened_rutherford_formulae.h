/*
 * screened_rutherford_formulae.h
 *
 *  Created on: Oct 27, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_SCREENED_RUTHERFORD_FORMULAE_H_
#define SRC_EMS_SCREENED_RUTHERFORD_FORMULAE_H_

#include "defines.h"
#include "params.h"
#include "math_utils.h"
#include "boltview/math/vector.h"
#include "periodic_table.h"

namespace ems {

class RutherfordFormulae {
public:
	RutherfordFormulae() = default;

	RutherfordFormulae(const Element& element, double electron_energy_keV) {
		auto atomic_number = element.atomicno;
		auto beta = getBetaRatio(electron_energy_keV);

		ZReBetaExpr = Num { M_PI * M_PI } * atomic_number * atomic_number * classical_electron_radius * classical_electron_radius
			* ((1 - beta * beta) / (beta * beta * beta * beta));
		screening_parameter = getMoliereScreeningParameter(beta, atomic_number);
		auto scattering_centers_density = getScatteringCentresDensity(element.density, element.atomicw);
		total_cross_section = estimateTotalCrossSection();
		mean_free_path = meanFreePath(scattering_centers_density);

	}

	BOLT_DECL_HYBRID
	Bohr meanFreePath() const {
		return mean_free_path;
	}

	BOLT_DECL_HYBRID
	Num totalCrossSection() const {
		return total_cross_section;
	}

	BOLT_DECL_HYBRID
	Num differentialCrossSection(Num cos_theta) const {
		Num screening_factor = 1.0 / ((1.0 - cos_theta + 2.0 * screening_parameter) * (1.0 - cos_theta + 2.0 * screening_parameter));
		return ZReBetaExpr * screening_factor;
	}

	BOLT_DECL_HYBRID
	Num pdf(Num cos_theta) const {
		return Num { 2 } * screening_parameter * (screening_parameter + 1)
			/ ((1.0 - cos_theta + 2.0 * screening_parameter) * (1.0 - cos_theta + 2.0 * screening_parameter));
	}

	BOLT_DECL_HYBRID
	Num cumulativeDistributionFunction(Num cos_theta) const {
		return screening_parameter * (cos_theta + screening_parameter) / (Num { 2 } * screening_parameter - cos_theta + 1);
	}

	BOLT_DECL_HYBRID
	Num sampleCosTheta(float sample) const {
		return (2 * screening_parameter * sample - screening_parameter + sample) / (screening_parameter + sample);
	}

private:
	Bohr meanFreePath(double scattering_centers_density) const {
		return Bohr { 1.0 / (total_cross_section * scattering_centers_density) };
	}

	double getBetaRatio(double electron_energy_keV) const {
		double lorentz_factor = ((electron_energy_keV + electron_rest_mass) / electron_rest_mass);
		return sqrt(1.0 - 1.0 / (lorentz_factor * lorentz_factor));
	}

	double getMoliereScreeningParameter(double beta, short atomic_number) const {
		double alfa = 1.0 / 137.0; //fine structure constant - its actually a thing in physics
		double beta_expr = ((1 - beta * beta) / (beta * beta));
		double first_half = beta_expr * pow(atomic_number, 2.0 / 3.0) * alfa * alfa / (0.885 * 0.885 * 4);
		double second_half = 1.13 + 3.76 * (alfa * atomic_number / beta) * (alfa * atomic_number / beta);
		return first_half * second_half;
	}

	double getScatteringCentresDensity(double density_g_cm3, double atomic_weight) const {
		//particle density is Avogadro * density / atomic weight
		// transforming density from g/cm^3 to g/Bohr^3: x / (6.751269 * 1e24)
		// avogadro * density =  Av * 1e23 * d / (6.751269 * 1e24) = Av* d /(10 * 6.751269)

		auto avogadro_times_density = 6.02214086 * density_g_cm3 / (10 * 6.751269);
		auto particle_density = avogadro_times_density / atomic_weight;
		return particle_density;
	}

	double estimateTotalCrossSection() const {
		auto screening_factor = Num { 1 } / (Num { 2 } * screening_parameter * (Num { 1 } + screening_parameter));
		return ZReBetaExpr * screening_factor;
	}

private:
	double ZReBetaExpr;
	double screening_parameter;
	double total_cross_section;
	Bohr mean_free_path;

	static constexpr double classical_electron_radius = 0.0000532514; //Bohrs
	static constexpr double electron_rest_mass = 511; // keV/c^2
};

}

#endif /* SRC_EMS_SCREENED_RUTHERFORD_FORMULAE_H_ */
