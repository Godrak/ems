/*
 * ray_observers.h
 *
 *  Created on: Mar 3, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_RAY_OBSERVERS_H_
#define SRC_EMS_RAY_OBSERVERS_H_

#include "raycast_data.h"

namespace ems {

struct NullRayObserver {
	template<typename TPayload>
	BOLT_DECL_HYBRID
	void operator()(TPayload& payload, const Ray& ray, const Bohr& step) const {
	}
};

struct DistanceRayObserver {
	template<typename TPayload>
	BOLT_DECL_HYBRID
	void operator()(TPayload& payload, const Ray& ray, const Bohr& step) const {
		payload.distance += step;
	}
};

template<typename TVolumeView>
struct WeightingVolumeRayObserver {
	BOLT_DECL_HYBRID WeightingVolumeRayObserver(PhysicalVolume<TVolumeView> physical_volume, float outside_weight) :
		physical_volume(physical_volume),
		outside_weight(outside_weight),
		min_step_size(bolt::minElement(physical_volume.getSizeOfVoxel()) / 100.0f)
	{
	}

	template<typename TPayload>
	BOLT_DECL_HYBRID
	void operator()(TPayload& payload, const Ray& ray, const Bohr& step) const {
		payload.distance +=Bohr {getWeightedDistance(ray, step)};
	}

private:

	BOLT_DECL_HYBRID
	float getWeightedDistance(const Ray& start_ray, const Bohr& step) const {
		Bohr raytraced_distance { 0 };
		float result = 0;
		auto ray = start_ray;
		BoundingBox bb;
		Bohr tmin;
		Bohr tmax;
		if (!intersection(ray, physical_volume.getBoundingBox(), tmin, tmax)) { // we missed the weight volume
			return u(step) * outside_weight;
		}

		Bohr ray_end = min(step, tmax);

		if (tmin > Bohr { 0 }) { //we are not yet in the volume
			auto dis = min(step, tmin);
			result += u(dis * outside_weight);
			ray = Ray { ray.origin + ray.dir * dis, ray.dir };
			raytraced_distance += dis;
		}

		while (raytraced_distance < ray_end) { // tracing within the volume
			physical_volume.voxelBoundingBox(ray.origin, bb);
			intersection(ray, bb, tmin, tmax);
			tmax = max(Bohr { 0 }, min(tmax, step - raytraced_distance));
			tmin = max(tmin, Bohr { 0 });
			Bohr dis_to_voxel_edge = tmax - tmin;
			auto r = u(dis_to_voxel_edge) * physical_volume.access(ray.origin + ray.dir * (dis_to_voxel_edge / Num { 2 }));
			result += r;
			auto real_distance = max(min_step_size, dis_to_voxel_edge);
			ray = Ray { ray.origin + ray.dir * real_distance, ray.dir };
			raytraced_distance += real_distance;
		}
		result += u(step - raytraced_distance) * outside_weight; // finish
		return result;
	}

	PhysicalVolume<TVolumeView> physical_volume;
	float outside_weight;
	Bohr min_step_size;
};

template<typename TVolumeView>
BOLT_DECL_HYBRID
WeightingVolumeRayObserver<TVolumeView> makeWeightingVolumeRayObserver(PhysicalVolume<TVolumeView> physical_volume, float outside_weight) {
	return WeightingVolumeRayObserver<TVolumeView>(physical_volume, outside_weight);
}

template<typename TWeightingProjectionView, typename TResultVolumeView>
struct TransposedMultiplicationRayObserver {
	BOLT_DECL_HYBRID TransposedMultiplicationRayObserver(
		TWeightingProjectionView weighting_projection,
		PhysicalVolume<TResultVolumeView> result_volume) : weighting_projection(weighting_projection), result_volume(result_volume)
	{
	}

	template<typename TPayload>
	BOLT_DECL_HYBRID
	void operator()(TPayload& payload, const Ray& ray, const Bohr& step) const {
//TODO
	}

	TWeightingProjectionView weighting_projection;
	PhysicalVolume<TResultVolumeView> result_volume;
};

}

#endif /* SRC_EMS_RAY_OBSERVERS_H_ */
