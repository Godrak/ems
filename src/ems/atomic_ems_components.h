/*
 * atomic_ems_components.h
 *
 *  Created on: May 13, 2021
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_ATOMIC_EMS_COMPONENTS_H_
#define SRC_EMS_ATOMIC_EMS_COMPONENTS_H_

#include "math_utils.h"
#include "ems_setup.h"
#include "boltview/fill.h"
#include "boltview/for_each.h"

namespace ems {

struct EmsAtom {
	unsigned char atomic_number;
	Bohr3 position;
	Bohr3 velocity;
};

template<typename TValuesView>
struct PotentialValues {
	BOLT_DECL_HYBRID PotentialValues(TValuesView values_view, Bohr max_distance) :
		values_view(values_view),
		max_distance(max_distance),
		distance_per_voxel(max_distance / Num(values_view.size()[1]))
	{
	}

	BOLT_DECL_HYBRID
	Num compute(unsigned char atomic_number, Bohr distance) const {
		return values_view.access(bolt::Float2 { atomic_number, distance / distance_per_voxel });
	}

	TValuesView values_view;
	Bohr distance_per_voxel;
	Bohr max_distance;
};

template<typename TCellView, typename TAtomsView, typename TValuesView>
class PhysicalAtomicVolume {
public:

	PhysicalAtomicVolume(
		TCellView cell_view,
		TAtomsView atoms_view,
		PotentialValues<TValuesView> potential_values,
		const ElectronMicroscopeSetup& setup) :
		cell_view(cell_view),
		atoms_view(atoms_view),
		potential_values(potential_values),
		bounding_box(setup.interaction_bounding_box),
		bohrs_per_voxel(setup.bohr_per_voxel, setup.bohr_per_voxel, setup.bohr_per_voxel),
		voxel_size(bolt::div(bounding_box.size(), Num3 { cell_view.size() })),
		voxels_per_bohr(t<Voxel>(bolt::div(Num3 { 1, 1, 1 }, u(voxel_size))))
	{
	}

	void UpdateCells() {
		bolt::forEachPosition(cell_view, *this);
		bolt::fill(cell_view, 0);
		bolt::forEachPosition(atoms_view, *this);
	}

	BOLT_DECL_HYBRID
	void operator()(size_t& atom_index, const bolt::Int3& index) const {
		auto atom_pos = atoms_view[atom_index].position;
		for (signed char x = -1; x <= 1; x++) {
			for (signed char y = -1; y <= 1; y++) {
				for (signed char z = -1; z <= 1; z++) {
					auto atom_id = accessVoxel(t<IVoxel>(index) + t<IVoxel>(x, y, z));
					auto atom = atoms_view[atom_id];
					if (atom_index != atom_id) {
						auto dis = bolt::norm(atom_pos - atoms_view[atom_id].position);
						auto force = dis - AVERAGE_BOND_LENGTH_ANG * ANG_TO_BOHR;
						force = max(0.0, -force);
						atoms_view[atom_index].velocity += force * t<Bohr3>(bolt::normalize(u(atom_pos - atoms_view[atom_id].position)));
					}
				}
			}
		}

	}

	BOLT_DECL_HYBRID
	void operator()(EmsAtom& atom, const bolt::Int1& index) const {
		EmsAtom copy = atom;
		copy.position += copy.velocity;
		copy.velocity = Bohr3 { };
		auto voxel = worldToIVoxel(copy.position);
		cell_view[u(voxel)] = index;
		atom = copy;
	}

	BOLT_DECL_HYBRID
	Num access(const Bohr3& position) const {
		Num result = 0;
		auto voxel = worldToIVoxel(position);
		for (signed char x = -1; x <= 1; x++) {
			for (signed char y = -1; y <= 1; y++) {
				for (signed char z = -1; z <= 1; z++) {
					auto atom_id = accessVoxel(voxel + t<IVoxel>(x, y, z));
					auto atom = atoms_view[atom_id];
					result += potential_values.compute(atom.atomic_number, t<Bohr>(bolt::norm(atom.position - position)));
				}
			}
		}
		return result;
	}

	BOLT_DECL_HYBRID
	Num accessAndShift(const Bohr3& position, const Num3& shift) const {
		Num result = 0;
		auto voxel = worldToIVoxel(position);
		for (signed char x = -1; x <= 1; x++) {
			for (signed char y = -1; y <= 1; y++) {
				for (signed char z = -1; z <= 1; z++) {
					auto atom_id = accessVoxel(voxel + t<IVoxel>(x, y, z));
					auto atom = atoms_view[atom_id];
					result += potential_values.compute(atom.atomic_number, t<Bohr>(bolt::norm(atom.position - position)));
					auto atom_pos = atoms_view[atom_id].position;
					atoms_view[atom_id].velocity += t<Bohr3>(shift);
				}
			}
		}
		return result;
	}

	BOLT_DECL_HYBRID
	Voxel worldToVoxel(const Bohr3& world_position) const {
		return bolt::product(u(world_position - bounding_box.min()), voxels_per_bohr) - center_shift;
	}

	BOLT_DECL_HYBRID
	Bohr3 voxelToWorld(const Voxel& voxel_position) const {
		return bolt::product(u(voxel_position + center_shift), voxel_size) + bounding_box.min();
	}

	BOLT_DECL_HYBRID
	IVoxel worldToIVoxel(const Bohr3& world_position) const {
		auto voxel = worldToVoxel(world_position);
		auto coords = bolt::round(u(voxel));
		return t<IVoxel>(int(coords[0]), int(coords[1]), int(coords[2]));
	}

	BOLT_DECL_HYBRID
	bool voxelBoundingBox(const Bohr3& world_position, BoundingBox& voxel_bounding_box) const {
		auto voxel = worldToVoxel(world_position);
		auto coords = t<Voxel>(bolt::round(u(voxel)));
		auto voxel_start = coords - center_shift;
		auto min = voxel_start;
		auto min_w = voxelToWorld(min);
		auto max_w = min_w + voxel_size;
		voxel_bounding_box = BoundingBox { min_w, max_w };
		return isValidVoxelCoords(t<IVoxel>(u(coords)));
	}

	BOLT_DECL_HYBRID
	typename TCellView::AccessType accessVoxel(const IVoxel& voxel_coords) const {
		if (isValidVoxelCoords(voxel_coords)) {
			return cell_view[u(voxel_coords)];
		} else
			return cell_view[bolt::Int3 { 0, 0, 0 }];
	}

	BOLT_DECL_HYBRID
	typename TCellView::AccessType accessVoxel(const Bohr3& position) const {
		return accessVoxel(worldToIVoxel(position));
	}

	BOLT_DECL_HYBRID
	const BoundingBox& getBoundingBox() const {
		return bounding_box;
	}

	BOLT_DECL_HYBRID
	bool isValidVoxelCoords(const IVoxel& voxel_coords) const {
		return voxel_coords >= t<IVoxel>(0, 0, 0) && voxel_coords < t<IVoxel>(cell_view.size());
	}

	BOLT_DECL_HYBRID
	const Bohr3& getSizeOfVoxel() const {
		return voxel_size;
	}

	BOLT_DECL_HYBRID
	IVoxel getSizeInVoxels() const {
		return t<IVoxel>(cell_view.size());
	}

	BOLT_DECL_HYBRID
	const Voxel& getVoxelsPerBohr() const {
		return voxels_per_bohr;
	}

private:
	const Voxel center_shift = t<Voxel>(0.4999f, 0.4999f, 0.4999f);
	const TAtomsView atoms_view;
	const TCellView cell_view;
	const PotentialValues<TValuesView> potential_values;
	const BoundingBox bounding_box;
	const Bohr3 voxel_size;
	const Voxel voxels_per_bohr;
	const Bohr3 bohrs_per_voxel;
};

template<typename TCellView, typename TAtomsView, typename TValuesView>
PhysicalAtomicVolume<TCellView, TAtomsView, TValuesView> makeAtomicPhysicalVolume(
	TCellView cell_view,
	TAtomsView atoms_view,
	PotentialValues<TValuesView> potential_values,
	const ElectronMicroscopeSetup& setup)
{
	return {cell_view, atoms_view, potential_values, setup};
}

template<typename TPhysicalVolume, typename TShiftingView>
struct RutherfordAtomicShiftingAndScattering {

	explicit RutherfordAtomicShiftingAndScattering(
		const TPhysicalVolume scattering_volume,
		const TShiftingView shifting_view,
		const RutherfordFormulae& sampling_function,
		const Bohr& mean_free_path,
		const float& majorant) :
		scattering_volume(scattering_volume),
		shifting_view(shifting_view),
		sampling_function(sampling_function),
		mean_free_path(mean_free_path),
		majorant(majorant)
	{
	}

	template<typename TRandomGenerator, typename TPayload>
	BOLT_DECL_HYBRID
	Ray scatter(
		const Ray& orig_ray,
		TRandomGenerator& rng,
		Bohr& next_event_distance,
		TPayload& payload,
		Bohr max_distance = Bohr { 0 }) const
	{
		Num random_sample = rng();
		if (max_distance > Bohr { 0 }) {
			Num bound = Num { 1 } - exp(-max_distance / mean_free_path);
			random_sample *= bound;
		}

		next_event_distance = sampleNextEventDistance(random_sample);

		auto scattering_scattering_coefficient = scattering_volume.accessAndShift(orig_ray.origin, shifting_view[u(payload.origin)]);
		auto ratio = scattering_scattering_coefficient / majorant;
		if (rng() < ratio) {
			Num ruther_pdf { };
			auto new_dir = sampleDirection(orig_ray.dir, rng(), rng(), ruther_pdf);
			return Ray { orig_ray.origin, new_dir };
		} else {
			return orig_ray;
		}
	}

	BOLT_DECL_HYBRID
	Num3 sampleDirection(const Num3& dir, Num sample_one, Num sample_two, Num& pdf) const {
		auto sample_theta = sample_one;
		auto cos_theta = sampling_function.sampleCosTheta(sample_theta);
		pdf = sampling_function.pdf(cos_theta);

		float theta = acos(fmin(1.0f, fmax(-1.0f, cos_theta)));
		float psi = 2 * M_PI * sample_two;
		float sin_theta = sin(theta);

		float x = sin_theta * cos(psi);
		float y = sin_theta * sin(psi);
		float z = cos_theta;

		Num3 local_dir { x, y, z };
		Frame f;
		f.setFromZ(dir);
		auto new_dir = f.toWorld(local_dir);
		return new_dir;
	}

	BOLT_DECL_HYBRID
	Bohr sampleNextEventDistance(Num random_sample) const {
		return -mean_free_path * log(Num { 1.00001 } - random_sample);
	}

public:
	const RutherfordFormulae sampling_function;
	const float majorant;
	const Bohr mean_free_path;
	const TPhysicalVolume scattering_volume;
	const TShiftingView shifting_view;
};

struct OriginPayload {
	OriginPayload(const IPixel2& origin_pixel, size_t sample) : origin(origin_pixel) {
	}
	IPixel2 origin;
	Num weight { 1 };
};

struct CounterImageFormationAtomic {
	using Payload = OriginPayload;

	template<typename TProjectionView>
	BOLT_DECL_HYBRID
	void operator()(
		const TProjectionView& projection,
		const IPixel2& projection_pixel,
		const Payload& payload,
		const Ray& ray_deviation,
		const Bohr2& pixel_size) const
	{
#if defined(__CUDA_ARCH__)
		atomicAdd(&projection[u(projection_pixel)], payload.weight);
#else
		projection[u(projection_pixel)] += payload.weight;
#endif
	}
};

}

#endif /* SRC_EMS_ATOMIC_EMS_COMPONENTS_H_ */
