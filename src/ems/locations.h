/*
 * locations.h
 *
 *  Created on: Dec 17, 2019
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_LOCATIONS_H_
#define SRC_LOCATIONS_H_

#include "defines.h"

namespace ems {

//
//                                 |
//                                 |
//                                 |
//                                 | y
//                                 |
//                                 |
//                                 |
//                                 |
//                                 |
//                                 |
//                                 |
//                                 |
//                                 |
//                        ---------|---------/|
//                       /         |        / |
//                      /          |       /  |
//                     /           +------+   |
//                    /          / |     /|   |
//                   /          /  |    / |   |
//                  |----------+-------+--+------------------------------
//                  |          |  /    |  /   |                          x
//                  |          | /     | /    |
//                  |          |/      |/    /
//                  |          +-------+    /
//                  |         /        |   /
//                  |        /         |  /
//                  |       /          | /   volume
//                  |      /           |/
//                  ------/-------------
//                       /  /
//                      /  /
//                     /  / electron
//                  z /  /
//                   /  /
//|------------+----------|
//|            |   /  /   |
//|            |  /  /    |
//|            | /        |                                             -
//|            |/         |
//|            +----------+
//|           /           |
//|          /            |
//|         /             |
//|        /              | projection
//|-------/---------------|
//       /

//The data and pixels are now aligned if the size of the detector is the same as the corresponding size of the data.
// _._._._._._._   the data volume is extended by one voxel in the "min" direction, so that the intersection volume works properly
//  ._._._._._._
// if the size of the data is different, the first pixel is aligned but the last one not
// _._._._._._._
//  .__.__.__.__

struct ImageLocation {
	BOLT_DECL_HYBRID ImageLocation() {
	}

	BOLT_DECL_HYBRID ImageLocation(const Bohr3& origin, const Bohr3& vec_x, const Bohr3& vec_y) : origin(origin), vec_x(vec_x), vec_y(vec_y)
	{
		image_size = t<Bohr2>(bolt::norm(vec_x), bolt::norm(vec_y));
		normalized_vec_x = bolt::normalize(u(vec_x));
		normalized_vec_y = bolt::normalize(u(vec_y));

		normal_vec = bolt::normalize(bolt::cross(normalized_vec_x, normalized_vec_y));

		d = -(origin[0] * normal_vec[0] + origin[1] * normal_vec[1] + origin[2] * normal_vec[2]);
	}

	BOLT_DECL_HYBRID
	const Bohr2& size() const {
		return image_size;
	}

	BOLT_DECL_HYBRID
	const Bohr3& start() const {
		return origin;
	}

	BOLT_DECL_HYBRID
	Bohr3 end() const {
		return origin + vec_x + vec_y;
	}

	BOLT_DECL_HYBRID
	Bohr3 center() const {
		return origin + 0.5f * (vec_x + vec_y);
	}

	BOLT_DECL_HYBRID
	const Num3& xDir() const {
		return normalized_vec_x;
	}

	BOLT_DECL_HYBRID
	const Num3& yDir() const {
		return normalized_vec_y;
	}

	BOLT_DECL_HYBRID
	const Num3& normal() const {
		return normal_vec;
	}

	BOLT_DECL_HYBRID
	Bohr getPointDistance(const Bohr3& point) const {
		return abs(bolt::sum(bolt::product(normal_vec, point)) + d);
	}

private:
	Bohr3 origin { };
	Bohr3 vec_x { };
	Num3 normalized_vec_x { };
	Bohr3 vec_y { };
	Num3 normalized_vec_y { };
	Bohr2 image_size { };

	Num3 normal_vec { };
	Bohr d = Bohr { 0 }; //n1*x +n2*y +n3*z + d = 0;
};
}

#endif /* SRC_LOCATIONS_H_ */
