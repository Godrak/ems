/*
 * defines.h
 *
 *  Created on: Jan 30, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_DEFINES_H_
#define SRC_DEFINES_H_

#include "strong_type.h"

constexpr float BOHR_TO_ANG = 0.529177210903;
constexpr float ANG_TO_BOHR = 1.0f / BOHR_TO_ANG;
constexpr float EL_CHARGE = 1.0;
#define  EPSILON 1e-5f
#define INF 1.0f/0.0f

constexpr float AVERAGE_BOND_LENGTH_ANG = 1.54;

namespace ems {

//tags
struct tag_bohr {
};
struct tag_angstrom {
};
struct tag_radian {
};
struct tag_world {
};
struct tag_pixel {
};
struct tag_ipixel {
};

using com_t = float;

using Bohr = StrongType<com_t, tag_bohr>;
using Bohr2 = bolt::Vector<Bohr, 2>;
using Bohr3 = bolt::Vector<Bohr, 3>;
using Angstrom = StrongType<com_t, tag_angstrom>;
using Radian = StrongType<com_t, tag_radian>;
using Pixel = StrongType<com_t, tag_pixel>;
using Pixel2 = bolt::Vector<Pixel, 2>;
using Pixel3 = bolt::Vector<Pixel, 3>;
using Voxel = bolt::Vector<Pixel, 3>;
using IPixel = StrongType<int, tag_ipixel>;
using IPixel2 = bolt::Vector<IPixel, 2>;
using IPixel3 = bolt::Vector<IPixel, 3>;
using IVoxel = bolt::Vector<IPixel, 3>;

using Num = com_t;
using Num2 = bolt::Vector<Num,2>;
using Num3 = bolt::Vector<Num,3>;

template<typename T>
BOLT_DECL_HYBRID
T angToBohr(T ang) {
	return ang * ANG_TO_BOHR;
}

template<typename T>
BOLT_DECL_HYBRID
T bohrToAng(T bohr) {
	using namespace bolt;
	return bohr * BOHR_TO_ANG;
}

inline Bohr toBohr(const Angstrom& ang) {
	return Bohr { ang.get() * ANG_TO_BOHR };
}

inline Angstrom toAng(const Bohr& bohr) {
	return Angstrom { bohr.get() * BOHR_TO_ANG };
}

BOLT_DECL_HYBRID
inline Num energyToMomentum(const Num& electron_energy_keV) {
//	1 Ha = 0.036749308137 eV
//  p = sqrt(2E)
	return sqrt(0.036749308137 * 2 * 1000 * electron_energy_keV);
}

template<int S, typename T>
std::string toString(bolt::Vector<T, S> v) {
	std::string res;
	for (size_t x = 0; x < S; ++x) {
		res += std::to_string(v[x]);
		res += "_";
	}
	return res;
}

#ifdef __CUDA_ARCH__
#define LOG(M) ;
#else
#define LOG(M) std::cout << M << std::endl;
#endif

}

#endif /* SRC_DEFINES_H_ */
