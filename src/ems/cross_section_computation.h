/*
 * cross_section_computation.h
 *
 *  Created on: Oct 16, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_CROSS_SECTION_COMPUTATION_H_
#define SRC_EMS_CROSS_SECTION_COMPUTATION_H_

#include "localized_fourier_potential.h"
#include "random_generators_view.h"
#include "params.h"

namespace ems {

template<typename TView, typename TGeneratorsView>
class LocalCrossSectionFunctor {
public:
	LocalCrossSectionFunctor(
		const PhysicalVolume<TView>& volume,
		const TGeneratorsView& generators,
		const RutherfordFormulae& sampling_function,
		const json_params::JsonParamsHandler& params) :
		volume(volume),
		generators(generators),
		sampling_function(sampling_function),
		electron_momentum(energyToMomentum(params.get<ems_ElectronEnergy>())),
		cross_section_frame_size(params.get<ems_LocalFTFrameRadius>()),
		max_fourier_samples_per_axis(params.get<ems_LocalFTSamplesPerAxis>()),
		local_cross_section_sample_count(params.get<ems_LocalCrossSectionSampleCount>()),
		limit_angle(
			computeLimitingAngleOfLocalizedFP(
				energyToMomentum(params.get<ems_ElectronEnergy>()),
				params.get<ems_LocalFTFrameRadius>(),
				params.get<ems_LocalFTSamplesPerAxis>())),
		sampling_boundary(sampling_function.cumulativeDistributionFunction(cos(limit_angle))),
		pdf_normalization(Num { 1.0 } / (Num { 1 } - sampling_boundary))
	{
	}

	BOLT_DECL_HYBRID
	void operator()(float& val, const bolt::Int3& index) const {
		auto position = volume.voxelToWorld(t<Voxel>(index));
		auto localized_fp = ems::makeLocalizedFourierTransform(volume, position, cross_section_frame_size, max_fourier_samples_per_axis);
		size_t count;
		if (localized_fp.at(Num3 { 0, 0, 0 }, count).x == 0) {
			val = 0;
		} else {
			val = estimateIntegral(localized_fp, generators[index]);
		}
	}

	template<typename LOCALIZED_FP, typename TRandomGenerator>
	BOLT_DECL_HYBRID
	Num estimateIntegral(const LOCALIZED_FP& localized_fp, TRandomGenerator& gen) const {
		Num pdf;
		Num sum = 0;
		for (size_t sample = 0; sample < local_cross_section_sample_count; ++sample) {
			auto sample_one = gen();
			auto sample_two = gen();

			auto dir = sampleDirection(direction, sample_one, sample_two, pdf);
			auto sample_value = computeSampleValue(localized_fp, dir);
			sum += sample_value / pdf;
		}
		return sum / local_cross_section_sample_count;
	}

	BOLT_DECL_HYBRID
	Num3 sampleDirection(const Num3& dir, Num sample_one, Num sample_two, Num& pdf) const {
		auto sample_theta = sampling_boundary + sample_one * (1 - sampling_boundary);
		auto cos_theta = sampling_function.sampleCosTheta(sample_theta);
		pdf = sampling_function.pdf(cos_theta);
		pdf = pdf * pdf_normalization;

		float theta = acos(fmin(1.0f, fmax(-1.0f, cos_theta)));
		float psi = 2 * M_PI * sample_two;
		float sin_theta = sin(theta);

		float x = sin_theta * cos(psi);
		float y = sin_theta * sin(psi);
		float z = cos_theta;

		Num3 local_dir { x, y, z };
		Frame f;
		f.setFromZ(dir);
		auto new_dir = f.toWorld(local_dir);
		return new_dir;
	}

	template<typename LOCALIZED_FP>
	BOLT_DECL_HYBRID
	float computeSampleValue(const LOCALIZED_FP& localized_fp, const Num3& sampled_dir) const {
		auto momentum_diff = bolt::product(direction - sampled_dir, Num3::fill(electron_momentum));
		size_t samples_count;
		auto fourier_value = localized_fp.at(momentum_diff, samples_count);

		auto square_norm = fourier_value.x * fourier_value.x + fourier_value.y * fourier_value.y;
		float sample_value = square_norm * M_1_PI * M_1_PI / 4.0;
		sample_value = sample_value / samples_count;

		return sample_value;
	}

	const Num sampling_boundary;
	const Num pdf_normalization;
	const RutherfordFormulae sampling_function;
	const Num limit_angle;

	const Num3 direction { 0, 0, 1 };

	const PhysicalVolume<TView> volume;
	const float electron_momentum;
	const Bohr cross_section_frame_size;
	const size_t max_fourier_samples_per_axis;
	const size_t local_cross_section_sample_count;
	const TGeneratorsView generators;
};

template<typename TView, typename TCrossSectionsView>
void precomputeLocalCrossSections(
	const PhysicalVolume<TView>& volume,
	TCrossSectionsView cross_sections_view,
	const json_params::JsonParamsHandler& params)
{
	auto generators = ems::createRandomGeneratorsImage<TView::kIsDeviceView>(u(volume.getSizeInVoxels()), params.get<ems_RandomSeed>());
	auto generators_view = generators.view();
	PeriodicTable p { };
	LocalCrossSectionFunctor<TView, decltype(generators_view)> cross_section_functor { volume, generators_view, RutherfordFormulae { p.get(
		params.get<ems_RutherSamplingAtomicNumber>()), params.get<ems_ElectronEnergy>() }, params };
	bolt::forEachPosition(cross_sections_view, cross_section_functor);
}

}

#endif /* SRC_EMS_CROSS_SECTION_COMPUTATION_H_ */
