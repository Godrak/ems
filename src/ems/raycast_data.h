/*
 * raycast_data.h
 *
 *  Created on: Feb 4, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_RAYCAST_DATA_H_
#define SRC_RAYCAST_DATA_H_

#include "math_utils.h"
#include "ems_setup.h"

namespace ems {

struct Detector {
	BOLT_DECL_HYBRID Detector(ImageLocation location, const IPixel2& size) :
		location(location),
		size(size),
		pixel_size(bolt::div(location.size(), Num2 { u(size) }))
	{
	}

	BOLT_DECL_HYBRID
	Bohr3 getPixelCenterLocation(const IPixel2& index) const {
		Pixel2 pixel_center = t<Pixel2>(1.0f * u(index)) + t<Pixel2>(0.4999f, 0.4999f);
		Bohr2 plane_position = bolt::product(u(pixel_center), pixel_size);
		return location.start() + location.xDir() * plane_position[0] + location.yDir() * plane_position[1];
	}

	BOLT_DECL_HYBRID
	Bohr3 getInPixelLocation(const IPixel2 index, const Pixel2& pixel_offset) const {
		//pixel offset values are in pixel units and should be in range -0.5, 0.5
		Pixel2 pixel_center = t<Pixel2>(1.0f * u(index)) + t<Pixel2>(0.4999f, 0.4999f) + pixel_offset;
		Bohr2 plane_position = bolt::product(u(pixel_center), pixel_size);
		return location.start() + location.xDir() * (plane_position[0]) + location.yDir() * (plane_position[1]);
	}

	/**
	 * returns whether does the ray intersect any valid pixel of the detector. The pixel is returned in the pixel value, if such pixel exists. The distance
	 * is set to the distance of the detector plane, regardless of the pixel validity. Distance is inf if ray parallel to the detector plane.
	 */
	BOLT_DECL_HYBRID
	bool getIntersectedPixel(const Ray& ray, IPixel2& pixel, Bohr& distance) const {
		if (intersection(ray, location, distance)) {
			auto position = ray.origin + ray.dir * distance;
			auto x = t<Bohr>(bolt::dot(u(position - location.start()), location.xDir()));
			auto y = t<Bohr>(bolt::dot(u(position - location.start()), location.yDir()));

			auto i = int(floor(x / pixel_size[0]));
			auto j = int(floor(y / pixel_size[1]));

			pixel = t<IPixel2>(i, j);
			return (pixel >= t<IPixel2>(0, 0) && pixel < size);
		} else {
			return false;
		}
	}

	const ImageLocation location;
	const IPixel2 size;
	const Bohr2 pixel_size;
};

struct Emitter {
	BOLT_DECL_HYBRID Emitter(
		ImageLocation location,
		const IPixel2& size,
		size_t samples_per_pixel,
		const int& randomized_electron_position,
		const Num3& init_ray_direction) :
		location(location),
		size(size),
		samples_per_pixel(samples_per_pixel),
		randomized_electron_position(randomized_electron_position),
		init_ray_direction(init_ray_direction),
		pixel_size(bolt::div(location.size(), Num2 { u(size) }))
	{
	}

	BOLT_DECL_HYBRID
	Bohr3 getPixelCenterLocation(const IPixel2& index) const {
		Pixel2 pixel_center = t<Pixel2>(1.0f * u(index)) + t<Pixel2>(0.4999f, 0.4999f);
		Bohr2 plane_position = bolt::product(u(pixel_center), pixel_size);
		return location.start() + location.xDir() * plane_position[0] + location.yDir() * plane_position[1];
	}

	BOLT_DECL_HYBRID
	Bohr3 getSourceLocation(const IPixel2 index, const Num& random_sample1, const Num& random_sample2) const {
		Pixel2 pixel = t<Pixel2>(1.0f * u(index));
		Pixel2 pixel_offset = +t<Pixel2>(0.4999f, 0.4999f);
		if (randomized_electron_position == 1) {
			pixel_offset = t<Pixel2>(random_sample1, random_sample2);
		}
		Bohr2 plane_position = bolt::product(u(pixel + pixel_offset), pixel_size);
		if (randomized_electron_position == 2) {
			plane_position = bolt::product(Num2 { random_sample1, random_sample2 }, location.size());
		}
		return location.start() + location.xDir() * plane_position[0] + location.yDir() * plane_position[1];
	}

	const ImageLocation location;
	const IPixel2 size;
	const Bohr2 pixel_size;
	const Num3 init_ray_direction;
	const size_t samples_per_pixel;
	const int randomized_electron_position;
};

template<typename TVolumeView>
class PhysicalVolume {
public:

	BOLT_DECL_HYBRID PhysicalVolume(TVolumeView interaction_volume, const ElectronMicroscopeSetup& setup) :
		volume_view(interaction_volume),
		bounding_box(setup.interaction_bounding_box),
		bohrs_per_voxel(setup.bohr_per_voxel, setup.bohr_per_voxel, setup.bohr_per_voxel),
		voxel_size(bolt::div(bounding_box.size(), Num3 { interaction_volume.size() })),
		voxels_per_bohr(t<Voxel>(bolt::div(Num3 { 1, 1, 1 }, u(voxel_size))))
	{
	}

	template<typename T = TVolumeView, typename std::enable_if_t<bolt::IsInterpolatedView<T>::value, bool> = true>
	BOLT_DECL_HYBRID
	typename T::AccessType access(const Bohr3& position) const
	{
		return volume_view.access(u(worldToVoxel(position)));
	}

	template<typename T = TVolumeView, typename std::enable_if_t<bolt::IsInterpolatedView<T>::value, bool> = true>
	BOLT_DECL_HYBRID
	typename T::AccessType access(const Voxel& position) const
	{
		return volume_view.access(u(position));
	}

	BOLT_DECL_HYBRID
	Voxel worldToVoxel(const Bohr3& world_position) const {
		return bolt::product(u(world_position - bounding_box.min()), voxels_per_bohr) - center_shift;
	}

	BOLT_DECL_HYBRID
	Bohr3 voxelToWorld(const Voxel& voxel_position) const {
		return bolt::product(u(voxel_position + center_shift), voxel_size) + bounding_box.min();
	}

	BOLT_DECL_HYBRID
	IVoxel worldToIVoxel(const Bohr3& world_position) const {
		auto voxel = worldToVoxel(world_position);
		auto coords = t<Voxel>(bolt::round(u(voxel)));
		return t<IVoxel>(int(coords[0]), int(coords[1]), int(coords[2]));
	}

	BOLT_DECL_HYBRID
	bool voxelBoundingBox(const Bohr3& world_position, BoundingBox& voxel_bounding_box) const {
		auto voxel = worldToVoxel(world_position);
		auto coords = t<Voxel>(bolt::round(u(voxel)));
		auto voxel_start = coords - center_shift;
		auto min = voxel_start;
		auto min_w = voxelToWorld(min);
		auto max_w = min_w + voxel_size;
		voxel_bounding_box = BoundingBox { min_w, max_w };
		return isValidVoxelCoords(t<IVoxel>(u(coords)));
	}

	BOLT_DECL_HYBRID
	typename TVolumeView::AccessType accessVoxel(const IVoxel& voxel_coords) const {
		return volume_view[u(voxel_coords)];
	}

	BOLT_DECL_HYBRID
	typename TVolumeView::AccessType accessVoxel(const Bohr3& position) const {
		return accessVoxel(worldToIVoxel(position));
	}

	BOLT_DECL_HYBRID
	const BoundingBox& getBoundingBox() const {
		return bounding_box;
	}

	BOLT_DECL_HYBRID
	bool isValidVoxelCoords(const IVoxel& voxel_coords) const {
		return voxel_coords >= t<IVoxel>(0, 0, 0) && voxel_coords < t<IVoxel>(volume_view.size());
	}

	BOLT_DECL_HYBRID
	const Bohr3& getSizeOfVoxel() const {
		return voxel_size;
	}

	BOLT_DECL_HYBRID
	IVoxel getSizeInVoxels() const {
		return t<IVoxel>(volume_view.size());
	}

	BOLT_DECL_HYBRID
	const Voxel& getVoxelsPerBohr() const {
		return voxels_per_bohr;
	}

private:
	const Voxel center_shift = t<Voxel>(0.4999f, 0.4999f, 0.4999f);
	const TVolumeView volume_view;
	const BoundingBox bounding_box;
	const Bohr3 voxel_size;
	const Voxel voxels_per_bohr;
	const Bohr3 bohrs_per_voxel;
};

template<typename TVolumeView>
PhysicalVolume<TVolumeView> makePhysicalVolume(TVolumeView view, const ElectronMicroscopeSetup& setup) {
	return {view, setup};
}

}

#endif /* SRC_RAYCAST_DATA_H_ */
