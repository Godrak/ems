/*
 * scattering_function.h
 *
 *  Created on: Jan 29, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_SCATTERING_FUNCTION_H_
#define SRC_SCATTERING_FUNCTION_H_

#include "raycast_data.h"
#include "localized_fourier_potential.h"
#include  "screened_rutherford_formulae.h"

#include "params.h"
#include "array"

namespace ems {

struct NoScattering {
	explicit NoScattering() {
	}

	template<typename TRandomGenerator, typename TPayload>
	BOLT_DECL_HYBRID
	Ray scatter(
		const Ray& orig_ray,
		TRandomGenerator& rng,
		Bohr& next_event_distance,
		TPayload& payload,
		Bohr max_distance = Bohr { 0 }) const
	{
		next_event_distance = Bohr { INF };
		return orig_ray;
	}
};

struct FixedStepScattering {
	explicit FixedStepScattering(const Bohr& next_event_distance) : default_next_event_distance(next_event_distance) {
	}

	template<typename TRandomGenerator, typename TPayload>
	BOLT_DECL_HYBRID
	Ray scatter(
		const Ray& orig_ray,
		TRandomGenerator& rng,
		Bohr& next_event_distance,
		TPayload& payload,
		Bohr max_distance = Bohr { 0 }) const
	{
		next_event_distance = default_next_event_distance;
		return orig_ray;
	}

	Bohr default_next_event_distance;
};

template<typename TVolumeView>
struct HenyeyGreensteinScattering {

	explicit HenyeyGreensteinScattering(
		const PhysicalVolume<TVolumeView> scattering_volume,
		const json_params::JsonParamsHandler& params,
		const Bohr& mean_free_path,
		const float& majorant) :
		scattering_volume(scattering_volume),
		heyney_greenstein_g(params.get<ems_hg_GreensteinG>()),
		mean_free_path(mean_free_path),
		majorant(majorant)
	{
	}

	template<typename TRandomGenerator, typename TPayload>
	BOLT_DECL_HYBRID
	Ray scatter(
		const Ray& orig_ray,
		TRandomGenerator& rng,
		Bohr& next_event_distance,
		TPayload& payload,
		Bohr max_distance = Bohr { 0 }) const
	{
		Num random_sample = rng();
		if (max_distance > Bohr { 0 }) {
			Num bound = Num { 1 } - exp(-max_distance / mean_free_path);
			random_sample *= bound;
		}

		next_event_distance = sampleNextEventDistance(random_sample);

		auto actual_cross_section = scattering_volume.access(orig_ray.origin);
		if (rng() <= actual_cross_section / majorant) {
			auto new_dir = hgScatter(orig_ray.dir, rng(), rng());
			return Ray { orig_ray.origin, new_dir };
		} else {
			return orig_ray;
		}
	}

private:
	BOLT_DECL_HYBRID
	Num3 hgScatter(const Num3& dir, float uniform_sample1, float uniform_sample2) const
	{
		auto local_dir = SampleHeyneyGreenstein(heyney_greenstein_g, uniform_sample1, uniform_sample2, nullptr);

		Frame f;
		f.setFromZ(dir);
		auto new_dir = f.toWorld(local_dir);
		return new_dir;
	}

	BOLT_DECL_HYBRID
	Bohr sampleNextEventDistance(Num random_sample) const
	{
		return -mean_free_path * log(1.0001f - random_sample);
	}

public:
	const Num heyney_greenstein_g;
	const Num majorant;
	const Bohr mean_free_path;
	const PhysicalVolume<TVolumeView> scattering_volume;
};

template<typename RutherfordsView>
void fillRutherfordsView(const RutherfordsView& view, const PeriodicTable& table, float electron_energy_keV) {
	bolt::HostImage<RutherfordFormulae, 1> tmp { view.size() };
	auto tmp_view = tmp.view();
	for (size_t atomic = 0; atomic < view.size(); ++atomic) {
		tmp_view[atomic] = RutherfordFormulae { table.get(atomic + 1), electron_energy_keV };
	}
	bolt::copy(tmp_view, view);
}

template<typename TVolumeView, typename RutherfordsView>
struct RutherfordProportionalScattering {

	explicit RutherfordProportionalScattering(
		const PhysicalVolume<TVolumeView> scattering_volume,
		const RutherfordsView rutherfords,
		const float& majorant) :
		scattering_volume(scattering_volume),
		majorant(majorant),
		rutherfords(rutherfords),
		majorant_atomic_number(rutherfords.size())
	{
	}

	template<typename TRandomGenerator, typename TPayload>
	BOLT_DECL_HYBRID
	Ray scatter(
		const Ray& orig_ray,
		TRandomGenerator& rng,
		Bohr& next_event_distance,
		TPayload& payload,
		Bohr max_distance = Bohr { 0 }) const
	{
		Num random_sample = rng();
		if (max_distance > Bohr { 0 }) {
			Num bound = Num { 1 } - exp(-max_distance / rutherfords[majorant_atomic_number - 1].meanFreePath());
			random_sample *= bound;
		}

		next_event_distance = sampleNextEventDistance(random_sample);

		auto actual_scattering_coefficient = scattering_volume.access(orig_ray.origin);
		auto ratio = actual_scattering_coefficient / majorant;
		if (rng() < ratio) {
			size_t atom_estimation = min(size_t(ceil(ratio * majorant_atomic_number)), majorant_atomic_number);
			Num pdf;
			auto new_dir = rutherfordScatter(rutherfords[atom_estimation - 1], orig_ray.dir, rng(), rng(), pdf);
			return Ray { orig_ray.origin, new_dir };
		} else {
			return orig_ray;
		}
	}

public:
	BOLT_DECL_HYBRID
	Num3 rutherfordScatter(
		const RutherfordFormulae& rutherford,
		const Num3& dir,
		float uniform_sample1,
		float uniform_sample2,
		Num& pdf) const
	{
		auto cos_theta = rutherford.sampleCosTheta(uniform_sample1);
		pdf = rutherford.pdf(cos_theta);

		float theta = acos(fmin(1.0f, fmax(-1.0f, cos_theta)));
		float psi = 2 * M_PI * uniform_sample2;
		float sin_theta = sin(theta);

		float x = sin_theta * cos(psi);
		float y = sin_theta * sin(psi);
		float z = cos_theta;

		Num3 local_dir { x, y, z };
		Frame f;
		f.setFromZ(dir);
		auto new_dir = f.toWorld(local_dir);
		return new_dir;
	}

	BOLT_DECL_HYBRID
	Bohr sampleNextEventDistance(Num random_sample) const
	{
		return -rutherfords[majorant_atomic_number - 1].meanFreePath() * log(Num { 1.00001 } - random_sample);
	}

public:
	const size_t majorant_atomic_number;
	const RutherfordsView rutherfords; //shift expected, so that hydrogen is on index 0
	const PhysicalVolume<TVolumeView> scattering_volume;
	const Num majorant;
};


template<typename TVolumeView>
struct RutherfordSimpleScattering {

	explicit RutherfordSimpleScattering(
		const PhysicalVolume<TVolumeView> scattering_volume,
		const RutherfordFormulae& sampling_function,
		const Bohr& mean_free_path,
		const float& majorant) :
		scattering_volume(scattering_volume),
		sampling_function(sampling_function),
		mean_free_path(mean_free_path),
		majorant(majorant)
	{
	}

	template<typename TRandomGenerator, typename TPayload>
	BOLT_DECL_HYBRID
	Ray scatter(
		const Ray& orig_ray,
		TRandomGenerator& rng,
		Bohr& next_event_distance,
		TPayload& payload,
		Bohr max_distance = Bohr { 0 }) const
	{
		Num random_sample = rng();
		if (max_distance > Bohr { 0 }) {
			Num bound = Num { 1 } - exp(-max_distance / mean_free_path);
			random_sample *= bound;
		}

		next_event_distance = sampleNextEventDistance(random_sample);

		auto scattering_scattering_coefficient = scattering_volume.access(orig_ray.origin);
		auto ratio = scattering_scattering_coefficient / majorant;
		if (rng() < ratio) {
			Num ruther_pdf { };
			auto new_dir = sampleDirection(orig_ray.dir, rng(), rng(), ruther_pdf);
			return Ray { orig_ray.origin, new_dir };
		} else {
			return orig_ray;
		}
	}

	BOLT_DECL_HYBRID
	Num3 sampleDirection(const Num3& dir, Num sample_one, Num sample_two, Num& pdf) const
	{
		auto sample_theta = sample_one;
		auto cos_theta = sampling_function.sampleCosTheta(sample_theta);
		pdf = sampling_function.pdf(cos_theta);

		float theta = acos(fmin(1.0f, fmax(-1.0f, cos_theta)));
		float psi = 2 * M_PI * sample_two;
		float sin_theta = sin(theta);

		float x = sin_theta * cos(psi);
		float y = sin_theta * sin(psi);
		float z = cos_theta;

		Num3 local_dir { x, y, z };
		Frame f;
		f.setFromZ(dir);
		auto new_dir = f.toWorld(local_dir);
		return new_dir;
	}

	BOLT_DECL_HYBRID
	Bohr sampleNextEventDistance(Num random_sample) const
	{
		return -mean_free_path * log(Num { 1.00001 } - random_sample);
	}

public:
	const RutherfordFormulae sampling_function;
	const float majorant;
	const Bohr mean_free_path;
	const PhysicalVolume<TVolumeView> scattering_volume;
};

template<typename TVolumeView>
struct DcsScatteringUniformSampling {

	explicit DcsScatteringUniformSampling(
		const PhysicalVolume<TVolumeView> potential_volume,
		const PhysicalVolume<TVolumeView> total_cs_volume,
		const PhysicalVolume<TVolumeView> scattering_volume,
		const json_params::JsonParamsHandler& params,
		const Bohr& mean_free_path,
		const float& majorant) :
		potential_volume(potential_volume),
		total_cs_volume(total_cs_volume),
		scattering_volume(scattering_volume),
		mean_free_path(mean_free_path),
		majorant(majorant),
		electron_momentum(energyToMomentum(params.get<ems_ElectronEnergy>())),
		cross_section_frame_size(params.get<ems_LocalFTFrameRadius>()),
		max_fourier_samples_per_axis(params.get<ems_LocalFTSamplesPerAxis>()),
		limit_angle(
			computeLimitingAngleOfLocalizedFP(
				energyToMomentum(params.get<ems_ElectronEnergy>()),
				params.get<ems_LocalFTFrameRadius>(),
				params.get<ems_LocalFTSamplesPerAxis>()))
	{
		printf("LIMIT ANGLE: %f \n", limit_angle);
	}

	template<typename TRandomGenerator, typename TPayload>
	BOLT_DECL_HYBRID
	Ray scatter(
		const Ray& orig_ray,
		TRandomGenerator& rng,
		Bohr& next_event_distance,
		TPayload& payload,
		Bohr max_distance = Bohr { 0 }) const
	{
		Num random_sample = rng();
		if (max_distance > Bohr { 0 }) {
			Num bound = Num { 1 } - exp(-max_distance / mean_free_path);
			random_sample *= bound;
		}

		next_event_distance = sampleNextEventDistance(random_sample);

		auto scattering_scattering_coefficient = scattering_volume.access(orig_ray.origin);
		auto ratio = scattering_scattering_coefficient / majorant;
		if (rng() < ratio) {
			auto local_dir = sampleS2CapUniform(rng(), rng(), limit_angle);
			Frame f;
			f.setFromZ(orig_ray.dir);
			auto new_dir = f.toWorld(local_dir);
			auto electron_pdf = getPdfForDir(orig_ray, new_dir, total_cs_volume.access(orig_ray.origin));
			payload.weight *= electron_pdf * areaS2Cap(limit_angle);
			return Ray { orig_ray.origin, new_dir };
		} else {
			return orig_ray;
		}
	}

	BOLT_DECL_HYBRID
	Num getPdfForDir(const Ray& orig_ray, const Num3& new_dir, Num total_cross_section) const
	{
		auto localized_fp = ems::makeLocalizedFourierTransform(
			potential_volume,
			orig_ray.origin,
			cross_section_frame_size,
			max_fourier_samples_per_axis);

		auto value = computeSampleValue(localized_fp, orig_ray.dir, new_dir);
		return value / total_cross_section;
	}

	template<typename LocalizedFourierPotential>
	BOLT_DECL_HYBRID
	float computeSampleValue(const LocalizedFourierPotential& localized_fp, const Num3& orig_dir, const Num3& sampled_dir) const
	{
		auto momentum_diff = bolt::product(orig_dir - sampled_dir, Num3::fill(electron_momentum));
		size_t samples_count;
		auto fourier_value = localized_fp.at(momentum_diff, samples_count);

		auto square_norm = fourier_value.x * fourier_value.x + fourier_value.y * fourier_value.y;
		float sample_value = square_norm * M_1_PI * M_1_PI / 4.0;
		sample_value = sample_value / samples_count;

		return sample_value;
	}

	BOLT_DECL_HYBRID
	Bohr sampleNextEventDistance(Num random_sample) const
	{
		return -mean_free_path * log(Num { 1.00001 } - random_sample);
	}

public:
	const float electron_momentum;
	const float majorant;
	const float limit_angle;
	const Bohr mean_free_path;
	const Bohr cross_section_frame_size;
	const size_t max_fourier_samples_per_axis;
	const PhysicalVolume<TVolumeView> potential_volume;
	const PhysicalVolume<TVolumeView> total_cs_volume;
	const PhysicalVolume<TVolumeView> scattering_volume;
};

template<typename TVolumeView>
struct DcsScatteringRutherSampling {

	explicit DcsScatteringRutherSampling(
		const PhysicalVolume<TVolumeView> potential_volume,
		const PhysicalVolume<TVolumeView> total_cs_volume,
		const PhysicalVolume<TVolumeView> scattering_volume,
		const json_params::JsonParamsHandler& params,
		const RutherfordFormulae& sampling_function,
		const Bohr& mean_free_path,
		const float& majorant) :
		potential_volume(potential_volume),
		total_cs_volume(total_cs_volume),
		scattering_volume(scattering_volume),
		sampling_function(sampling_function),
		mean_free_path(mean_free_path),
		majorant(majorant),
		electron_momentum(energyToMomentum(params.get<ems_ElectronEnergy>())),
		cross_section_frame_size(params.get<ems_LocalFTFrameRadius>()),
		max_fourier_samples_per_axis(params.get<ems_LocalFTSamplesPerAxis>()),
		limit_angle(
			computeLimitingAngleOfLocalizedFP(
				energyToMomentum(params.get<ems_ElectronEnergy>()),
				params.get<ems_LocalFTFrameRadius>(),
				params.get<ems_LocalFTSamplesPerAxis>())),
		sampling_boundary(sampling_function.cumulativeDistributionFunction(cos(limit_angle))),
		pdf_normalization(Num { 1.0 } / (Num { 1 } - sampling_boundary))
	{
		printf("LIMIT ANGLE: %f \n  sampling boundary: %f \n", limit_angle, sampling_boundary);
	}

	template<typename TRandomGenerator, typename TPayload>
	BOLT_DECL_HYBRID
	Ray scatter(
		const Ray& orig_ray,
		TRandomGenerator& rng,
		Bohr& next_event_distance,
		TPayload& payload,
		Bohr max_distance = Bohr { 0 }) const
	{
		Num random_sample = rng();
		if (max_distance > Bohr { 0 }) {
			Num bound = Num { 1 } - exp(-max_distance / mean_free_path);
			random_sample *= bound;
		}

		next_event_distance = sampleNextEventDistance(random_sample);

		auto scattering_cross_section = scattering_volume.access(orig_ray.origin);
		auto ratio = scattering_cross_section / majorant;
		if (rng() < ratio) {
			Num ruther_pdf { };
			auto new_dir = sampleDirection(orig_ray.dir, rng(), rng(), ruther_pdf);
			auto electron_pdf = getPdfForDir(orig_ray, new_dir, total_cs_volume.access(orig_ray.origin));
			payload.weight *= electron_pdf / ruther_pdf;
			return Ray { orig_ray.origin, new_dir };
		} else {
			return orig_ray;
		}
	}

	BOLT_DECL_HYBRID
	Num3 sampleDirection(const Num3& dir, Num sample_one, Num sample_two, Num& pdf) const
	{
		auto sample_theta = sampling_boundary + sample_one * (1 - sampling_boundary);
		auto cos_theta = sampling_function.sampleCosTheta(sample_theta);
		pdf = sampling_function.pdf(cos_theta);
		pdf = pdf * pdf_normalization;

		float theta = acos(fmin(1.0f, fmax(-1.0f, cos_theta)));
		float psi = 2 * M_PI * sample_two;
		float sin_theta = sin(theta);

		float x = sin_theta * cos(psi);
		float y = sin_theta * sin(psi);
		float z = cos_theta;

		Num3 local_dir { x, y, z };
		Frame f;
		f.setFromZ(dir);
		auto new_dir = f.toWorld(local_dir);
		return new_dir;
	}

	BOLT_DECL_HYBRID
	Num getPdfForDir(const Ray& orig_ray, const Num3& new_dir, Num total_cross_section) const
	{
		auto localized_fp = ems::makeLocalizedFourierTransform(
			potential_volume,
			orig_ray.origin,
			cross_section_frame_size,
			max_fourier_samples_per_axis);

		auto value = computeSampleValue(localized_fp, orig_ray.dir, new_dir);
		return value / total_cross_section;
	}

	template<typename LocalizedFourierPotential>
	BOLT_DECL_HYBRID
	float computeSampleValue(const LocalizedFourierPotential& localized_fp, const Num3& orig_dir, const Num3& sampled_dir) const
	{
		auto momentum_diff = bolt::product(orig_dir - sampled_dir, Num3::fill(electron_momentum));
		size_t samples_count;
		auto fourier_value = localized_fp.at(momentum_diff, samples_count);

		auto square_norm = fourier_value.x * fourier_value.x + fourier_value.y * fourier_value.y;
		float sample_value = square_norm * M_1_PI * M_1_PI / 4.0;
		sample_value = sample_value / samples_count;

		return sample_value;
	}

	BOLT_DECL_HYBRID
	Bohr sampleNextEventDistance(Num random_sample) const
	{
		return -mean_free_path * log(Num { 1.00001 } - random_sample);
	}
public:
	const Num sampling_boundary;
	const Num pdf_normalization;

public:
	const RutherfordFormulae sampling_function;
	const float electron_momentum;
	const float majorant;
	const float limit_angle;
	const Bohr mean_free_path;
	const Bohr cross_section_frame_size;
	const size_t max_fourier_samples_per_axis;
	const PhysicalVolume<TVolumeView> potential_volume;
	const PhysicalVolume<TVolumeView> total_cs_volume;
	const PhysicalVolume<TVolumeView> scattering_volume;
};

}

#endif /* SRC_SCATTERING_FUNCTION_H_ */
