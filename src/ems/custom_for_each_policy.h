/*
 * custom_for_each_policy.h
 *
 *  Created on: Oct 1, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_CUSTOM_FOR_EACH_POLICY_H_
#define SRC_EMS_CUSTOM_FOR_EACH_POLICY_H_

#include "boltview/for_each.h"
#include "boltview/procedural_views.h"

namespace ems {
#if defined(__CUDACC__)
template<int tDimension>
BOLT_DECL_HYBRID
dim3 customBlockDimForDimension();

template<>
BOLT_DECL_HYBRID
inline dim3 customBlockDimForDimension<2>()
{
	return dim3(32, 8, 1);
}

template<typename TView>
struct CustomForEachPolicyBase {
#if defined(__CUDACC__)
	static BOLT_DECL_HYBRID dim3 blockSize() {
		return customBlockDimForDimension<TView::kDimension>();
	}

	static BOLT_DECL_HYBRID dim3 gridSize(const TView& view) {
		return bolt::detail::defaultGridSizeForBlockDim<TView::kDimension>(dataSize(view), blockSize());
	}
#endif	// __CUDACC__

};

template<typename TView>
using CustomForEachPolicy = bolt::DefaultForEachPolicyMixin<TView, CustomForEachPolicyBase<TView>>;

/// Procedural image view, which returns the index value for all indices.
template<int tDimension, bool IsDevice, typename TPolicy = bolt::DefaultViewPolicy>
class EmsIndexView: public bolt::HybridImageViewBase<tDimension, TPolicy> {
public:
	static const bool kIsDeviceView = IsDevice;
	static const bool kIsHostView = !IsDevice;
	static const bool kIsMemoryBased = false;
	static const int kDimension = tDimension;
	using Policy = TPolicy;
	using TIndex = typename Policy::IndexType;
	using SizeType = typename bolt::VectorTraits<bolt::Vector<TIndex, tDimension>>::type;
	using IndexType = typename bolt::VectorTraits<bolt::Vector<TIndex, tDimension>>::type;
	using Predecessor = bolt::HybridImageViewBase<tDimension, Policy>;
	using Element = IndexType;
	using AccessType = IndexType;

	BOLT_DECL_HYBRID
	explicit EmsIndexView(SizeType size) : Predecessor(size)
	{
	}

	BOLT_DECL_HYBRID
	IndexType operator[](IndexType index) const
	{
		return index;
	}
};
/// @}

}

#endif
#endif /* SRC_EMS_CUSTOM_FOR_EACH_POLICY_H_ */
