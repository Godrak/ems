/*
 * random_generators_view.h
 *
 *  Created on: Feb 5, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_RANDOM_GENERATORS_VIEW_H_
#define SRC_RANDOM_GENERATORS_VIEW_H_

#include <random>

#include "boltview/host_image.h"
#include "boltview/device_image.h"
#include "boltview/for_each.h"

#include "locations.h"
#include "view_random_fill.h"

namespace ems {

// Taken from:
//	https://developer.nvidia.com/gpugems/gpugems3/part-vi-gpu-computing/chapter-37-efficient-random-number-generation-and-application
struct PseudorandomGenerator {
	static const int constructor_size = 4;
	BOLT_DECL_HYBRID PseudorandomGenerator() {
		z1 = z2 = z3 = z4 = 0;
	}

	BOLT_DECL_HYBRID void initialize(unsigned* init) {
		z1 = init[0];
		z2 = init[1];
		z3 = init[2];
		z4 = init[3];
		// at least three init numbers should be larger than 128
		if (z1 < 129)
			z1 = int(129 + z1 / float(129) * 4294967000.0f);
		if (z2 < 129)
			z2 += int(129 + z2 / float(129) * 4294967000.0f);
		if (z3 < 129)
			z3 += int(129 + z3 / float(129) * 4294967000.0f);
	}

	BOLT_DECL_HYBRID
	float operator()()
	{
		return hybridTaus();
	}

private:
	//S1, S2, S3, and M are all constants, and z is part of the
	// private per-thread generator state.
	BOLT_DECL_HYBRID
	unsigned tausStep(unsigned& z, int S1, int S2, int S3, unsigned M)
	{
		unsigned b = (((z << S1) ^ z) >> S2);
		return z = (((z & M) << S3) ^ b);
	}

	BOLT_DECL_HYBRID
	unsigned LCGStep(unsigned& z, unsigned A, unsigned C)
	{
		return z = (A * z + C);
	}

	BOLT_DECL_HYBRID
	float hybridTaus()
	{
		return 2.3283064365387e-10
			* (tausStep(z1, 13, 19, 12, 4294967294UL) ^ tausStep(z2, 2, 25, 4, 4294967288UL) ^ tausStep(z3, 3, 11, 17, 4294967280UL)
				^ LCGStep(z4, 1664525, 1013904223UL));
	}

	unsigned z1, z2, z3, z4;
};

namespace detail {

template<typename TRandomView, typename TGenerator, typename TIndexType>
struct GeneratorConstructionFunctor {
	BOLT_DECL_HYBRID GeneratorConstructionFunctor(TRandomView random_view) : random_view(random_view) {
	}

	BOLT_HD_WARNING_DISABLE
	BOLT_DECL_HYBRID
	void operator()(TGenerator& gen, TIndexType index) const
	{
		unsigned init[PseudorandomGenerator::constructor_size];
		for (int i = 0; i < PseudorandomGenerator::constructor_size; ++i) {
			auto r = bolt::insertDimension(index, i, index.kDimension);
			init[i] = random_view[r];
		}
		gen.initialize(init);
	}

	const TRandomView random_view;
};

template<bool tRunOnDevice>
struct createRandomGeneratorsImageImpl {

#if defined(__CUDACC__)
	template<typename TSizeType>
	static auto run(TSizeType size, unsigned seed) {
		bolt::DeviceImage<PseudorandomGenerator, TSizeType::kDimension> generators_image { size };
		auto generators_image_view = generators_image.view();

		auto random_image_size = bolt::insertDimension(size, PseudorandomGenerator::constructor_size, size.kDimension);
		bolt::DeviceImage<unsigned, TSizeType::kDimension + 1> random_image(random_image_size);
		auto random_view = random_image.view();
		fillWithRandom(random_view, seed);

		GeneratorConstructionFunctor<typeof(random_image.constView()), PseudorandomGenerator,
			typename decltype(generators_image_view)::IndexType> construction_functor { random_image.constView() };

		bolt::forEachPosition(generators_image_view, construction_functor);
		return std::move(generators_image);
	}
#endif  // __CUDACC__
};

template<>
struct createRandomGeneratorsImageImpl<false> {
	template<typename TSizeType>
	static auto run(TSizeType size, unsigned seed) {
		bolt::HostImage<PseudorandomGenerator, TSizeType::kDimension> generators_image { size };
		auto generators_image_view = generators_image.view();

		auto random_image_size = bolt::insertDimension(size, PseudorandomGenerator::constructor_size, size.kDimension);
		bolt::HostImage<unsigned, TSizeType::kDimension + 1> random_image(random_image_size);
		auto random_view = random_image.view();
		fillWithRandom(random_view, seed);
		GeneratorConstructionFunctor<typeof(random_view), PseudorandomGenerator, typename decltype(generators_image_view)::IndexType> construction_functor {
			random_view };

		bolt::forEachPosition(generators_image_view, construction_functor);
		return std::move(generators_image);
	}
};

} // namespace detail

template<bool TRunOnDevice, typename TSizeType>
auto createRandomGeneratorsImage(TSizeType size, unsigned seed) {
	if (seed == 0) {
		seed = std::random_device { }.operator ()();
	}
	return std::move(detail::createRandomGeneratorsImageImpl<TRunOnDevice>::run(size, seed));
}

}

#endif /* SRC_RANDOM_GENERATORS_VIEW_H_ */
