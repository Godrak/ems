/*
 * strong_type.h
 *
 *  Created on: Sep 15, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_STRONG_TYPE_H_
#define SRC_EMS_STRONG_TYPE_H_

#include "boltview/math/quaternion.h"
#define weak

template<typename T, typename ID>
class StrongType {
public:
	StrongType() = default;
	StrongType(const StrongType&) = default;
	BOLT_DECL_HYBRID StrongType(StrongType&& value) {
		m_value = value.m_value;
	}
	StrongType& operator=(const StrongType&) = default;
	BOLT_DECL_HYBRID
	StrongType& operator=(StrongType&& other) {
		if (this != &other) {
			m_value = other.m_value;
		}
		return *this;
	}

	BOLT_DECL_HYBRID
	explicit StrongType(const T& value) : m_value(value) {
	}

	BOLT_DECL_HYBRID
	T get() const {
		return m_value;
	}
	template<typename T2>
	BOLT_DECL_HYBRID
	StrongType<T, ID>& operator+=(const StrongType<T2, ID>& v) {
		m_value += v.get();
		return *this;
	}

	template<typename T2>
	BOLT_DECL_HYBRID
	StrongType<T, ID>& operator-=(const StrongType<T2, ID>& v) {
		m_value -= v.get();
		return *this;
	}

private:
	T m_value;
};

template<typename S, typename ID>
std::ostream& operator<<(std::ostream& os, const StrongType<S, ID>& value) {
	os << value.get();
	return os;
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
bool operator==(const StrongType<T, ID>& v1, const StrongType<T, ID>& v2) {
	return v1.get() == v2.get();
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
bool operator!=(const StrongType<T, ID>& v1, const StrongType<T, ID>& v2) {
	return !(v1 == v2);
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
bool operator<(const StrongType<T, ID>& v1, const StrongType<T, ID>& v2) {
	return v1.get() < v2.get();
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
bool operator>(const StrongType<T, ID>& v1, const StrongType<T, ID>& v2) {
	return v2 < v1;
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
bool operator<=(const StrongType<T, ID>& v1, const StrongType<T, ID>& v2) {
	return !(v1 > v2);
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
bool operator>=(const StrongType<T, ID>& v1, const StrongType<T, ID>& v2) {
	return !(v1 < v2);
}

#ifdef weak
//Arithmetic

template<typename T, typename ID>
BOLT_DECL_HYBRID
StrongType<T, ID> operator+(const StrongType<T, ID>& v1, const StrongType<T, ID>& v2) {
	return StrongType<T, ID> { v1.get() + v2.get() };
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
StrongType<T, ID> operator-(const StrongType<T, ID>& v1, const StrongType<T, ID>& v2) {
	return StrongType<T, ID> { v1.get() - v2.get() };
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
StrongType<T, ID> operator*(const T& v1, const StrongType<T, ID>& v2) {
	return StrongType<T, ID> { v1 * v2.get() };
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
StrongType<T, ID> operator/(const T& v1, const StrongType<T, ID>& v2) {
	return StrongType<T, ID> { v1 / v2.get() };
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
StrongType<T, ID> operator*(const StrongType<T, ID>& v1, const T& v2) {
	return StrongType<T, ID> { v1.get() * v2 };
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
StrongType<T, ID> operator/(const StrongType<T, ID>& v1, const T& v2) {
	return StrongType<T, ID> { v1.get() / v2 };
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
StrongType<T, ID> operator%(const StrongType<T, ID>& v1, const StrongType<T, ID>& v2) {
	return StrongType<T, ID> { remainder(v1.get(), v2.get()) };
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
T operator/(const StrongType<T, ID>& v1, const StrongType<T, ID>& v2) {
	return {v1.get() / v2.get()};
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
StrongType<T, ID> operator-(const StrongType<T, ID>& v) {
	return StrongType<T, ID> { -v.get() };
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
StrongType<T, ID> operator+(const StrongType<T, ID>& v) {
	return {+v.get()};
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
StrongType<T, ID> abs(const StrongType<T, ID>& v) {
	return StrongType<T, ID> { abs(v.get()) };
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
StrongType<T, ID> min(const StrongType<T, ID>& v1, const StrongType<T, ID>& v2) {
	return StrongType<T, ID> { min(v1.get(), v2.get()) };
}

template<typename T, typename ID>
BOLT_DECL_HYBRID
StrongType<T, ID> max(const StrongType<T, ID>& v1, const StrongType<T, ID>& v2) {
	return StrongType<T, ID> { max(v1.get(), v2.get()) };
}

template<int DIM, typename T, typename ID>
BOLT_DECL_HYBRID
bolt::Vector<StrongType<T, ID>, DIM> operator*(const T& m, const bolt::Vector<StrongType<T, ID>, DIM>& v) {
	bolt::Vector<StrongType<T, ID>, DIM> res;
	for (int i = 0; i < DIM; ++i) {
		res[i] = v[i] * m;
	}
	return res;
}

template<int DIM, typename T, typename ID>
BOLT_DECL_HYBRID
bolt::Vector<StrongType<T, ID>, DIM> operator*(const bolt::Vector<T, DIM>& m, const StrongType<T, ID>& v) {
	bolt::Vector<StrongType<T, ID>, DIM> res;
	for (int i = 0; i < DIM; ++i) {
		res[i] = v * m[i];
	}
	return res;
}

template<int DIM, typename T, typename ID>
BOLT_DECL_HYBRID
bolt::Vector<StrongType<T, ID>, DIM> abs(const bolt::Vector<StrongType<T, ID>, DIM>& v) {
	bolt::Vector<StrongType<T, ID>, DIM> res;
	for (int i = 0; i < DIM; ++i) {
		res[i] = abs(v[i]);
	}
	return res;
}

#endif
//Casts

template<typename T, typename ID>
BOLT_DECL_HYBRID
inline T u(const StrongType<T, ID>& v) {
	return v.get();
}

template<typename ST, typename T>
BOLT_DECL_HYBRID
inline ST t(const T& v) {
	return ST { v };
}

template<int DIM, typename T, typename ID>
BOLT_DECL_HYBRID
bolt::Vector<T, DIM> u(const bolt::Vector<StrongType<T, ID>, DIM>& v) {
	bolt::Vector<T, DIM> res;
	for (int i = 0; i < DIM; ++i) {
		res[i] = v[i].get();
	}
	return res;
}

template<typename ST, typename T, int DIM>
BOLT_DECL_HYBRID
ST t(const bolt::Vector<T, DIM>& v) {
	static_assert(ST::kDimension == DIM,
		"The vectors must have the same dimension.");
	ST res;
	for (int i = 0; i < DIM; ++i) {
		res[i] = typename ST::Element { v[i] };
	}
	return res;
}

template<typename ST, typename T>
BOLT_DECL_HYBRID
ST t(const T& v0, const T& v1) {
	static_assert(ST::kDimension == 2,
		"The vector must have dimension 2.");
	ST res;
	res[0] = typename ST::Element { v0 };
	res[1] = typename ST::Element { v1 };
	return res;
}

template<typename ST, typename T>
BOLT_DECL_HYBRID
ST t(const T& v0, const T& v1, const T& v2) {
	static_assert(ST::kDimension == 3,
		"The vector must have dimension 3.");
	ST res;
	res[0] = typename ST::Element { v0 };
	res[1] = typename ST::Element { v1 };
	res[2] = typename ST::Element { v2 };
	return res;
}

template<int DIM, typename T, typename ID>
BOLT_DECL_HYBRID
T length(const bolt::Vector<StrongType<T, ID>, DIM>& v) {
	return bolt::length(u(v));
}

#endif /* SRC_EMS_STRONG_TYPE_H_ */
