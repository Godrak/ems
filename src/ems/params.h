/*
 * params.h
 *
 *  Created on: Sep 21, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_PARAMS_H_
#define SRC_EMS_PARAMS_H_

#include "jparam_structs.h"
#include "json_params.h"
#include "defines.h"

namespace json_params {

template<>
inline ems::Bohr JsonParamsHandler::getValue(const std::string& key) const {
	bool ok = false;
	ems::Num res = getFractional(params[key], ok);
	if (ok)
		return ems::Bohr { res };
	//the type of this value is not numeric -> error
	std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, Float expected" << std::endl;
	exit(1);
}

template<>
inline ems::IPixel JsonParamsHandler::getValue(const std::string& key) const {
	bool ok = false;
	ems::Num res = params[key].ToInt(ok);
	if (ok)
		return ems::IPixel { res };
	//the type of this value is not numeric -> error
	std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, Int expected" << std::endl;
	exit(1);
}

template<>
inline ems::Num3 JsonParamsHandler::getValue(const std::string& key) const {
	bool array = params[key].length() == 3;

	if (array) {
		bool resOk = true;
		bool ok = false;
		ems::Num res0 = getFractional(params[key][0], ok);
		resOk = ok && resOk;
		ems::Num res1 = getFractional(params[key][1], ok);
		resOk = ok && resOk;
		ems::Num res2 = getFractional(params[key][2], ok);
		resOk = ok && resOk;

		if (resOk)
			return ems::Num3 { res0, res1, res2 };
	}
	//the type of this value is not numeric -> error
	std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, array of 3 numbers expected" << std::endl;
	exit(1);
}

template<>
inline bolt::Vector<int, 2> JsonParamsHandler::getValue(const std::string& key) const {
	bool array = params[key].length() == 2;

	if (array) {
		bool resOk = true;
		bool ok = false;
		int res0 = params[key][0].ToInt(ok);
		resOk = ok && resOk;
		int res1 = params[key][1].ToInt(ok);
		resOk = ok && resOk;

		if (resOk)
			return {res0, res1};
	}
	//the type of this value is not numeric -> error
	std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, array of 2 numbers expected" << std::endl;
	exit(1);
}

template<>
inline ems::IPixel2 JsonParamsHandler::getValue(const std::string& key) const {
	auto res = getValue<bolt::Vector<int, 2>>(key);
	return t<ems::IPixel2>(res);
}

template<>
inline ems::IPixel3 JsonParamsHandler::getValue(const std::string& key) const {
	auto res = getValue<bolt::Vector<int, 3>>(key);
	return t<ems::IPixel3>(res);
}

template<>
inline bolt::Quaternion<float> JsonParamsHandler::getValue(const std::string& key) const {
	bool array = params[key].length() == 4;

	if (array) {
		bool resOk = true;
		bool ok = false;
		ems::Num res0 = getFractional(params[key][0], ok);
		resOk = ok && resOk;
		ems::Num res1 = getFractional(params[key][1], ok);
		resOk = ok && resOk;
		ems::Num res2 = getFractional(params[key][2], ok);
		resOk = ok && resOk;
		ems::Num res3 = getFractional(params[key][3], ok);
		resOk = ok && resOk;

		if (resOk)
			return bolt::Quaternion<float> { res0, res1, res2, res3 };
	}
	//the type of this value is not numeric -> error
	std::cerr << "ERROR: The parameter: " << key << " found in json, but has incorrect type, array of 3 numbers expected" << std::endl;
	exit(1);
}

template<>
inline ems::Bohr3 JsonParamsHandler::getValue(const std::string& key) const {
	auto res = getValue<ems::Num3>(key);
	return t<ems::Bohr3>(res);
}

}

namespace ems {

PARAMWD(ems_PotentialFile, std::string, "", "path to mrc file with the potential map");

PARAMWD(ems_ScatteringFile, std::string, "", "path to mrc file with the scattering volume map");

PARAMWD(ems_ElectronDose, float, 20.001426027, "electron dose to the specimen [electron per Bohr^2]");

PARAM(ems_ElectronEnergy, float, "energy of emitted electrons [keV]");

PARAMWD(ems_ApplyDefocus, bool, false, "if true, detector is shifted away by the defocus value");

PARAMWD(ems_ApplyCTF, bool, false, "if true, the final image is convolved with the CTF function");

PARAM(
	ems_RandomizedElectronEmission,
	int,
	"0 - electron starts in the center of the emitter pixel, 1 - electron starts within the pixel uniformly displaced, 2 - electron can start anywehere on the emitter plane");

PARAMWD(ems_RandomSeed, size_t, 0, "random seed, if zero, random device is used, for reproducible results use some fixed number");

PARAM(ems_Model, std::string, "defines simulator model. currently: dcs, ruther, trans");

PARAMWD(
	ems_EmitterPixelSize,
	ems::IPixel,
	t<ems::IPixel>(0),
	"size of emitter plane in pixels, if not provided, computed such that pixel has size of one bohr^2");

PARAM(ems_MajorantAtomicNumber, size_t, "atomic number of the majorant - element with largest atomic number");

PARAM(
	ems_RutherSamplingAtomicNumber,
	size_t,
	"atomic number used for sampling formula in Rutherford (Ruther model and importance sampling of DCS)");

PARAMWD(
	ems_MajorantPercentile,
	float,
	1,
	"value between zero and one, describing the ratio of density values, that are lower than majorant {serves as a control for density outliers}");

PARAM(
	ems_LocalFTFrameRadius,
	Bohr,
	"the radius of the location that is considered when computing local Fourier transform for scattering [Bohr]. If not provided, computed from voxel size");

PARAM(ems_LocalFTSamplesPerAxis, size_t, "number of samples per axis when computing Fourier sample from definition");

PARAMWD(ems_hg_GreensteinG, float, 0.995, "parameter g of Greenstein sampling");

PARAM(ems_LocalCrossSectionSampleCount, size_t, "number of samples per spot computed for local cross sections");

PARAMWD(
	ems_CrossSectionsFile,
	std::string,
	"",
	"path to mrc file with the computed cross sections. If not provided, computed from potential");

PARAMWD(ems_CrossSectionsDumpPath, std::string, "", "path to file where the computed cross sections volume should be stored");

}

#endif /* SRC_EMS_PARAMS_H_ */
