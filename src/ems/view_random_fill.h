/*
 * view_random_fill.h
 *
 *  Created on: Feb 4, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_VIEW_RANDOM_FILL_H_
#define SRC_VIEW_RANDOM_FILL_H_

#include "curand.h"
#include "boltview/for_each.h"

namespace ems {

namespace detail {

template<bool tRunOnDevice>
struct FillWithRandomImpl {

#if defined(__CUDACC__)
#define CURAND_CHECK_MSG(error_message, ...) \
		do {\
			curandStatus_t err = __VA_ARGS__ ;\
			if(CURAND_STATUS_SUCCESS != err) {\
				std::string msg = boost::str(boost::format("%1%:%2%: %3% %4%") % __FILE__ % __LINE__ % error_message % (err));\
				BOOST_THROW_EXCEPTION(::bolt::CudaError() << bolt::MessageErrorInfo(msg));\
			}\
		} while (false);

#define CURAND_CHECK(...) \
		CURAND_CHECK_MSG(#__VA_ARGS__, __VA_ARGS__)

	template<typename TView>
	static void run(TView view, unsigned seed) {
		curandGenerator_t generator { };
		CURAND_CHECK(curandCreateGenerator(&generator, CURAND_RNG_PSEUDO_XORWOW));
		CURAND_CHECK(curandSetPseudoRandomGeneratorSeed(generator, seed));
		auto mem_size = bolt::product(view.size());
		CURAND_CHECK(curandGenerate(generator, view.pointer(), mem_size));
		CURAND_CHECK(curandDestroyGenerator(generator));
	}
#endif  // __CUDACC__
};

template<>
struct FillWithRandomImpl<false> {
	template<typename TView>
	static void run(TView view, unsigned seed) {
		srand(seed);
		bolt::forEach(view, [](typename TView::AccessType val) {val = rand();});
	}
};

} // namespace detail

template<typename TView>
void fillWithRandom(TView view, unsigned seed) {
	detail::FillWithRandomImpl<TView::kIsDeviceView>::run(view, seed);
}

}

#endif /* SRC_VIEW_RANDOM_FILL_H_ */
