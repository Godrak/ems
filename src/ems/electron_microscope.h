/*
 * electron_microscope.h
 *
 *  Created on: Mar 2, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_ELECTRON_MICROSCOPE_H_
#define SRC_EMS_ELECTRON_MICROSCOPE_H_

#include "scattering_function.h"
#include "random_generators_view.h"
#include "ray_observers.h"
#include "custom_for_each_policy.h"
#include "ray_tracer.h"
#include "image_formations.h"
#include "cross_section_computation.h"
#include "ems_setup.h"

#include "boltview/for_each.h"

namespace ems {

template<typename TScattering, typename TImageFormation, typename TRayObserverOne = NullRayObserver,
	typename TRayObserverTwo = NullRayObserver>
class ElectronMicroscope {
public:
	using ImageFormation = TImageFormation;

	ElectronMicroscope(
		const ElectronMicroscopeSetup setup,
		TScattering scattering,
		TImageFormation image_formation,
		TRayObserverOne observer_one = { },
		TRayObserverTwo observer_two = { }) :
		setup(setup),
		scattering(scattering),
		image_formation(image_formation),
		observer_one(observer_one),
		observer_two(observer_two)
	{
	}

	template<typename TProjectionView, typename TGeneratorsView>
	Detector render(
		TProjectionView projection,
		bolt::Quaternion<Num> orientation,
		const Pixel3& subpixel_shift,
		const Bohr& detector_distance,
		TGeneratorsView generators) const
	{
		Num unscattered_dose_per_pixel;
		auto emitter_detector = constructEmitterAndDetector(
			t<IPixel2>(projection.size()),
			t<IPixel2>(generators.size()),
			orientation,
			subpixel_shift,
			detector_distance,
			unscattered_dose_per_pixel);
		Emitter emitter = emitter_detector.first;
		Detector detector = emitter_detector.second;
		auto raytracer = constructRayTracer(*this, projection, generators, emitter, detector);
		bolt::fill(projection, typename TProjectionView::Element { unscattered_dose_per_pixel });
		EmsIndexView < 2, TProjectionView::kIsDeviceView > emitting_view { u(emitter.size) };
//		bolt::forEach(emitting_view, raytracer); //CustomForEachPolicy<decltype(emitting_view)> { }
		bolt::forEach(emitting_view, raytracer, CustomForEachPolicy<decltype(emitting_view)> { });
		return detector;
	}

private:
	std::pair<Emitter, Detector> constructEmitterAndDetector(
		const IPixel2& projection_size,
		const IPixel2& generators_size,
		bolt::Quaternion<Num> orientation,
		const Pixel3& pixel_shift,
		const Bohr& detector_distance,
		Num& unscattered_dose_per_pixel) const
	{
		Bohr3 detector_origin = t<Bohr3>(
			bolt::rotate(u(setup.default_detector_origin + setup.default_ray_dir * detector_distance), orientation));
		Num detector_x_shift = u(pixel_shift[0]) * bolt::norm(u(setup.default_detector_x_vector)) / u(projection_size[0]);
		Num detector_y_shift = u(pixel_shift[1]) * bolt::norm(u(setup.default_detector_y_vector)) / u(projection_size[1]);
		Bohr3 detector_x_vector = t<Bohr3>(bolt::rotate(u(setup.default_detector_x_vector), orientation));
		Bohr3 detector_y_vector = t<Bohr3>(bolt::rotate(u(setup.default_detector_y_vector), orientation));
		detector_origin += detector_x_shift * detector_x_vector + detector_y_shift * detector_y_vector;

		Bohr3 emitter_origin = t<Bohr3>(bolt::rotate(u(setup.default_emitter_origin), orientation));
		Num emitter_x_shift = u(pixel_shift[0]) * bolt::norm(u(setup.default_emitter_x_vector)) / u(projection_size[0]);
		Num emitter_y_shift = u(pixel_shift[1]) * bolt::norm(u(setup.default_emitter_y_vector)) / u(projection_size[1]);
		Bohr3 emitter_x_vector = t<Bohr3>(bolt::rotate(u(setup.default_emitter_x_vector), orientation));
		Bohr3 emitter_y_vector = t<Bohr3>(bolt::rotate(u(setup.default_emitter_y_vector), orientation));
		emitter_origin += emitter_x_shift * emitter_x_vector + emitter_y_shift * emitter_y_vector;

		Num3 ray_dir = bolt::rotate(setup.default_ray_dir, orientation);

		ImageLocation detector_location { detector_origin, detector_x_vector, detector_y_vector };
		ImageLocation emitter_location { emitter_origin, emitter_x_vector, emitter_y_vector };

		auto emitter_bohr_per_pixel = bolt::div(u(emitter_location.size()), u(generators_size));
		auto emitter_pixel_size_bohr2 = bolt::product(emitter_bohr_per_pixel);
		auto emitter_samples_per_pixel = size_t(std::ceil(setup.scattered_dose * emitter_pixel_size_bohr2));
		LOG("scattered electrons per emitter pixel:  " << emitter_samples_per_pixel);
		Emitter emitter { emitter_location, generators_size, emitter_samples_per_pixel, setup.randomized_electron_position, ray_dir };

		Detector detector { detector_location, projection_size };
		unscattered_dose_per_pixel = setup.unscattered_dose * bolt::product(u(detector.pixel_size));
		LOG("unscattered electrons detector per pixel:  " << unscattered_dose_per_pixel);

		return std::make_pair(emitter, detector);
	}

	const ElectronMicroscopeSetup setup;
	const TScattering scattering;
	const TImageFormation image_formation;
	const TRayObserverOne observer_one;
	const TRayObserverTwo observer_two;

private:
	template<typename TElectronMicroscope, typename TProjectionView, typename TGeneratorsView>
	friend class RayTracer;
};

template<typename TScattering, typename TImageFormation, typename TRayObserverOne = NullRayObserver,
	typename TRayObserverTwo = NullRayObserver>
ElectronMicroscope<TScattering, TImageFormation, TRayObserverOne, TRayObserverTwo> constructElectronMicroscope(
	const ElectronMicroscopeSetup setup,
	TScattering scattering,
	TImageFormation image_formation,
	TRayObserverOne observer_one = { },
	TRayObserverTwo observer_two = { })
{
	return ElectronMicroscope<TScattering, TImageFormation, TRayObserverOne, TRayObserverTwo> { setup, scattering, image_formation,
		observer_one, observer_two };
}

}

#endif /* SRC_EMS_ELECTRON_MICROSCOPE_H_ */
