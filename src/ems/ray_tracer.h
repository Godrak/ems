/*
 * ray_tracer.h
 *
 *  Created on: Oct 5, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_RAY_TRACER_H_
#define SRC_EMS_RAY_TRACER_H_

#include "raycast_data.h"

namespace ems {

template<typename TElectronMicroscope, typename TProjectionView, typename TGeneratorsView>
struct RayTracer {
	const TElectronMicroscope microscope;
	const TProjectionView projection;
	const TGeneratorsView generators;
	const Emitter emitter;
	const Detector detector;

	using Payload = typename TElectronMicroscope::ImageFormation::Payload;

	BOLT_DECL_HYBRID RayTracer(
		TElectronMicroscope microscope,
		TProjectionView projection,
		TGeneratorsView generators,
		Emitter emitter,
		Detector detector) : microscope(microscope), projection(projection), generators(generators), emitter(emitter), detector(detector)
	{
	}

	// not used, artifact from history
	BoundingBox paddedInteractionBoundingBox(const ImageLocation& loc1, const ImageLocation& loc2) const {
		auto min1 = bolt::min(loc1.start(), loc1.end());
		auto min2 = bolt::min(loc2.start(), loc2.end());
		auto max1 = bolt::max(loc1.start(), loc1.end());
		auto max2 = bolt::max(loc2.start(), loc2.end());

		auto min = bolt::min(min1, min2);
		auto max = bolt::max(max1, max2);
		return {min,max};
	}

	BOLT_DECL_HYBRID
	void operator()(const bolt::Int2& origin_pixel) const {
		tracePixelRays(t<IPixel2>(origin_pixel));
	}

private:BOLT_DECL_HYBRID
	typename TGeneratorsView::AccessType getGenerator(const IPixel2& origin_pixel) const {
		return generators[u(origin_pixel)];
	}

	BOLT_DECL_HYBRID
	float getRnd(const IPixel2& origin_pixel) const {
		return getGenerator(origin_pixel)();
	}

	BOLT_DECL_HYBRID
	void tracePixelRays(const IPixel2& origin_pixel) const {
		for (size_t sample = 0; sample < emitter.samples_per_pixel; ++sample) {
			Payload payload { origin_pixel, sample };
			Bohr3 origin = emitter.getSourceLocation(origin_pixel, getRnd(origin_pixel), getRnd(origin_pixel));
			Ray ray { origin, emitter.init_ray_direction };
			traceSingleRay(ray, origin_pixel, payload);
		}
	}

	BOLT_DECL_HYBRID
	void traceSingleRay(const Ray& start_ray, const IPixel2& origin_pixel, Payload& payload) const {
		size_t event_count = 0;
		Bohr next_event_distance;
		Ray ray = start_ray;
		ray = microscope.scattering.scatter(
			ray,
			getGenerator(origin_pixel),
			next_event_distance,
			payload,
			microscope.setup.distance_from_emitter_to_detector);

		Bohr detector_plane_distance;
		IPixel2 detector_pixel;
		bool valid_detector_pixel;
		valid_detector_pixel = detector.getIntersectedPixel(ray, detector_pixel, detector_plane_distance);

		while (detector_plane_distance > next_event_distance && willIntersectVolume(ray)) {
			observe(payload, ray, next_event_distance);
			ray = { ray.origin + ray.dir * next_event_distance, ray.dir };
//			LOG("previous ray: " << toString(ray.dir));
			ray = microscope.scattering.scatter(ray, getGenerator(origin_pixel), next_event_distance, payload); //Warning: next_event_distance is changed here
//			LOG("new ray: " << toString(ray.dir));
			event_count++;
			valid_detector_pixel = detector.getIntersectedPixel(ray, detector_pixel, detector_plane_distance);
		}

		if (valid_detector_pixel) {
			observe(payload, ray, detector_plane_distance);
			ray = { ray.origin + ray.dir * detector_plane_distance, ray.dir };

			Frame emitter_frame { emitter.location.xDir(), emitter.location.yDir(), emitter.init_ray_direction };

			auto ray_deviation = emitter_frame.toLocal(ray.dir);
			microscope.image_formation(projection, detector_pixel, payload, Ray { ray.origin, ray_deviation }, detector.pixel_size);
		}
	}

	BOLT_DECL_HYBRID
	bool willIntersectVolume(const Ray& ray) const {
		Bohr min;
		Bohr max;
		bool intersect = intersection(ray, microscope.setup.interaction_bounding_box, min, max);
		return intersect && max >= Bohr { -EPSILON };
	}

	BOLT_DECL_HYBRID
	void observe(Payload& payload, const Ray& ray, const Bohr& step) const {
		microscope.observer_one(payload, ray, step);
		microscope.observer_two(payload, ray, step);
	}
};

template<typename TElectronMicroscope, typename TProjectionView, typename TGeneratorsView>
BOLT_DECL_HYBRID
RayTracer<TElectronMicroscope, TProjectionView, TGeneratorsView> constructRayTracer(
	const TElectronMicroscope& microscope,
	TProjectionView projection,
	TGeneratorsView generators,
	const Emitter& emitter,
	const Detector& detector)
{
	return RayTracer<TElectronMicroscope, TProjectionView, TGeneratorsView>(microscope, projection, generators, emitter, detector);
}

}

#endif /* SRC_EMS_RAY_TRACER_H_ */
