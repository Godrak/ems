/*
 * localized_fourier_potential.h
 *
 *  Created on: Apr 15, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_LOCALIZED_FOURIER_POTENTIAL_H_
#define SRC_EMS_LOCALIZED_FOURIER_POTENTIAL_H_

#include "boltview/math/vector.h"
#include "boltview/host_image.h"
#include "boltview/copy.h"
#include "boltview/fill.h"
#include "boltview/for_each.h"
#include "boltview/image_io.h"
#include "boltview/device_image.h"
#include "boltview/fft/fft_calculator.h"
#include "boltview/interpolation.h"
#include "boltview/procedural_views.h"

#include <cmath>

#include "raycast_data.h"

namespace ems {

Num computeLimitingAngleOfLocalizedFP(Num momentum_size, Bohr radius, size_t max_per_axis_samples_count) {
	auto step_size = Num { 2 } * radius / Num { max_per_axis_samples_count };
	auto max_frequency = Num { 2 } / u(step_size);
	// Use cosine law to compute angle of the triangle
	auto cos_theta = (momentum_size * momentum_size + momentum_size * momentum_size - max_frequency * max_frequency)
		/ (2 * momentum_size * momentum_size);

	return acos(cos_theta);
}

template<typename TPotentialView>
struct LocalizedFourierTransform {
	using ComplexType = typename std::conditional_t<bolt::IsDeviceImageView<TPotentialView>::value, bolt::DeviceComplexType, bolt::HostComplexType>;

	const PhysicalVolume<TPotentialView> volume;
	const Bohr3 center;
	const Bohr radius;
	const size_t max_per_axis_samples_count;

	const Bohr3 min;
	const Bohr3 max;
	const Bohr3 size;
	const Bohr3 step_size;
	const Bohr3 offset;

	BOLT_DECL_HYBRID
	LocalizedFourierTransform(
		const PhysicalVolume<TPotentialView>& volume,
		const Bohr3& position,
		Bohr radius,
		size_t max_per_axis_samples_count) :
		volume(volume),
		center(position),
		radius(radius),
		max_per_axis_samples_count(max_per_axis_samples_count),
		min(center - Bohr3::fill(radius)),
		max(center + Bohr3::fill(radius)),
		size(max - min),
		step_size(bolt::div(size, Num3::fill(max_per_axis_samples_count))),
		offset(Num { 0.5 } * (size - bolt::product(step_size, Num3::fill(max_per_axis_samples_count))))
	{
		static_assert(bolt::IsInterpolatedView<TPotentialView>::value);
	}

	BOLT_DECL_HYBRID
	ComplexType at(const Num3& frequency, size_t& samples_count) const
	{
		samples_count = bolt::product(Num3::fill(max_per_axis_samples_count));
		ComplexType sum { };
		if (!(Num { 2 } / bolt::maxElement(u(step_size)) >= bolt::maxElement(bolt::abs(frequency)))) {
			return sum;
		}

		using namespace bolt;
		for (size_t x = 0; x < max_per_axis_samples_count; ++x) {
			for (size_t y = 0; y < max_per_axis_samples_count; ++y) {
				for (size_t z = 0; z < max_per_axis_samples_count; ++z) {
					Bohr3 position = min + offset + bolt::product(step_size, Num3 { x, y, z });
					auto shift_param = Num(-2 * M_PI) * bolt::sum(bolt::product(frequency, bolt::div(position - min, size)));
					auto shift = ComplexType { cos(shift_param), sin(shift_param) };
					sum = sum + ComplexType { volume.access(position) } * shift;
				}
			}
		}
		return sum;
	}

	BOLT_DECL_HYBRID
	Bohr3 getLocationSize() const
	{
		return size;
	}
};

template<typename TPotentialView>
BOLT_DECL_HYBRID
auto makeLocalizedFourierTransform(
	const PhysicalVolume<TPotentialView>& volume,
	const Bohr3& position,
	Bohr radius,
	size_t max_per_axis_samples_count)
{
	return LocalizedFourierTransform<TPotentialView> { volume, position, radius, max_per_axis_samples_count };
}

}

#endif /* SRC_EMS_LOCALIZED_FOURIER_POTENTIAL_H_ */
