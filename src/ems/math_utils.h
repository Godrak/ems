/*
 * math_utils.h
 *
 *  Created on: Dec 18, 2019
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_MATH_UTILS_H_
#define SRC_MATH_UTILS_H_

#include "locations.h"

namespace ems {

struct BoundingBox {
	BOLT_DECL_HYBRID BoundingBox() {
	}

	BOLT_DECL_HYBRID BoundingBox(const Bohr3& min_corner, const Bohr3& max_corner) : min_corner(min_corner), max_corner(max_corner) {
	}

	BOLT_DECL_HYBRID
	Bohr3 size() const {
		return max_corner - min_corner;
	}

	BOLT_DECL_HYBRID
	Bohr3 min() const {
		return min_corner;
	}

	BOLT_DECL_HYBRID
	Bohr3 max() const {
		return max_corner;
	}

	BOLT_DECL_HYBRID
	Bohr3 center() const {
		return 0.5f * (min_corner + max_corner);
	}

	BOLT_DECL_HYBRID
	bool isInside(const Bohr3& point) const {
		return min() <= point && point <= max();
	}

private:
	Bohr3 min_corner { };
	Bohr3 max_corner { };
};

struct Ray {
	BOLT_DECL_HYBRID Ray(const Bohr3& origin, const Num3& direction) :
		origin(origin),
		dir(bolt::normalize(direction)),
		inv_dir(bolt::div(Num3 { 1, 1, 1 }, dir))
	{
	}

	Bohr3 origin;
	Num3 dir;
	Num3 inv_dir;
};

BOLT_DECL_HYBRID bool intersection(const Ray& ray, const BoundingBox& bounding_box, Bohr& tmin, Bohr& tmax) {
	Bohr3 entry = bolt::product(bounding_box.min() - ray.origin, ray.inv_dir);
	Bohr3 exit = bolt::product(bounding_box.max() - ray.origin, ray.inv_dir);

	Bohr3 real_entry = bolt::min(entry, exit);
	Bohr3 real_exit = bolt::max(entry, exit);

	tmin = bolt::maxElement(real_entry);
	tmax = bolt::minElement(real_exit);

	return tmax > (tmin + Bohr { EPSILON });
}

BOLT_DECL_HYBRID bool intersection(const Ray& ray, const ImageLocation& image_location, Bohr& distance) {
//	(ray.origin + ray.dir*distance - image_location.start()  ) dot image_location.normal() = 0;
//distance * ray.dir dot image_location.normal()  + (ray.origin - image_location.start()) dot image_location.normal() = 0
//	distance = (image_location.start() - ray.origin) dot image_location.normal() / (ray.dir dot image_location.normal());

	auto denom = bolt::dot(ray.dir, image_location.normal());
	if (abs(denom) < EPSILON) {
		//ray almost parallel to the plane, no intersection
		distance = Bohr { INF };
		return false;
	} else {
		distance = t<Bohr>(bolt::dot(u(image_location.start() - ray.origin), image_location.normal()) / denom);
		return true;
	}

}

/// Coordinate frame
class Frame {
public:

	BOLT_DECL_HYBRID Frame() {
		mX = Num3(1, 0, 0);
		mY = Num3(0, 1, 0);
		mZ = Num3(0, 0, 1);
	}

	BOLT_DECL_HYBRID Frame(const Num3& x, const Num3& y, const Num3& z) : mX(x), mY(y), mZ(z) {
	}

	BOLT_DECL_HYBRID
	void setFromZ(const Num3& z) {
		mZ = normalize(z);
		Num3 tmpZ = mZ;
		Num3 tmpX = (std::abs(tmpZ[0]) > 0.99f) ? Num3(0, 1, 0) : Num3(1, 0, 0);
		mY = normalize(cross(tmpZ, tmpX));
		mX = cross(mY, tmpZ);
	}

	BOLT_DECL_HYBRID
	Num3 toWorld(const Num3& a) const {
		return a[0] * mX + a[1] * mY + a[2] * mZ;
	}

	BOLT_DECL_HYBRID
	Num3 toLocal(const Num3& a) const {
		return Num3(dot(a, mX), dot(a, mY), dot(a, mZ));
	}

	BOLT_DECL_HYBRID
	const Num3& binormal() const {
		return mX;
	}

	BOLT_DECL_HYBRID
	const Num3& tangent() const {
		return mY;
	}

	BOLT_DECL_HYBRID
	const Num3& normal() const {
		return mZ;
	}

public:
	Num3 mX, mY, mZ;
};

BOLT_DECL_HYBRID
Num3 sampleSphereUniform(float sample_one, float sample_two, float* pdf = nullptr) {
	float term_one = 2.0f * M_PIf32 * sample_one;
	float term_two = 2.0f * sqrt(sample_two - sample_two * sample_two);

	if (pdf) {
		*pdf = 0.25f * M_1_PIf32;
	}

	return {cos(term_one)*term_two, sin(term_one)*term_two, 1.0f - 2.0f*sample_two};
}

BOLT_DECL_HYBRID
bolt::Float2 sampleDiscUniform(float sample_one, float sample_two) {
	auto theta = (sample_two * 2.0f - 1.0f) * M_PIf32;
	auto sq_a = sqrt(sample_one);
	return {sq_a*cos(theta), sq_a*(sin(theta))};
}

BOLT_DECL_HYBRID
Num3 sampleS2CapUniform(float sample_one, float sample_two, float spread_angle) {
	auto theta = (sample_two * 2.0f - 1.0f) * M_PIf32;
	auto sq_a = sqrt(sample_one);
	auto x = sq_a * cos(theta);
	auto y = sq_a * sin(theta);

	float cap_height = 1.0f - cos(spread_angle);
	float dot = x * x + y * y;
	float k = cap_height * dot;   // h(x^2+y^2)
	float s = sqrt(cap_height * (2.f - k));     // sqrt( h(2-h(x^2+y^2)) )

	return {s*x, s*y, 1.0f - k};
}

BOLT_DECL_HYBRID
float areaS2Cap(float spread_angle) {
	float cap_height = 1 - cos(spread_angle);
	return 2 * M_PIf32 * cap_height;
}


BOLT_DECL_HYBRID
Num3 SamplePowerCosHemisphere(float sample_one, float sample_two, float power, float* pdf) {
	const float term1 = 2.f * M_PIf32 * sample_one;
	const float term2 = std::pow(sample_two, 1.f / (power + 1.f));
	const float term3 = std::sqrt(1.f - term2 * term2);

	if (pdf) {
		*pdf = (power + 1.f) * std::pow(term2, power) * (0.5f * M_1_PIf32x);
	}

	return Num3(std::cos(term1) * term3, std::sin(term1) * term3, term2);
}

//Beware that the PDF is adjusted for spherical sampling
BOLT_DECL_HYBRID
Num SampleHeyneyGreensteinCosTheta(Num g, Num sample_one, Num* pdf) {
	Num cos_theta;
	if (g == 0) {
		cos_theta = 2 * sample_one - 1;
		if (pdf) {
			*pdf = (1.0) / (4.0 * M_PI);
		}
	} else {
		Num g2 = g * g;
		Num ex = (1 - g2) / (1 - g + 2 * g * sample_one);
		cos_theta = (1 + g2 - ex * ex) / (2 * g);

		if (pdf) {
			*pdf = (1.0 - g2) / (4.0 * M_PI * pow(1.0 + g2 - 2.0 * g * cos_theta, 3.0 / 2.0));
		}

	}
	return cos_theta;
}

BOLT_DECL_HYBRID
Num3 SampleHeyneyGreenstein(Num g, Num sample_one, Num sample_two, Num* pdf) {
	Num cos_theta = SampleHeyneyGreensteinCosTheta(g, sample_one, pdf);

	//https://mathinsight.org/spherical_coordinates
	float theta = acos(fmin(1.0f, fmax(-1.0f, cos_theta)));
	float psi = 2 * M_PI * sample_two;
	float sin_theta = sin(theta);

	float x = sin_theta * cos(psi);
	float y = sin_theta * sin(psi);
	float z = cos_theta;

	return {x,y,z};
}

template<typename T, int S>
BOLT_DECL_HYBRID
void checkNan(const bolt::Vector<T, S>& v, const char* name) {
	bool nan = false;
	for (size_t i = 0; i < S; ++i) {
		nan = (v[i] != v[i] || nan);
	}

	if (nan) {
		printf("%s : %4.2f %4.2f %4.2f", name, v[0], v[1], v[2]);
	}

}

}

#endif /* SRC_MATH_UTILS_H_ */
