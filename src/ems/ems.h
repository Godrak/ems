/*
 * ems.h
 *
 *  Created on: Mar 4, 2021
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_EMS_H_
#define SRC_EMS_EMS_H_

#include "electron_microscope.h"
#include "memory"
#include <boost/optional.hpp>
#include <boost/utility/in_place_factory.hpp>
#include "functional"

namespace ems {

namespace utils {

void parseMRCHeader(const std::string& mrc_file, IVoxel& volume_size, Bohr& bohr_per_voxel) {
	auto vol_dims = mrc::GetDimensions(mrc_file);
	auto header = mrc::GetHeader(mrc_file);
	float apvo = mrc::GetApix(header);
	bohr_per_voxel = Bohr { angToBohr(apvo) };
	volume_size = t<IVoxel>(std::get<0>(vol_dims), std::get<1>(vol_dims), std::get<2>(vol_dims));
}

template<typename TView, typename std::enable_if_t<!bolt::IsInterpolatedView<TView>::value, bool> = true>
auto makeInterpolated(TView view) {
	return bolt::makeInterpolatedView(view, bolt::LinearInterpolator<bolt::BorderHandlingTraits<bolt::BorderHandling::kZero>>());
}

template<typename TView, typename std::enable_if_t<bolt::IsInterpolatedView<TView>::value, bool> = true>
auto makeInterpolated(TView view) {
	return view;
}

template<typename T, int S>
T getValueOfPercentile(bolt::HostImageView<T, S> view, float percentile) {
	std::vector<T> data(view.pointer(), view.pointer() + bolt::product(view.size()));
	std::sort(data.begin(), data.end());
	return data[std::min(size_t(percentile * data.size()), data.size() - 1)];
}

template<typename HostView, typename TView>
void loadToDevice(std::string path, const HostView& host, const TView& dest) {
	mrc::Data(path, host);
	bolt::copy(host, dest);
}

}

template<bool USE_DEVICE>
class Ems {
public:
	template<typename TType, int TDimension>
	using ImgType = typename std::conditional_t<USE_DEVICE, bolt::DeviceImage<TType, TDimension>, bolt::HostImage<TType, TDimension>>;
	using RenderViewType = typename ImgType<float,2>::ViewType;

private:
	template<typename TType, int TDimension>
	using IntImgType = typename std::conditional_t<USE_DEVICE, bolt::TextureImage<TType, TDimension>, bolt::HostImage<TType, TDimension>>;
	using ComplexImage = typename std::conditional_t<USE_DEVICE, bolt::DeviceImage<bolt::DeviceComplexType,2>,bolt::HostImage<bolt::HostComplexType,2>>;
	using GeneratorsImage = decltype(createRandomGeneratorsImage<USE_DEVICE>(typename ImgType<float,2>::SizeType {},0));

	std::unique_ptr<IntImgType<float, 3>> potential_volume;
	std::unique_ptr<IntImgType<float, 3>> cross_sections_volume;
	std::unique_ptr<IntImgType<float, 3>> scattering_coefficients_volume;
	std::unique_ptr<GeneratorsImage> generators_image;

	// std::function is used for type erasure -> this class can represent any of the microscope models, which have different types due to templates.
	// the actual constructed microscope model is stored in the lambda capture and only its render function is exposed via this interface.
	std::function<Detector(RenderViewType, bolt::Quaternion<Num>, const Pixel3&, const Bohr&)> render_function;

	std::string model_name;
	ElectronMicroscopeSetup setup;
public:
	explicit Ems(const json_params::JsonParamsHandler& params) {
		std::string potential_file = params.get<ems_PotentialFile>();
		std::string scattering_file = params.get<ems_ScatteringFile>();
		std::string cross_sections_file = params.get<ems_CrossSectionsFile>();
		std::string dump_path = params.get<ems_CrossSectionsDumpPath>();

		model_name = params.get<ems_Model>();

		// While not common, it is possible to initialize microscope with the scattering volume only, if using Rutherford scattering function.
		// Allows for experiments with the scattering volume.
		std::string volume_file;
		if (scattering_file != "") {
			volume_file = scattering_file;
		} else {
			volume_file = potential_file;
		}

		// Before loading the volumetric data, we need to parse the headers and get sizes of the volume
		IVoxel volume_size { };
		Bohr bohr_per_voxel { };
		utils::parseMRCHeader(volume_file, volume_size, bohr_per_voxel);
		setup = ElectronMicroscopeSetup { volume_size, bohr_per_voxel, params };

		//host based memory holder for loading. From this image, we will later copy the data onto device. In CPU version, we make unnecessary copy as well (currently not a problem)
		bolt::HostImage<float, 3> host_volume { u(volume_size) };
		auto host = host_volume.view();

		//Type for physical interpolated volume
		using PIV = decltype(makePhysicalVolume(utils::makeInterpolated(typename IntImgType<float, 3>::ViewType {}), setup));

		//loading potential volume (if potential file specified)
		boost::optional<PIV> physical_potential_int_view { };
		if (potential_file != "") {
			potential_volume = std::make_unique<IntImgType<float, 3>>(u(volume_size));
			utils::loadToDevice(potential_file, host, potential_volume->view());
			auto potential_int_view = utils::makeInterpolated(potential_volume->view());
			physical_potential_int_view = boost::in_place(makePhysicalVolume(potential_int_view, setup));
		}

		boost::optional<PIV> physical_cs_int_view { };
		if (params.get<ems_Model>() == "dcs") {
			//loading / computing cross sections volume
			cross_sections_volume = std::make_unique<IntImgType<float, 3>>(u(volume_size));
			if (cross_sections_file != "") {
				utils::loadToDevice(cross_sections_file, host, cross_sections_volume->view());
			} else {
				utils::loadToDevice(potential_file, host, cross_sections_volume->view());
				ImgType<float, 3> tmp_cross_sections { u(volume_size) };
				precomputeLocalCrossSections(
					makePhysicalVolume(utils::makeInterpolated(cross_sections_volume->view()), setup),
					tmp_cross_sections.view(),
					params);
				if (dump_path != "") {
					bolt::copy(tmp_cross_sections.view(), host_volume.view());
					mrc::Write(dump_path, host_volume.view(), BOHR_TO_ANG * u(bohr_per_voxel));
				}
				bolt::copy(tmp_cross_sections.view(), cross_sections_volume->view());
			}
			auto cs_int_view = utils::makeInterpolated(cross_sections_volume->view());
			physical_cs_int_view = boost::in_place(makePhysicalVolume(cs_int_view, setup));
		}

		//loading/computing scattering coefficients volume
		scattering_coefficients_volume = std::make_unique<IntImgType<float, 3>>(u(volume_size));
		if (scattering_file != "") {
			utils::loadToDevice(scattering_file, host, scattering_coefficients_volume->view());
		} else {
			utils::loadToDevice(potential_file, host, host);
			bolt::copy(bolt::square(host), host);
			bolt::copy(host, scattering_coefficients_volume->view());
		}
		auto scattering_int_view = utils::makeInterpolated(scattering_coefficients_volume->view());
		auto physical_scattering_int_view = makePhysicalVolume(scattering_int_view, setup);
		bolt::copy(scattering_int_view, host_volume.view());
		auto majorant = utils::getValueOfPercentile(host_volume.view(), params.get<ems_MajorantPercentile>());

		generators_image = std::make_unique<GeneratorsImage>(
			createRandomGeneratorsImage<USE_DEVICE>(u(setup.emitter_size), params.get<ems_RandomSeed>()));

		PeriodicTable p { };

		// DCS version of the microscope
		if (model_name == "dcs") {
			auto dcs_scattering = DcsScatteringRutherSampling<decltype(utils::makeInterpolated(cross_sections_volume->view()))> {
				physical_potential_int_view.value(), physical_cs_int_view.value(), physical_scattering_int_view, params,
				RutherfordFormulae { p.get(params.get<ems_RutherSamplingAtomicNumber>()), params.get<ems_ElectronEnergy>() },
				setup.rutherford.meanFreePath(), majorant };

			auto em_dcs = constructElectronMicroscope(setup, dcs_scattering, CounterImageFormation { });

			render_function = [this, em_dcs](
				RenderViewType projection,
				bolt::Quaternion<Num> orientation,
				const Pixel3& subpixel_shift,
				const Bohr& detector_distance) {
					return em_dcs.render(projection,orientation,subpixel_shift,detector_distance, this->generators_image->view());
				};
		} else

		// Rutherford version of the microscope
		if (model_name == "ruther") {
			potential_volume.reset(); //not needed, free the memory
			cross_sections_volume.reset();
			auto rutherford_scattering = RutherfordSimpleScattering<decltype(scattering_int_view)> { physical_scattering_int_view,
				RutherfordFormulae { p.get(params.get<ems_RutherSamplingAtomicNumber>()), params.get<ems_ElectronEnergy>() },
				setup.rutherford.meanFreePath(), majorant };

			auto em_rutherford = constructElectronMicroscope(setup, rutherford_scattering, CounterImageFormation { });

			render_function = [this, em_rutherford](
				RenderViewType projection,
				bolt::Quaternion<Num> orientation,
				const Pixel3& subpixel_shift,
				const Bohr& detector_distance) {
					return em_rutherford.render(projection,orientation,subpixel_shift,detector_distance, this->generators_image->view());
				};
		} else

		// Transmittance version of the microscope
		if (model_name == "trans") {
			scattering_coefficients_volume.reset();
			cross_sections_volume.reset();

			auto em_transmittance = constructElectronMicroscope(
				setup,
				NoScattering { },
				WeightedDistanceImageFormation { },
				makeWeightingVolumeRayObserver(physical_potential_int_view.value(), 0));

			render_function = [this, em_transmittance](
				RenderViewType projection,
				bolt::Quaternion<Num> orientation,
				const Pixel3& subpixel_shift,
				const Bohr& detector_distance) {
					return em_transmittance.render(projection,orientation,subpixel_shift,detector_distance, this->generators_image->view());
				};
		} else

		//uknonwn model
		{
			LOG("ERROR: Unknown model name: " << model_name);
			exit(1);
		}
	}

	Detector render(
		RenderViewType projection,
		bolt::Quaternion<Num> orientation,
		const Pixel3& subpixel_shift,
		const Bohr& detector_distance) const
	{
		return render_function(projection, orientation, subpixel_shift, detector_distance);
	}

	const std::string& modelName() {
		return model_name;
	}

	const ElectronMicroscopeSetup& peekSetup() {
			return setup;
		}
};
}

#endif /* SRC_EMS_EMS_H_ */
