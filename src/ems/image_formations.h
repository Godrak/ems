/*
 * image_formations.h
 *
 *  Created on: Oct 8, 2020
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#ifndef SRC_EMS_IMAGE_FORMATIONS_H_
#define SRC_EMS_IMAGE_FORMATIONS_H_

#include "raycast_data.h"
#include "localized_fourier_potential.h"
#include "boltview/view_iterators.h"

#include "params.h"

namespace ems {

struct DistancePayload {
	DistancePayload(const IPixel2& origin_pixel, size_t sample) {}

	Num weight { 1 };
	Bohr distance { 0 };
};

struct WeightedDistanceImageFormation {
	using Payload = DistancePayload;

	template<typename TProjectionView>
	BOLT_DECL_HYBRID
	void operator()(
		const TProjectionView& projection,
		const IPixel2& projection_pixel,
		const Payload& payload,
		const Ray& ray_deviation,
		const Bohr2& pixel_size) const
	{
		auto result = u(payload.distance) * payload.weight;
#if defined(__CUDA_ARCH__)
		atomicAdd(&projection[u(projection_pixel)], result);
#else
		projection[u(projection_pixel)] += result;
#endif
	}
};

struct CounterImageFormation {
	using Payload = DistancePayload;

	template<typename TProjectionView>
	BOLT_DECL_HYBRID
	void operator()(
		const TProjectionView& projection,
		const IPixel2& projection_pixel,
		const Payload& payload,
		const Ray& ray_deviation,
		const Bohr2& pixel_size) const
	{
#if defined(__CUDA_ARCH__)
		atomicAdd(&projection[u(projection_pixel)], payload.weight);
#else
		projection[u(projection_pixel)] += payload.weight;
#endif
	}
};

struct DebugImageFormation {
	using Payload = DistancePayload;

	template<typename TProjectionView>
	BOLT_DECL_HYBRID
	void operator()(
		const TProjectionView& projection,
		const IPixel2& projection_pixel,
		const Payload& payload,
		const Ray& ray_deviation,
		const Bohr2& pixel_size) const
	{
		if (payload.weight != 1) {
#if defined(__CUDA_ARCH__)
			atomicAdd(&projection[u(projection_pixel)], payload.weight);
#else
			projection[u(projection_pixel)] += payload.weight;
#endif
		}
	}
};

struct DistanceImageFormation {
	using Payload = DistancePayload;

	template<typename TProjectionView>
	BOLT_DECL_HYBRID
	void operator()(
		const TProjectionView& projection,
		const IPixel2& projection_pixel,
		const Payload& payload,
		const Ray& ray_deviation,
		const Bohr2& pixel_size) const
	{
#if defined(__CUDA_ARCH__)
		atomicAdd(&projection[u(projection_pixel)], payload.weight);
#else
		projection[u(projection_pixel)] += payload.weight * payload.distance;
#endif
	}
};

template<typename TProjectionView>
struct PhaseShiftImageFormation {
	using Payload = DistancePayload;
	using ComplexType = typename TProjectionView::Element;

	const float normalization_factor = 1.0f / sqrt(8 * M_PI * M_PI * M_PI);
	const Num electron_momentum;

	explicit PhaseShiftImageFormation(const json_params::JsonParamsHandler& params) :
		electron_momentum(energyToMomentum(params.get<ems_ElectronEnergy>()))
	{
	}

	BOLT_DECL_HYBRID
	void operator()(
		const TProjectionView& projection,
		const IPixel2& projection_pixel,
		const Payload& payload,
		const Ray& ray_deviation,
		const Bohr2& pixel_size) const
	{
		auto phase_shift = u(payload.distance) * electron_momentum;
		auto proj_size = projection.size();

		Num real_part { };
		Num imag_part { };
		sincos((double(phase_shift)), &imag_part, &real_part);
		auto wave_function = ComplexType { normalization_factor * real_part, normalization_factor * imag_part };

		wave_function.x *= payload.weight;
		wave_function.y *= payload.weight;

		auto* real = &projection[u(projection_pixel)].x;
		auto* imag = &projection[u(projection_pixel)].y;

#if defined(__CUDA_ARCH__)
				atomicAdd(real, wave_function.x);
				atomicAdd(imag, wave_function.y);
#else
		(*real) += wave_function.x;
		(*imag) += wave_function.y;
#endif
	}
};

template<typename TProjectionView>
struct GaussPhaseShiftImageFormation {
	using Payload = DistancePayload;
	using ComplexType = typename TProjectionView::Element;

	const float normalization_factor = 1.0f / sqrt(8 * M_PI * M_PI * M_PI);
	const float electron_momentum;
	const Bohr falloff_distance { 8 };

	explicit GaussPhaseShiftImageFormation(const json_params::JsonParamsHandler& params) :
		electron_momentum(energyToMomentum(params.get<ems_ElectronEnergy>()))
	{
	}

//	BOLT_DECL_HYBRID
//	IPixel2 getShiftedCoords(int x, int y, const IPixel2& center, const bolt::Int2& proj_size, bool& valid) const
//	{
//		IPixel2 res = center + t<IPixel2>(x, y);
//		valid = u(res) >= bolt::Int2{0,0} && u(res) < proj_size;
//		return res;
//	}

	BOLT_DECL_HYBRID
	IPixel2 getShiftedCoords(int x, int y, const IPixel2& center, const bolt::Int2& proj_size) const
	{
		auto res = u(center) + bolt::Int2 { x, y };
		return t<IPixel2>(res[0] % proj_size[0], res[1] % proj_size[1]);
	}

	BOLT_DECL_HYBRID
	void operator()(
		const TProjectionView& projection,
		const IPixel2& projection_pixel,
		const Payload& payload,
		const Ray& ray_deviation,
		const Bohr2& pixel_size) const
	{
		auto phase_shift = u(payload.distance) * electron_momentum;
		Num3 momentum = electron_momentum * ray_deviation.dir;
		auto proj_size = projection.size();
		auto pixels = falloff_distance / pixel_size[0];

//		for (int x = -pixels; x < pixels; ++x) {
//			for (int y = -pixels; y < pixels; ++y) {
		for (int x = 0; x < proj_size[0]; ++x) {
			for (int y = 0; y < proj_size[1]; ++y) {
				bool valid = true;
				auto coords = getShiftedCoords(x, y, projection_pixel, proj_size);
				if (!valid) {
					continue;
				}
				Num2 diff = bolt::product(u(pixel_size), u(coords - projection_pixel));
				auto dist = bolt::length(diff);

				auto falloff = exp(-(dist * dist / u(falloff_distance)));

				Num real_part { };
				Num imag_part { };
				Num wave_function_term = momentum[0] * diff[0] + momentum[1] * diff[1] + phase_shift;
				sincos(wave_function_term, &imag_part, &real_part);
				auto wave_function = ComplexType { normalization_factor * real_part, normalization_factor * imag_part };

				wave_function.x *= payload.weight; // * falloff;
				wave_function.y *= payload.weight; // * falloff;

				auto* real = &projection[u(coords)].x;
				auto* imag = &projection[u(coords)].y;

#if defined(__CUDA_ARCH__)
				atomicAdd(real, wave_function.x);
				atomicAdd(imag, wave_function.y);
#else
				(*real) += wave_function.x;
				(*imag) += wave_function.y;
#endif
			}
		}
	}
};

template<typename TComplexType>
struct ComplexToSquaredNormFunctor {
	BOLT_DECL_HYBRID
	Num operator()(const TComplexType& data) const
	{
		return data.x * data.x + data.y * data.y;
	}
};

template<typename TComplexType>
struct ComplexToImagFunctor {
	BOLT_DECL_HYBRID
	Num operator()(const TComplexType& data) const
	{
		return data.y;
	}
};

template<typename TComplexType>
struct ComplexToRealFunctor {
	BOLT_DECL_HYBRID
	Num operator()(const TComplexType& data) const
	{
		return data.x;
	}
};

template<typename TView, typename TFunctor>
bolt::UnaryOperatorImageView<TView, TFunctor> makeUnaryOperatorView(TView view, TFunctor functor = { }) {
	return bolt::UnaryOperatorImageView<TView, TFunctor> { view, functor };
}

}

#endif /* SRC_EMS_IMAGE_FORMATIONS_H_ */
