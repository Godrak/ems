/*
 * main_blocks.cpp
 *
 *  Created on: May 5, 2021
 *      Author: Pavel Mikus <pavel.mikus@eyen.se>
 */

#include "external/StarFileReader.h"
#include "external/projection.h"
#include "external/ctf_convolve.h"
#include "star_file_reader.h"

#include "helper_func.h"
#include "ems/ems.h"

PARAMWD(outputFile, std::string, "", "filename of the output");

int main(int argc, char** argv) {

	auto params = loadParams(argc, argv);

	auto ems = ems::Ems<true> { params };

	if (params.get<particlesRange>()[1] == 0) {
		return 0;
	}

	std::string star_file = params.get<projectionsStarFile>();

	//Projections
	bolt::Int2 image_size = bolt::Int2(u(params.get<detectorPixelSize>()), u(params.get<detectorPixelSize>()));

	std::vector<ctftools::Projection> projections { };
	projections = ctftools::readStarFile(star_file, image_size, ems.peekSetup());

	ems::Ems<true>::ImgType<float, 2> rendered_image { image_size };
	auto rendered_view = rendered_image.view();

	ems::Ems<false>::ImgType<float, 2> host_image { image_size };
	auto host_view = host_image.view();

	size_t start = params.get<particlesRange>()[0];
	size_t end = std::min(size_t(params.get<particlesRange>()[1]), projections.size());

	CtfConvolver<decltype(rendered_view)> convolver { image_size };

	auto output = params.get<outputFile>();
	if (output == "") {
		output = "rendered_particles_" + std::to_string(start) + "_" + std::to_string(end) + ".mrc";
	}

	auto output_apix = BOHR_TO_ANG * bolt::norm(u(ems.peekSetup().default_detector_x_vector)) / rendered_view.size()[0];

	auto dim3D = bolt::Vector<int, 3>(rendered_view.size()[0], rendered_view.size()[1], end - start);
	mrc::FormatDescription::Header header = mrc::SimpleVolumeHeader(
		dim3D[0],
		dim3D[1],
		dim3D[2],
		mrc::GetPixelType<float>(),
		0,
		0,
		0,
		output_apix);

	std::ofstream mrcFile(output, std::ios_base::out | std::ios_base::binary);
	if (!mrcFile.good()) {
		MRC_THROW(mrc::FileNotFoundError() << mrc::GetFileNotFoundInfo(output));
	}
	mrcFile << header;

	bool applyCtf = params.get<ems::ems_ApplyCTF>();
	for (size_t particle_index = start; particle_index < end; ++particle_index) {
		auto ctf_params = projections[particle_index].PsfParams<ctftools::CtfParameters>();
		auto detector_distance = ems::Bohr { 0 };
		if (params.get<ems::ems_ApplyDefocus>()) {
			detector_distance = max(ems::Bohr { ems::angToBohr(abs(ctf_params.defocus)) }, ems::Bohr { 0 });
		}

		ems.render(rendered_view, projections[particle_index].Orientation(), { }, detector_distance);
		if (applyCtf) {
			convolver.ctfConvolve2D(rendered_view, projections[particle_index]);
		}

		bolt::copy(rendered_view, host_view);
		using namespace mrc;
		mrcFile << host_view;

		std::cout << "particle: " << particle_index << " done " << std::endl;
	}

	mrcFile.close();

	return 0;
}
